package com.btoc.pharma.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class UserPermission {
    @Id
    private String id;
    private String userId;
    private String roleId;
    private String userName;
    private MasterPolicies master;
    private CustomerPolicies customer;
    private OrderPolicies order;
    private ReportsPolicies reports;

    public UserPermission(String id, String userId, String roleId,
                          String userName, MasterPolicies master, CustomerPolicies customer,
                          OrderPolicies order, ReportsPolicies reports) {
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
        this.userName = userName;
        this.master = master;
        this.customer = customer;
        this.order = order;
        this.reports = reports;
    }

    public UserPermission() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public MasterPolicies getMaster() {
        return master;
    }

    public void setMaster(MasterPolicies master) {
        this.master = master;
    }

    public CustomerPolicies getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerPolicies customer) {
        this.customer = customer;
    }

    public OrderPolicies getOrder() {
        return order;
    }

    public void setOrder(OrderPolicies order) {
        this.order = order;
    }

    public ReportsPolicies getReports() {
        return reports;
    }

    public void setReports(ReportsPolicies reports) {
        this.reports = reports;
    }
}
