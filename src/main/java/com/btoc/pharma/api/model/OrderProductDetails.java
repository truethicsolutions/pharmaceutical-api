package com.btoc.pharma.api.model;

public class OrderProductDetails {
    private String productId;
    private String name;
    private String image;
    private int quantity;
    private double pckQuantity;
    private double mrp;
    private double totalAmount;
    private double discountAmount;
    private double discount;
    private String discountType;
    private int status;
    private double sTax;
    private double cTax;
    private double totalTax;
    private String packing;
    public OrderProductDetails() {
    }

    public OrderProductDetails(String productId, String name, String image, int quantity,
                               double pckQuantity, double mrp, double totalAmount,
                               double discountAmount, double discount, String discountType,
                               int status, double sTax, double cTax, double totalTax, String packing) {
        this.productId = productId;
        this.name = name;
        this.image = image;
        this.quantity = quantity;
        this.pckQuantity = pckQuantity;
        this.mrp = mrp;
        this.totalAmount = totalAmount;
        this.discountAmount = discountAmount;
        this.discount = discount;
        this.discountType = discountType;
        this.status = status;
        this.sTax = sTax;
        this.cTax = cTax;
        this.totalTax = totalTax;
        this.packing = packing;
    }

    public double getsTax() {
        return sTax;
    }

    public void setsTax(double sTax) {
        this.sTax = sTax;
    }

    public double getcTax() {
        return cTax;
    }

    public void setcTax(double cTax) {
        this.cTax = cTax;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getPckQuantity() {
        return pckQuantity;
    }

    public void setPckQuantity(double pckQuantity) {
        this.pckQuantity = pckQuantity;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }
}
