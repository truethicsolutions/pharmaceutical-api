package com.btoc.pharma.api.model;

import com.btoc.pharma.api.dto.RolesDTO;

import java.util.List;

public class Data {

    private List<RolesDTO> data;
    private long total;//total rows
    private int page; // page number
    private int per_page; // page size/ limit
    private int total_pages; // total pages

    public Data(List<RolesDTO> data, long total, int page, int per_page, int total_pages) {
        this.data = data;
        this.total = total;
        this.page = page;
        this.per_page = per_page;
        this.total_pages = total_pages;
    }

    public List<RolesDTO> getData() {
        return data;
    }

    public void setData(List<RolesDTO> data) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }
}
