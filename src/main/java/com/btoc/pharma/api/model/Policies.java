package com.btoc.pharma.api.model;

public class Policies {
    /* Json parameters */
    private MasterPolicies master;
    private CustomerPolicies customer;
    private OrderPolicies order;
    private ReportsPolicies reports;

    public Policies() {
        super();
    }

    public Policies(MasterPolicies master, CustomerPolicies customer,
                    OrderPolicies order, ReportsPolicies reports) {
        this.master = master;
        this.customer = customer;
        this.order = order;
        this.reports = reports;
    }

    public MasterPolicies getMaster() {
        return master;
    }

    public void setMaster(MasterPolicies master) {
        this.master = master;
    }

    public CustomerPolicies getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerPolicies customer) {
        this.customer = customer;
    }

    public OrderPolicies getOrder() {
        return order;
    }

    public void setOrder(OrderPolicies order) {
        this.order = order;
    }

    public ReportsPolicies getReports() {
        return reports;
    }

    public void setReports(ReportsPolicies reports) {
        this.reports = reports;
    }
}
