package com.btoc.pharma.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Document
public class Order {
    @Id
    private String id;
    private String adminId;
    private String userId;
    private String outletId;
    private String customerId;
    private String deliveryBoyId;
    private String branchId;
    private String orderNo;
    private LocalDateTime orderDate;
    private String customerName;
    private long mobileNumber;
    private String orderStatus;
    private int orderQuantity;
    // private String orderUnit;
    private double totalPrice;
    private double orderDiscount;
    private String paymentMode;
    private String paymentStatus;
    private LocalDateTime paymentDate;
    private int payableAmount;
    private String deliveryAddress;
    private String latitude;
    private String longitude;
    private String deliveryBoyName;
    private String deliveryStatus;
    private LocalDateTime deliveryDate;
    private String remark;
    private String transactionNumber;
    private String orderType;
    private String orderImagePath;
    private String prescriptionCode;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private int status;
    private int loyaltyPoints;
    private int orderFrom;
    private List<OrderProductDetails> productDetailsList = new ArrayList();
    private PaymentDetails paymentDetails;

    public Order() {

    }

    public Order(String id, String adminId, String userId, String outletId, String customerId,
                 String orderNo, LocalDateTime orderDate, String customerName, long mobileNumber,
                 String orderStatus, int orderQuantity, double totalPrice, double orderDiscount,
                 String paymentMode, String paymentStatus, LocalDateTime paymentDate, String deliveryAddress,
                 String deliveryBoyName, String deliveryStatus, String remark, String transactionNumber,
                 String orderType, String orderImagePath, String createdAt, String updatedAt,
                 String createdBy, String updatedBy, int status, String latitude, String longitude,
                 int loyaltyPoints, List<OrderProductDetails> productDetailsList, String branchId,
                 PaymentDetails paymentDetails, String prescriptionCode, String deliveryBoyId,
                 int orderFrom, int payableAmount, LocalDateTime deliveryDate) {
        this.id = id;
        this.adminId = adminId;
        this.userId = userId;
        this.outletId = outletId;
        this.customerId = customerId;
        this.orderNo = orderNo;
        this.orderDate = orderDate;
        //this.productId = productId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.orderStatus = orderStatus;
        this.orderQuantity = orderQuantity;
        this.totalPrice = totalPrice;
        this.orderDiscount = orderDiscount;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.paymentDate = paymentDate;
        this.deliveryAddress = deliveryAddress;
        this.deliveryBoyName = deliveryBoyName;
        this.deliveryStatus = deliveryStatus;
        this.remark = remark;
        this.transactionNumber = transactionNumber;
        this.orderType = orderType;
        this.orderImagePath = orderImagePath;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.status = status;
        this.loyaltyPoints = loyaltyPoints;
        this.productDetailsList = productDetailsList;
        this.paymentDetails = paymentDetails;
        this.branchId = branchId;
        this.deliveryBoyId = deliveryBoyId;
        this.prescriptionCode = prescriptionCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.orderFrom = orderFrom;
        this.payableAmount = payableAmount;
        this.deliveryDate = deliveryDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public int getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(int payableAmount) {
        this.payableAmount = payableAmount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public void setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrderImagePath() {
        return orderImagePath;
    }

    public void setOrderImagePath(String orderImagePath) {
        this.orderImagePath = orderImagePath;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(int loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public List<OrderProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    public void setProductDetailsList(List<OrderProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getPrescriptionCode() {
        return prescriptionCode;
    }

    public void setPrescriptionCode(String prescriptionCode) {
        this.prescriptionCode = prescriptionCode;
    }

    public int getOrderFrom() {
        return orderFrom;
    }

    public void setOrderFrom(int orderFrom) {
        this.orderFrom = orderFrom;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
}
