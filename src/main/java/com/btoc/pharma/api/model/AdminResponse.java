package com.btoc.pharma.api.model;

public class AdminResponse {
    private String fname;
    private String lname;
    private String email;
    private long mobileNumber;
    private String roleName;
    private String code;
    private String username;
    private String tradeId;
    private String tradeName;
    private int status;

    public AdminResponse(long branchId, String fname, String lname, String email, long mobileNumber,
                         String roleName, String code, String username, String tradeId,
                         String tradeName, int status) {

        this.fname = fname;
        this.lname = lname;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.roleName = roleName;
        this.code = code;
        this.username = username;
        this.tradeId = tradeId;
        this.tradeName = tradeName;
        this.status = status;
    }

    public AdminResponse() {
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }
}
