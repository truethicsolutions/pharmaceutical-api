package com.btoc.pharma.api.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "cities")
public class City {
    private String countryId;
    private String stateCode;
    private String cityName;

    private City(){

    }

    public City(String countryId, String stateCode, String cityName) {
        this.countryId = countryId;
        this.stateCode = stateCode;
        this.cityName = cityName;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
