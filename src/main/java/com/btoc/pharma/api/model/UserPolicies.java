package com.btoc.pharma.api.model;

public class UserPolicies {
    private String readKey;
    private String writeKey;
    private int readValue;
    private int writeValue;

    public UserPolicies() {
    }

    public UserPolicies(String readKey, String writeKey,
                        int readValue, int writeValue) {
        this.readKey = readKey;
        this.writeKey = writeKey;
        this.readValue = readValue;
        this.writeValue = writeValue;
    }

    public String getReadKey() {
        return readKey;
    }

    public void setReadKey(String readKey) {
        this.readKey = readKey;
    }

    public String getWriteKey() {
        return writeKey;
    }

    public void setWriteKey(String writeKey) {
        this.writeKey = writeKey;
    }

    public int getReadValue() {
        return readValue;
    }

    public void setReadValue(int readValue) {
        this.readValue = readValue;
    }

    public int getWriteValue() {
        return writeValue;
    }

    public void setWriteValue(int writeValue) {
        this.writeValue = writeValue;
    }
}
