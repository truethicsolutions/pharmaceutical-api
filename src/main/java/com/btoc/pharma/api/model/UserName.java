package com.btoc.pharma.api.model;

import com.btoc.pharma.api.dao.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserName {
    @Autowired
    private UsersRepository usersRepository;

    public String getUserName(String id) {
        Users mUsers = usersRepository.findByIdAndStatus(id, 1);
        String userName = null;
        if (mUsers != null) {
            userName = mUsers.getFirstName() + " " + mUsers.getLastName();
        }
        return userName;
    }
}

