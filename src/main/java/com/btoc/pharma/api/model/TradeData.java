package com.btoc.pharma.api.model;

import com.btoc.pharma.api.dto.TradeDTO;

import java.util.List;

public class TradeData {
    List<TradeDTO> data;
    int total;

    public TradeData(List<TradeDTO> data, int total) {
        this.data = data;
        this.total = total;
    }

    public TradeData() {
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
