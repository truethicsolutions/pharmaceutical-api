package com.btoc.pharma.api.model;

public class PaymentDetails {
    private String orderId;
    private String paymentMode;
    private String paymentStatus;
    private String paymentDate;
    private String transactionNummber;

    public PaymentDetails() {
    }

    public PaymentDetails(String orderId, String paymentMode, String paymentStatus,
                          String paymentDate, String transactionNummber) {
        this.orderId = orderId;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.paymentDate = paymentDate;
        this.transactionNummber = transactionNummber;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getTransactionNummber() {
        return transactionNummber;
    }

    public void setTransactionNummber(String transactionNummber) {
        this.transactionNummber = transactionNummber;
    }
}
