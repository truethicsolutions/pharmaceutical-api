package com.btoc.pharma.api.model;

public class CustomerPolicies {
    private String customerPermission;

    public CustomerPolicies(String customerPermission) {
        this.customerPermission = customerPermission;
    }

    public CustomerPolicies() {
    }

    public String getCustomerPermission() {
        return customerPermission;
    }

    public void setCustomerPermission(String customerPermission) {
        this.customerPermission = customerPermission;
    }
}
