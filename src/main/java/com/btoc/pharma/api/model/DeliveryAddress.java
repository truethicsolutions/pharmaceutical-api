package com.btoc.pharma.api.model;

public class DeliveryAddress {
    private String customerId;
    private String address;
    private String pincode;
    private String country;
    private String state;
    private String city;
    private String district;
    private int defaultAddress;

    public DeliveryAddress() {
    }

    public DeliveryAddress(String address, String pincode, String country, int defaultAddress,
                           String state, String city, String district, String customerId) {
        this.address = address;
        this.pincode = pincode;
        this.country = country;
        this.state = state;
        this.city = city;
        this.district = district;
        this.customerId = customerId;
        this.defaultAddress = defaultAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(int defaultAddress) {
        this.defaultAddress = defaultAddress;
    }
}
