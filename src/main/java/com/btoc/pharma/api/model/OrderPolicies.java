package com.btoc.pharma.api.model;

public class OrderPolicies {
    private String orderPermission;

    public OrderPolicies(String orderPermission) {
        this.orderPermission = orderPermission;
    }

    public OrderPolicies() {
    }

    public String getOrderPermission() {
        return orderPermission;
    }

    public void setOrderPermission(String orderPermission) {
        this.orderPermission = orderPermission;
    }
}
