package com.btoc.pharma.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;

@Document
public class Customer {

    @Id
    private String id;
    private String adminId;
    private String outletId;
    @Field("firstName")
    private String fName;
    @Field("lastName")
    private String lName;
    @Field("contactNumber")
    private long mobileNumber;
    private String gender;
    private String profileImage;
    private String emailId;
    private String userName;
    private String password;
    private String address;
    private String pincode;
    private String country;
    private String state;
    private String city;
    private String district;
    private long rewardsPoints;
    @Field("dateofbirth")
    private String dob;
    @Field("role")
    private String userRoleName;
    @Field("roleType")
    private String userRoleType;
    private int appUser;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private int status;
    private List<DeliveryAddress> deliveryAddress;
    private String fcmToken;

    public Customer() {
        deliveryAddress = new ArrayList<>();
    }

    public Customer(String id, String adminId, String outletId, String fName, String lName, long mobileNumber, String gender, String profileImage, String emailId, String userName, String password, String address, String pincode, String country, String state, String city, String district, long rewardsPoints, String dob, String userRoleName, String userRoleType, String createdAt, String updatedAt,
                    String createdBy, String updatedBy, List<DeliveryAddress> deliveryAddress,
                    int status, int appUser, String fcmToken) {
        this.id = id;
        this.adminId = adminId;
        this.outletId = outletId;
        this.fName = fName;
        this.lName = lName;
        this.mobileNumber = mobileNumber;
        this.gender = gender;
        this.profileImage = profileImage;
        this.emailId = emailId;
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.pincode = pincode;
        this.country = country;
        this.state = state;
        this.city = city;
        this.district = district;
        this.rewardsPoints = rewardsPoints;
        this.dob = dob;
        this.userRoleName = userRoleName;
        this.userRoleType = userRoleType;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.deliveryAddress = deliveryAddress;
        this.status = status;
        this.appUser = appUser;
        this.fcmToken = fcmToken;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public String getUserRoleType() {
        return userRoleType;
    }

    public void setUserRoleType(String userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<DeliveryAddress> getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(List<DeliveryAddress> deliveryAddress) {

        this.deliveryAddress = deliveryAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public long getRewardsPoints() {
        return rewardsPoints;
    }

    public void setRewardsPoints(long rewardsPoints) {
        this.rewardsPoints = rewardsPoints;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAppUser() {
        return appUser;
    }

    public void setAppUser(int appUser) {
        this.appUser = appUser;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
