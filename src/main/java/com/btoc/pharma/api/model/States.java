package com.btoc.pharma.api.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class States {
    private String countryId;
    private String stateName;
    private String stateCode;

    private States()
    {

    }
    public States(String countryId, String stateName, String stateCode) {
        this.countryId = countryId;
        this.stateName = stateName;
        this.stateCode = stateCode;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
}
