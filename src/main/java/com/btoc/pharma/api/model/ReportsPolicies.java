package com.btoc.pharma.api.model;

public class ReportsPolicies {

    private String paymentPermission;
    private String deliveryPermission;
    private String reportsPermission;

    public ReportsPolicies(String paymentPermission, String deliveryPermission, String reportsPermission) {
        this.paymentPermission = paymentPermission;
        this.deliveryPermission = deliveryPermission;
        this.reportsPermission = reportsPermission;
    }

    public ReportsPolicies() {
    }

    public String getPaymentPermission() {
        return paymentPermission;
    }

    public void setPaymentPermission(String paymentPermission) {
        this.paymentPermission = paymentPermission;
    }

    public String getDeliveryPermission() {
        return deliveryPermission;
    }

    public void setDeliveryPermission(String deliveryPermission) {
        this.deliveryPermission = deliveryPermission;
    }

    public String getReportsPermission() {
        return reportsPermission;
    }

    public void setReportsPermission(String reportsPermission) {
        this.reportsPermission = reportsPermission;
    }

}
