package com.btoc.pharma.api.model;

import org.springframework.data.annotation.Id;

public class ForgotUserPassword {
    @Id
    private String id;
    private String userId;
    private String otp;
    private String createdAt;

    public ForgotUserPassword() {
    }

    public ForgotUserPassword(String userId, String otp, String createdAt, String id) {
        this.userId = userId;
        this.otp = otp;
        this.createdAt = createdAt;
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

