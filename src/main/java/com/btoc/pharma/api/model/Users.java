package com.btoc.pharma.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Users {
    @Id
    private String id;
    private String tradeId;
    private String institutionName;
    private String firstName;
    private String lastName;
    private String gender;
    private String address;
    private String email;
    private long mobileNumber;
    private String roleId;//role(map roleId instead roleName)
    private String username;
    private String password;
    private String adminId;
    private String imagePath;
    private int userType;
    private int status;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private String fcmToken;

}
