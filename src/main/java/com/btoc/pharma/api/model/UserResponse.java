package com.btoc.pharma.api.model;

import com.fasterxml.jackson.annotation.JsonRootName;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@JsonRootName(value = "Result")
public class UserResponse {
    private String adminId;
    private String outletId;
    private String customerId;
    private String fName;
    private String lName;
    private long mobileNumber;
    private String gender;
    private String emailId;
    private String profileImage;
    private String userName;
    private String password;
    private String address;
    private String pincode;
    private String country;
    private String state;
    private String city;
    private String district;
    private long rewardsPoints;
    private String dob;
    private String userRoleName;
    private String userRoleType;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private String otp;
    private int appUser;
    private List<DeliveryAddress> deliveryAddress;
    private String fcmToken;
    private MultipartFile image;
    private int status;

    public UserResponse(String userId, String adminId, String fName, String lName, long mobileNumber,
                        String gender, String emailId, String profileImage, String userName,
                        String password, String address, String pincode, String country,
                        String state, String city, String district, long rewardsPoints, String dob,
                        String userRoleName, String userRoleType, String createdAt, String updatedAt,
                        String createdBy, String updatedBy, String otp, String customerId, int appUser,
                        List<DeliveryAddress> deliveryAddress, MultipartFile image, int status,
                        String fcmToken) {
        this.adminId = adminId;
        this.outletId = outletId;
        this.customerId = customerId;
        this.fName = fName;
        this.lName = lName;
        this.mobileNumber = mobileNumber;
        this.gender = gender;
        this.emailId = emailId;
        this.profileImage = profileImage;
        this.userName = userName;
        this.password = password;
        this.address = address;
        this.pincode = pincode;
        this.country = country;
        this.state = state;
        this.city = city;
        this.district = district;
        this.rewardsPoints = rewardsPoints;
        this.dob = dob;
        this.userRoleName = userRoleName;
        this.userRoleType = userRoleType;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.otp = otp;
        this.deliveryAddress = deliveryAddress;
        this.image = image;
        this.status = status;
        this.appUser = appUser;
        this.fcmToken = fcmToken;
    }

    public UserResponse() {
        super();
        deliveryAddress = new ArrayList<>();

    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public String getUserRoleType() {
        return userRoleType;
    }

    public void setUserRoleType(String userRoleType) {
        this.userRoleType = userRoleType;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<DeliveryAddress> getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(List<DeliveryAddress> deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public long getRewardsPoints() {
        return rewardsPoints;
    }

    public void setRewardsPoints(long rewardsPoints) {
        this.rewardsPoints = rewardsPoints;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public int getAppUser() {
        return appUser;
    }

    public void setAppUser(int appUser) {
        this.appUser = appUser;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }
}
