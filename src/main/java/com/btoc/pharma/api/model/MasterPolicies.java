package com.btoc.pharma.api.model;

public class MasterPolicies {

    private String rolePermission;//
    private String branchPermission;//
    private String outletPermission;//
    private String catlogPermission;///
    private String productPermission;//
    private String rewardPermission;//
    private String userPermission;

    public MasterPolicies(String rolePermission, String branchPermission,
                          String outletPermission, String catlogPermission,
                          String productPermission, String paymentPermission,
                          String rewardPermission, String userPermission) {
        this.rolePermission = rolePermission;
        this.branchPermission = branchPermission;
        this.outletPermission = outletPermission;
        this.catlogPermission = catlogPermission;
        this.productPermission = productPermission;
        this.rewardPermission = rewardPermission;
        this.userPermission = userPermission;
    }

    public MasterPolicies() {
    }

    public String getRolePermission() {
        return rolePermission;
    }

    public void setRolePermission(String rolePermission) {
        this.rolePermission = rolePermission;
    }

    public String getBranchPermission() {
        return branchPermission;
    }

    public void setBranchPermission(String branchPermission) {
        this.branchPermission = branchPermission;
    }

    public String getOutletPermission() {
        return outletPermission;
    }

    public void setOutletPermission(String outletPermission) {
        this.outletPermission = outletPermission;
    }

    public String getCatlogPermission() {
        return catlogPermission;
    }

    public void setCatlogPermission(String catlogPermission) {
        this.catlogPermission = catlogPermission;
    }

    public String getProductPermission() {
        return productPermission;
    }

    public void setProductPermission(String productPermission) {
        this.productPermission = productPermission;
    }

    public String getRewardPermission() {
        return rewardPermission;
    }

    public void setRewardPermission(String rewardPermission) {
        this.rewardPermission = rewardPermission;
    }

    public String getUserPermission() {
        return userPermission;
    }

    public void setUserPermission(String userPermission) {
        this.userPermission = userPermission;
    }
}
