package com.btoc.pharma.api.model;

import org.springframework.stereotype.Component;

@Component
public class BaseUrl {
    private String BASE_URL = "https://btocbucket.s3.ap-south-1.amazonaws.com";

    public String getBASE_URL() {
        return BASE_URL;
    }
}
