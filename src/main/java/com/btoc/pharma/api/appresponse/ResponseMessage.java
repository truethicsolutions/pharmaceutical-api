package com.btoc.pharma.api.appresponse;

/* Singlton class */

public class ResponseMessage {

    private static ResponseMessage instance = null;
    private String message = null;
    private String responseStatus = null;


    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private ResponseMessage() {
    }

    public static ResponseMessage getInstance() {

        if (instance == null) {
            instance = new ResponseMessage();
        }
        return instance;
    }

    public static void setInstance(ResponseMessage instance) {
        ResponseMessage.instance = instance;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }


}
