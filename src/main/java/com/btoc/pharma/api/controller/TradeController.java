package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.Trade;
import com.btoc.pharma.api.services.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class TradeController {
    @Autowired
    private TradeService service;

    @PostMapping(path = "/registerTrade")
    public ResponseEntity<?> registerTrade(@RequestBody Trade trade) {
        //httpHeaders.add("ACCESS_CONTROL_ALLOW_ORIGIN","*");
        //	httpHeaders.setAccessControlAllowOrigin("ACCESS_CONTROL_ALLOW_ORIGIN");
        return ResponseEntity.ok(service.registerTrade(trade));
    }

    @PostMapping(path = "/updateTrade")
    public ResponseEntity<?> updateTrade(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.updateTrade(requestParam.get("id"), requestParam.get("tradeName")));
    }

    @PostMapping(path = "/deleteTrade")
    public ResponseEntity<?> deleteTrade(@RequestBody Map<String, String> requestParam) {

        return ResponseEntity.ok(service.deleteTrade(requestParam.get("id")));
    }

    @GetMapping(path = "/getTrade")
    public List<Trade> getTrades() {
        List<Trade> list = service.getTrades();
        return list;
    }
    @PostMapping(path = "/getAllTrades")
    public ResponseEntity<?> getAllTrades(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getAllTrades(requestParam.get("pageNumber"),
                requestParam.get("pageSize")));
    }

    @GetMapping(path = "/getTrade/{id}")
    public Trade getTrades(@PathVariable String id) {
        Trade list = service.getTrades(id);
        return list;
    }
}
