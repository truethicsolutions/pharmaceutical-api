package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.City;
import com.btoc.pharma.api.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CityController {

    @Autowired
    private CityService cityservice;

    @PostMapping(path = "/getCity")
    public List<City> getCity(@RequestBody Map<String, String> requestParam) {
        List<City> list = cityservice.getCity(requestParam.get("stateCode"));
        return list;
    }
}
