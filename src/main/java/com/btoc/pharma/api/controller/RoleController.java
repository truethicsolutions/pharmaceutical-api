package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.RolesDTO;
import com.btoc.pharma.api.model.Roles;
import com.btoc.pharma.api.model.UserPermission;
import com.btoc.pharma.api.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
public class RoleController {

    @Autowired
    RoleService service;

    @PostMapping(path = "/registerRoles")
    public ResponseEntity<?> registerRoles(@RequestBody Roles role) {
        return ResponseEntity.ok(service.registerRoles(role));
    }

    @PostMapping(path = "/updateRoles")
    public RolesDTO updateRoles(@RequestBody Map<String, String> requestParam) {

        RolesDTO response = service.updateRoles(requestParam.get("id"), requestParam.get("roleName"));
        return response;
    }

    @PostMapping(path = "/deleteRoles")
    public ResponseEntity<?> deleteRoles(@RequestBody Map<String, String> requestParam) {

        return ResponseEntity.ok(service.deleteRoles(requestParam.get("id")));
    }

    @PostMapping(path = "/getRoles")
    public List<RolesDTO> getRoles(@RequestBody Map<String, String> requestParam) {
        List<RolesDTO> list = service.getRoles(requestParam.get("adminId"));
        return list;
    }

    @PostMapping(path = "/getAllRoles")
    public ResponseEntity<?> getAllRoles(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getAllRoles(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    @PostMapping(path = "/getPermission")
    public ResponseEntity<?> getPermission(@RequestBody Map<String, String> requestParam) {

        return ResponseEntity.ok(service.getPermission(requestParam.get("userId"),
                requestParam.get("roleId")));
    }

    @PostMapping(path = "/updatePermission")
    public ResponseEntity<?> updatePermission(@RequestBody UserPermission userPermission) {
        return ResponseEntity.ok(service.updatePermission(userPermission));
    }
}
