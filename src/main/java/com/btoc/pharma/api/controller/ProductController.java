package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dto.ProductDTO;
import com.btoc.pharma.api.services.ExcelService;
import com.btoc.pharma.api.services.ProductService;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class ProductController {

    @Autowired
    private ProductService service;

    @Autowired
    ExcelService excelService;

    @PostMapping("/createProduct")
    public ResponseEntity<?> createProduct(@ModelAttribute ProductDTO productDTO) {
        return ResponseEntity.ok(service.createProduct(productDTO));
    }

    @PostMapping("/updateProduct")
    public ResponseEntity<?> updateProduct(@ModelAttribute ProductDTO productDTO) {
        return ResponseEntity.ok(service.updateProduct(productDTO));
    }

    /* Update Rate of Product from Generate Order Page -> back office */
    @PostMapping("/updateRatePerPiece")
    public ResponseEntity<?> updateProductPerPeice(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.updateRatePerPiece(requestParam.get("id"),
                requestParam.get("ratePerPiece"), requestParam.get("discountAmount")));
    }

    @PostMapping("/deleteProduct")
    public ResponseEntity<?> deleteProduct(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteProduct(requestParam.get("id")));
    }

    @PostMapping("/getProducts")
    public ResponseEntity<?> getProducts(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getProducts(requestParam.get("adminId")));
    }

    @PostMapping("/fetchProduct")
    public ResponseEntity<?> fetchProduct(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchProduct(requestParam.get("id")));
    }

    /* By Sneha */
    @PostMapping(path = "/fetchAllProduct")
    public ResponseEntity<?> fetchAllProduct(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchAllProduct(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    @PostMapping("/uploadExcel")
    public ResponseEntity<ResponseMessage> uploadExcel(@RequestParam(required = false) String userId,
                                                       @RequestParam(required = false) String adminId,
                                                       @RequestParam("file") MultipartFile file,
                                                       @RequestParam("zipFile") MultipartFile zipFile) {
        String message = "";
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        try {
            excelService.save(file, userId, adminId, zipFile);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            responseMessage.setMessage(message);
            responseMessage.setResponseStatus("success");
            return ResponseEntity.ok(responseMessage);
        } catch (Exception e) {
            e.printStackTrace();
            message = "Could not upload the file: " + file.getOriginalFilename() + "!" + e.getMessage();
            responseMessage.setMessage(message);
            responseMessage.setResponseStatus("Error");
            return ResponseEntity.ok(responseMessage);
        }
    }

    /*** for Mobile App ***/

    @PostMapping(path = "/getAllCompanyProducts")
    public Object getAllCompanyProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.getAllProducts(requestParam.get("companyId"), "company");
        return result.toString();
    }

    @PostMapping(path = "/getAllCategoryProducts")
    public Object getAllCategoryProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.getAllProducts(requestParam.get("categoryId"), "category");
        return result.toString();
    }

    @PostMapping(path = "/getTrendingProducts")
    public Object getTrendingProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.getAllProducts(requestParam.get("adminId"), "trending");
        return result.toString();
    }

    @PostMapping(path = "/getAllTrendingProducts")
    public Object getAllTrendingProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.getAllProducts(requestParam.get("adminId"), "Alltrending");
        return result.toString();
    }

    @PostMapping(path = "/getRelatedProducts")
    public Object getRelatedProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.getRelatedProducts(requestParam.get("adminId"), requestParam.get("companyId"));
        return result.toString();
    }

    @PostMapping(path = "/searchProducts")
    public Object searchProducts(@RequestBody Map<String,
            String> requestParam) throws JSONException {
        JSONArray result = service.searchProducts(requestParam.get("adminId"), requestParam.get("key"));
        return result.toString();
    }
    /*** end of for Mobile app ***/
}
