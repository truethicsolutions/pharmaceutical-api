package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dto.FiltersDTO;
import com.btoc.pharma.api.services.FiltersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FiltersController {
    @Autowired
    private FiltersService service;

    @PostMapping(path = "/createFilters")
    public ResponseEntity<ResponseMessage> createFilters(@ModelAttribute FiltersDTO filtersDTO) {
        ResponseEntity<ResponseMessage> response = service.createFilters(filtersDTO);
        return response;
    }

    @PostMapping(path = "/updateFilters")
    public ResponseEntity<ResponseMessage> updateFilters(@ModelAttribute FiltersDTO filtersDTO) {
        ResponseEntity<ResponseMessage> response = service.updateFilters(filtersDTO);
        return response;
    }

    @GetMapping("/getFilters/{id}/{categoryId}")
    public FiltersDTO getFilters(@PathVariable String id, @PathVariable String categoryId) {
        FiltersDTO filtersDTO = service.getFilters(id, categoryId);
        return filtersDTO;
    }

    @GetMapping("/getAllFilters/{categoryId}")
    public List<FiltersDTO> getAllFilters(@PathVariable String categoryId) {
        List<FiltersDTO> filtersDTOList = service.getAllFilters(categoryId);
        return filtersDTOList;
    }
}
