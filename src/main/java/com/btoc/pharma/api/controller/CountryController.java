package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.Country;
import com.btoc.pharma.api.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class CountryController {
    @Autowired
    private CountryService countryService;

    @GetMapping(path = "/getCountry")
    public List<Country> getCountry() {
        List<Country> list = countryService.getCountry();
        return list;
    }
}
