package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.AdminDTO;
import com.btoc.pharma.api.model.Admin;
import com.btoc.pharma.api.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;


@RestController
public class AdminController {
    @Autowired
    AdminService service;
	/*@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	@Autowired
	private JwtUserDetailsService userDetailsService;*/


    @PostMapping(path = "/registerAdmin")
    public ResponseEntity<?> registerAdmin(@RequestBody Admin admin) {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String createdAt = myDateObj.format(myFormatObj);
		long millis = myDateObj
				.atZone(ZoneId.systemDefault())
				.toInstant().toEpochMilli();
		admin.setCreatedAt(createdAt);
		admin.setUpdatedAt(createdAt);
		admin.setCreatedBy("SuperAdmin");
		admin.setUpdatedBy("SuperAdmin");
		return ResponseEntity.ok(service.registerAdmin(admin));
	}

	@PostMapping(path = "/updateAdmin")
	public ResponseEntity<?> updateAdmin(@RequestBody Admin admin) {
		LocalDateTime myDateObj = LocalDateTime.now();
		DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
		String updatedAt = myDateObj.format(myFormatObj);
		admin.setUpdatedAt(updatedAt);
		AdminDTO response = service.updateAdmin(admin);
		return ResponseEntity.ok(response);
	}

	@GetMapping(path = "/getAdmins")
	public List<Admin> getAllAdmin() {
        List<Admin> list = service.getAllAdmin();
        return list;
    }

    @GetMapping("/getAdmin/{id}")
    public AdminDTO getAdmin(@PathVariable String id) {
        AdminDTO admin = service.getAdmin(id);
        return admin;
    }

}
