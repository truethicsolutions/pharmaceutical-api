package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dto.CustomerDTO;
import com.btoc.pharma.api.model.DeliveryAddress;
import com.btoc.pharma.api.model.UserResponse;
import com.btoc.pharma.api.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

@RestController
class CustomerController {

    @Autowired
    private CustomerService userService;

    /* for mobile App */
    @PostMapping(path = "/loginCustomer")
    public CustomerDTO loginUser(@RequestBody Map<String, String> requestPara) {
        CustomerDTO customerDTOResponse = userService.loginUser(requestPara.get("mobileNumber"),
                requestPara.get("password"));
        return customerDTOResponse;
    }

    /* update Customer Profile */
    @PostMapping("/updateCustomer")
    public ResponseEntity<ResponseMessage> updateUser(@RequestBody UserResponse userResponse) {
        ResponseEntity<ResponseMessage> respponse = userService.updateUser(userResponse);
        return respponse;
    }

    @PostMapping(path = "/forgetPassword")
    @ResponseBody
    public ResponseEntity<ResponseMessage> forgetPassword(@RequestBody Map<String, Long> body,
                                                          HttpServletRequest request,
                                                          HttpSession httpSession)
            throws AddressException, MessagingException, IOException {

        ResponseEntity<ResponseMessage> respponse = userService.forgetPassword(body.get("mobileNumber"),
                request, httpSession);
        return respponse;
    }

    @PostMapping(path = "/getProfile")
    public CustomerDTO getUser(@RequestBody Map<String, String> requestParam)
            throws AddressException, MessagingException, IOException {
        CustomerDTO customerDTOResponse = userService.getUser(requestParam.get("mobileNumber"));
        return customerDTOResponse;
    }

    @PostMapping(path = "/updatePassword")
    public ResponseEntity<ResponseMessage> updatePassword(@RequestBody Map<String, String> requestParam,
                                                          HttpServletRequest request,
                                                          HttpSession httpSession) {
        ResponseEntity<ResponseMessage> response = userService.updatePassword(requestParam.get("mobileNumber"),
                requestParam.get("password"), request, httpSession);
        return response;
    }

    @PostMapping(path = "/registerCustomer")
    public ResponseEntity<?> registerCustomer(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(userService.registerCustomer(requestParam.get("mobileNumber"),
                requestParam.get("password"), requestParam.get("adminId"), requestParam.get("outletId"),
                requestParam.get("firstName"), requestParam.get("lastName")));
    }

    /*** Back office- Customer Details ***/
    @PostMapping("/getCustomers")
    public ResponseEntity<?> getCustomers(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(userService.getCustomers(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    /* update delivery address for Customer */
    @PostMapping("/updateDeliveryAddress")
    public ResponseEntity<?> updateDeliveryAddress(@RequestBody DeliveryAddress deliveryAddress) {
        return ResponseEntity.ok(userService.updateDeliveryAddress(deliveryAddress));
    }

    @PostMapping("/findCustomer")
    public ResponseEntity<?> findCustomer(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(userService.findCustomer(requestParam.get("id")));
    }

    @PostMapping("/registeredToken")
    public ResponseEntity<?> registeredToken(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(userService.registeredToken(requestParam.get("customerId"),
                requestParam.get("fcmToken")));
    }
}