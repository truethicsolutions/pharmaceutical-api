package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.UsersDTO;
import com.btoc.pharma.api.model.UserPermission;
import com.btoc.pharma.api.services.UsersService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UsersController {
    @Autowired
    UsersService service;
    /* Admin Creation from Backoffice */

    @PostMapping(path = "/registerInstitute")
    public ResponseEntity<?> registerAdmin(@RequestBody UsersDTO usersDTO) {
        return ResponseEntity.ok(service.registerAdmin(usersDTO));
    }

    /**** Forgot Password for Back Office  *****/

    /* Forgot password for Admin User from back office */
    @PostMapping(path = "/forgetAdminPassword")
    public Object forgetAdminPassword(@RequestBody Map<String, String> requestParam) {
        JSONObject response = service.forgetAdminPassword(requestParam.get("username"));
        return response.toString();
    }

    /* Verify OTP for back office */
    @PostMapping(path = "/verifyAdminOtp")
    public ResponseEntity<?> verifyAdminOtp(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.verifyAdminOtp(requestParam.get("userId"), requestParam.get("otp")));
    }

    /* Reset Admin Password for back office */
    @PostMapping(path = "/resetAdminPassword")
    public ResponseEntity<?> resetAdminPassword(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.resetAdminPassword(requestParam.get("userId"), requestParam.get("password")));
    }

    /* User creation of Admin from Backoffice */
    @PostMapping(path = "/registerUser")
    public ResponseEntity<?> registerUser(@RequestBody UsersDTO usersDTO) {
        return ResponseEntity.ok(service.registerUser(usersDTO));
    }

    @PostMapping(path = "/updateInstitute")
    public ResponseEntity<?> updateAdmin(@RequestBody UsersDTO usersDTO) {
        return ResponseEntity.ok(service.updateAdmin(usersDTO));
    }

    @PostMapping(path = "/updateUser")
    public ResponseEntity<?> updateUser(@RequestBody UsersDTO usersDTO) {
        return ResponseEntity.ok(service.updateUser(usersDTO));
    }

    @PostMapping(path = "/updateAdminProfile")
    public ResponseEntity<?> updateAdminProfile(@ModelAttribute UsersDTO usersDTO) {

        return ResponseEntity.ok(service.updateAdminProfile(usersDTO));
    }

    @PostMapping(path = "/deleteInstitute")
    public ResponseEntity<?> deleteAdmin(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteAdmin(requestParam.get("id")));
    }

    @PostMapping(path = "/deleteUser")
    public ResponseEntity<?> deleteUser(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteAdmin(requestParam.get("id")));
    }

    @GetMapping(path = "/getInstitutes")
    public List<UsersDTO> getAllAdmin() {
        List<UsersDTO> list = service.getAllAdmin();
        return list;
    }

    @PostMapping(path = "/findInstitutes")
    public ResponseEntity<?> findAllAdmin(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findAllAdmin(requestParam.get("pageNumber"),
                requestParam.get("pageSize")));
    }

    @PostMapping(path = "/getUsers")
    public List<UsersDTO> getUsers(@RequestBody Map<String, String> requestParam) {
        List<UsersDTO> list = service.getAllUsers(requestParam.get("adminId"));
        return list;
    }

    @PostMapping("/findInstitute")
    public ResponseEntity<?> getAdmin(@RequestBody Map<String, String> requestParam) {
        //UsersDTO admin = service.getAdmin(requestParam.get("id"));
        return ResponseEntity.ok(service.getAdmin(requestParam.get("id")));
    }

    /* for back office - admin / institute login */
    @PostMapping(path = "/loginAdmin")
    public ResponseEntity<?> loginAdmin(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.loginAdmin(requestParam.get("username"),
                requestParam.get("password")));
    }

    /* for Back office - super admin uses this url */
    @PostMapping(path = "/loginAsAdmin")
    public ResponseEntity<?> loginAsAdmin(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.loginAsAdmin(requestParam.get("adminId")));
    }

    /* user of admin user */
    @PostMapping(path = "/loginUser")
    public ResponseEntity<?> loginUser(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.loginUser(requestParam.get("username"),
                requestParam.get("password")));
    }

    @PostMapping(path = "/loginDeliveryUser")
    public ResponseEntity<?> loginDeliveryUser(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.loginDeliveryUser(requestParam.get("mobileNumber"),
                requestParam.get("password")));
    }

    @PostMapping(path = "/addRoleConfigure")
    public ResponseEntity<?> rolesConfigure(@RequestBody UserPermission userPermission) {
        return ResponseEntity.ok(service.rolesConfigure(userPermission));
    }

    @PostMapping(path = "/findAllUsers")
    public ResponseEntity<?> findAllUsers(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findAllUsers(requestParam.get("adminId"),
                requestParam.get("pageNumber"),
                requestParam.get("pageSize")));
    }

    /* Create Token for Delivery Boy User Only mobile app */
    @PostMapping("/createToken")
    public ResponseEntity<?> createToken(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.createToken(requestParam.get("userId"),
                requestParam.get("fcmToken")));
    }
}
