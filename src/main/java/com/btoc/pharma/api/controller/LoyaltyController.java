package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.Loyalty;
import com.btoc.pharma.api.services.LoyaltyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class LoyaltyController {
    @Autowired
    LoyaltyService service;

    @PostMapping(path = "/registerLoyalty")

    public ResponseEntity<?> registerRoles(@RequestBody Loyalty loyalty) {

        return ResponseEntity.ok(service.registerLoyalty(loyalty));
    }

    @PostMapping(path = "/updateLoyalty")
    public ResponseEntity<?> updateLoyalty(@RequestBody Loyalty loyalty) {

        return ResponseEntity.ok(service.updateLoyalty(loyalty));
    }

    @PostMapping(path = "/fetchAllLoyalty")
    public ResponseEntity<?> fetchAllLoyalty(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchAllLoyalty(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }


}
