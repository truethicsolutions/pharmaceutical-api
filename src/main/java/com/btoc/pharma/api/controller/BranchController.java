package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.BranchDTO;
import com.btoc.pharma.api.services.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class BranchController {

    @Autowired
    private BranchService service;

    @PostMapping("/createBranch")
    public ResponseEntity<?> createBranch(@RequestBody BranchDTO branchDTO) {
        return ResponseEntity.ok(service.createBranch(branchDTO));
    }

    @PostMapping("/updateBranch")
    public ResponseEntity<?> updateBranch(@RequestBody BranchDTO branchDtO) {
        return ResponseEntity.ok(service.updateBranch(branchDtO));
    }

    @PostMapping("/getBranch")
    public ResponseEntity<?> getBranch(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getBranch(requestParam.get("adminId")));
    }

    @PostMapping("/getAllBranch")
    public ResponseEntity<?> getAllBranch(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getAllBranch(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    @PostMapping("/findBranch")
    public ResponseEntity<?> findBranch(@RequestBody Map<String, String> requestParam) {
        //BranchDTO branchDTO = service.findBranch(requestParam.get("branchId"));
        return ResponseEntity.ok(service.findBranch(requestParam.get("id")));
    }

    @PostMapping("/deleteBranch")
    public ResponseEntity<?> deleteBranch(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteBranch(requestParam.get("id")));
    }
}
