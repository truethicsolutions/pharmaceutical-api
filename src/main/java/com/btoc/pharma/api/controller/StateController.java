package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.Outlet;
import com.btoc.pharma.api.model.States;
import com.btoc.pharma.api.services.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class StateController {

    @Autowired
    private StateService stateService;

    @PostMapping("/getState")
    public List<States> getState(@RequestBody Map<String, String> requestParam) {
        List<States> states = stateService.getState(requestParam.get("countryId"));
        return states;
    }
}
