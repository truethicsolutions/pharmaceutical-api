package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.OutletDTO;
import com.btoc.pharma.api.model.Outlet;
import com.btoc.pharma.api.services.OutletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class OutletController {
    @Autowired
    private OutletService service;

    @PostMapping("/createOutlet")
    public ResponseEntity<?> createOutlet(@RequestBody OutletDTO outletDTO) {
        return ResponseEntity.ok(service.createOutlet(outletDTO));
    }

    @PostMapping("/updateOutlet")
    public ResponseEntity<?> updateOutlet(@RequestBody OutletDTO outletDTO) {
        return ResponseEntity.ok(service.updateOutlet(outletDTO));
    }

    @PostMapping("/deleteOutlet")
    public ResponseEntity<?> deleteOutlet(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteOutlet(requestParam.get("id")));
    }

    @PostMapping("/getBranchOutlets")
    public List<Outlet> getBranchOutlets(@RequestBody Map<String, String> requestParam) {
        List<Outlet> outlets = service.getBranchOutlets(requestParam.get("branchId"));
        return outlets;
    }

    @PostMapping("/findOutlet")
    public Outlet getOutlet(@RequestBody Map<String, String> requestParam) {
        Outlet outlet = service.getOutlet(requestParam.get("id"));
        return outlet;
    }

    /*  Remove this */
    @PostMapping("/getAllOutlet")
    public List<OutletDTO> getAllOutlets(@RequestBody Map<String, String> requestParam) {
        List<OutletDTO> list = service.getAllOutlets(requestParam.get("adminId"));
        return list;
    }

    @PostMapping("/findAllOutlet")
    public ResponseEntity<?> findAllOutlet(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findAllOutlet(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    @PostMapping("/findBranchOutlet")
    public List<OutletDTO> getBranchOutlet(@RequestBody Map<String, String> requestParam) {
        List<OutletDTO> list = service.getBranchOutlet(requestParam.get("branchId"));
        return list;
    }
}
