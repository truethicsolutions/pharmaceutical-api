package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.GroupDTO;
import com.btoc.pharma.api.services.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class GroupController {
    @Autowired
    private GroupService service;

    @PostMapping(path = "/createGroup")
    public ResponseEntity<?> createGroup(@RequestBody GroupDTO groupDTO) {
        return ResponseEntity.ok(service.createGroup(groupDTO));
    }

    @PostMapping(path = "/updateGroup")
    public ResponseEntity<?> updateGroup(@RequestBody GroupDTO groupDTO) {
        return ResponseEntity.ok(service.updateGroup(groupDTO));
    }

    @PostMapping(path = "/deleteGroup")
    public ResponseEntity<?> deleteGroup(@RequestBody Map<String, String> requestParam) {

        return ResponseEntity.ok(service.deleteGroup(requestParam.get("id")));
    }

    @PostMapping(path = "/getGroup")
    public List<GroupDTO> getAllGroup(@RequestBody Map<String, String> requestParam) {
        List<GroupDTO> list = service.getAllGroup(requestParam.get("adminId"));
        return list;
    }

    @PostMapping(path = "/fetchAllGroup")
    public ResponseEntity<?> fetchAllGroup(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchAllGroup(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }
}
