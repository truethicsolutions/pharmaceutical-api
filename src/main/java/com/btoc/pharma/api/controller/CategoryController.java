package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.CategoryDTO;
import com.btoc.pharma.api.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CategoryController {

    @Autowired
    private CategoryService service;

    @PostMapping("/createCategory")
    public ResponseEntity<?> createCategory(@ModelAttribute CategoryDTO categoryDTO) {
        return ResponseEntity.ok(service.createCategory(categoryDTO));
    }

    @PostMapping("/updateCategory")
    public ResponseEntity<?> updateCategory(@ModelAttribute CategoryDTO categoryDTO) {
        return ResponseEntity.ok(service.updateCategory(categoryDTO));
    }

    @PostMapping("/deleteCategory")
    public ResponseEntity<?> deleteCategory(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.deleteCategory(requestParam.get("id")));

    }

    @PostMapping("/getCategories")
    public List<CategoryDTO> getAllCategories(@RequestBody Map<String, String> requestParam) {
        List<CategoryDTO> categoryDTOList = service.getAllCategories(requestParam.get("adminId"));
        return categoryDTOList;
    }

    @PostMapping("/fetchAllCategories")
    public ResponseEntity<?> fetchAllCategories(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchAllCategories(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    /* for mobile App */

    @PostMapping("/getTopCategories")
    public ResponseEntity<?> getTopCategories(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getTopCategories(requestParam.get("adminId")));
    }
}
