package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.model.Note;
import com.btoc.pharma.api.services.FirebaseMessagingService;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PushNotificationController {

    @Autowired
    FirebaseMessagingService firebaseService;

    /* @RequestMapping("/send-notification")
     @ResponseBody
     public String sendNotification(@RequestBody Map<String,
             String> requestParam) throws FirebaseMessagingException {
         return firebaseService.sendNotification(requestParam.get("subject"), requestParam.get("content"),
                 requestParam.get("token"));
     }*/
   /* @PostMapping("/send-notification")
    @ResponseBody
    public String sendNotification(@RequestBody Map<String, String> requestParam) throws FirebaseMessagingException {
        return firebaseService.sendNotification(requestParam.get("subject"),requestParam.get("content"),
                                                requestParam.get("token"));
    }*/
    @PostMapping("/send-notification")
    @ResponseBody
    public String sendNotification(@RequestBody Note note) throws FirebaseMessagingException {
        return firebaseService.sendNotification(note, "fcmToken");
    }

}
