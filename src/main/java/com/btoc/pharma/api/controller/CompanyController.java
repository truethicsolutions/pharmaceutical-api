package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.dto.CompanyDTO;
import com.btoc.pharma.api.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CompanyController {

    @Autowired
    private CompanyService service;

    @PostMapping(path = "/createCompany")
    public ResponseEntity<?> registerCompany(@ModelAttribute CompanyDTO companyDTO) {
        //httpHeaders.add("ACCESS_CONTROL_ALLOW_ORIGIN","*");
        //	httpHeaders.setAccessControlAllowOrigin("ACCESS_CONTROL_ALLOW_ORIGIN");
        return ResponseEntity.ok(service.registerCompany(companyDTO));
    }

    @PostMapping(path = "/updateCompany")
    public ResponseEntity<?> updateCompany(@ModelAttribute CompanyDTO companyDTO) {
        return ResponseEntity.ok(service.updateCompany(companyDTO));
    }
    @PostMapping(path = "/deleteCompany")
    public ResponseEntity<?> deleteCompany(@RequestBody Map<String, String> requestParam) {

        return ResponseEntity.ok(service.deleteCompany(requestParam.get("id")));
    }
    @PostMapping(path = "/getCompany")
    public List<CompanyDTO> getCompanies(@RequestBody Map<String, String> requestParam) {
        List<CompanyDTO> list = service.getCompanies(requestParam.get("adminId"));
        return list;
    }

    /**
     * for Back office
     **/
    @PostMapping(path = "/fetchAllCompany")
    public ResponseEntity<?> fetchAllCompany(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchAllCompany(requestParam.get("adminId"),
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    /**
     * for mobile app - Brand wise
     **/
    @PostMapping(path = "/fetchCompanies")
    public ResponseEntity<?> fetchCompanies(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.fetchCompanies(requestParam.get("adminId")));
    }

    /**
     * for mobile app - Fetured Brand wise
     **/
    @PostMapping(path = "/featuredCompanies")
    public ResponseEntity<?> featuredCompanies(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.featuredCompanies(requestParam.get("adminId")));
    }
}

