package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dto.SubCategoryDTO;
import com.btoc.pharma.api.services.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SubCategoryController {
    @Autowired
    private SubCategoryService service;

    @PostMapping("/creatSubCategory")
    @ResponseBody
    public ResponseEntity<ResponseMessage> createSubCategory(@ModelAttribute SubCategoryDTO subCategoryDTO) {
        ResponseEntity<ResponseMessage> response = service.createSubCategory(subCategoryDTO);
        return response;
    }

    @PostMapping("/updateSubCategory")
    public ResponseEntity<ResponseMessage> updateSubCategory(@ModelAttribute SubCategoryDTO subCategoryDTO) {
        ResponseEntity<ResponseMessage> response = service.updateSubCategory(subCategoryDTO);
        return response;
    }

    @GetMapping("/getSubCategory/{id}/{categoryId}")
    public SubCategoryDTO getSubCategory(@PathVariable String id, @PathVariable String categoryId) {
        SubCategoryDTO category = service.getSubCategory(id, categoryId);
        return category;
    }

    @GetMapping("/getAllSubCategories/{categoryId}")
    public List<SubCategoryDTO> getAllSubCategories(@PathVariable String categoryId) {
        List<SubCategoryDTO> subcategoryDTOList = service.getAllSubCategories(categoryId);
        return subcategoryDTOList;
    }
}
