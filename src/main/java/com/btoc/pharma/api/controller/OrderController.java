package com.btoc.pharma.api.controller;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dto.*;
import com.btoc.pharma.api.services.OrderService;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class OrderController {

    @Autowired
    private OrderService service;

    /* for non prescribed product orders */
    @PostMapping("/placeNonPrescribedOrder")
    public ResponseEntity<ResponseMessage> createOrder(@RequestBody OrderDTO orderDTO) {
        ResponseEntity<ResponseMessage> response = service.createOrder(orderDTO);
        return response;
    }

    /* upload Prescription from mobile app */
    @PostMapping("/uploadPrescription")
    public ResponseEntity<?> uploadPrescription(@ModelAttribute OrderDTO orderDTO) {
        return ResponseEntity.ok(service.uploadPrescription(orderDTO));
    }

    /*Upload prescription from web responsive page */
    @PostMapping("/orderResponsive")
    public ResponseEntity<?> orderResponsive(@ModelAttribute OrderResponsiveDTO orderResponsiveDTO) {
        return ResponseEntity.ok(service.orderResponsive(orderResponsiveDTO));
    }

    /* Update Orders of Prescibed Products from BackOffice - Generate Order from Back office */
    @PostMapping("/placePrescibedOrder")
    public ResponseEntity<?> placePrescibedOrder(@RequestBody ProductOrderListDTO productList)
            throws FirebaseMessagingException {
        return ResponseEntity.ok(service.placePrescibedOrder(productList));
    }

    /* Update Orders of Prescibed Products from Mobile App */
    @PostMapping("/updateOrder")
    public ResponseEntity<?> updateOrder(@RequestBody UpdateOrderDTO updateOrderDTO) {
        return ResponseEntity.ok(service.updateOrder(updateOrderDTO));
    }

    /* All Orders of Institutes from Back office */
    @PostMapping("/getAllOrder")
    public ResponseEntity<?> getAllOrder(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getAllOrder(requestParam.get("adminId")));
    }

    /* Get My Orders from Mobile App */
    @PostMapping("/getMyOrders")
    public ResponseEntity<?> getMyOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getMyOrders(requestParam.get("adminId"),
                requestParam.get("outletId"), requestParam.get("customerId")));
    }

    /* Get Prescribed Orders from Back office */
    @PostMapping("/getPOrders")
    public ResponseEntity<?> getPOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getPOrders(requestParam.get("adminId"), "prescribed",
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    /* Get Generated Order Details from Back office */
    @PostMapping("/getGeneratedOrders")
    public ResponseEntity<?> getGeneratedOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getGeneratedOrders(requestParam.get("id")));
    }

    /* Get Non Prescribed Orders from Back office */
    @PostMapping("/getNPOrders")
    public ResponseEntity<?> getNPOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getPOrders(requestParam.get("adminId"), "non-prescribed",
                requestParam.get("pageNumber"), requestParam.get("pageSize")));
    }

    /* specific Order , Back office */
    @PostMapping("/findOrder")
    public ResponseEntity<?> findOrder(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findOrder(requestParam.get("id")));
    }

    /* customer previous Order , Back office */
    @PostMapping("/findCustomerPrevOrder")
    public ResponseEntity<?> findCustomerPrevOrder(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findCustomerPrevOrder(requestParam.get("id")));
    }

    /* for Back office */
    @PostMapping("/findProduct")
    public ResponseEntity<?> findProduct(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.findProduct(requestParam.get("productId")));
    }

    /*** Delivery Process ***/

    /* get Delivery Boy List for Back office */
    @PostMapping("/getDeliveryBoy")
    public ResponseEntity<?> getDeliveryBoy(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getDeliveryBoy(requestParam.get("adminId")));
    }

    /* Assign Delivery Boy from Back office */
    @PostMapping("/assignDeliveryBoy")
    public ResponseEntity<?> assignDeliveryBoy(@RequestBody Map<String, String> requestParam)
            throws FirebaseMessagingException {
        return ResponseEntity.ok(service.assignDeliveryBoy(requestParam.get("id"),
                requestParam.get("userId"), requestParam.get("deliveryBoyId")));
    }

    /* Get confirmed order list of Delivery Boy from mobile app */
    @PostMapping("/getDeliveryOrders")
    public ResponseEntity<?> getDeliveryOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getDeliveryOrders(requestParam.get("adminId"),
                requestParam.get("deliveryBoyId")));
    }

    /* Get Delivered order list of Delivery Boy from mobile app */
    @PostMapping("/getDeliveredOrders")
    public ResponseEntity<?> getDeliveredOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getDeliveredOrders(requestParam.get("adminId"),
                requestParam.get("deliveryBoyId")));
    }

    /* Get All order list of Delivery Boy,  Mobile app */
    @PostMapping("/getAllDeliveryOrders")
    public ResponseEntity<?> getAllDeliveryOrders(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.getAllDeliveryOrders(requestParam.get("adminId"),
                requestParam.get("deliveryBoyId")));
    }

    /* update order after order delivered to customer from Delivery Boy,  Mobile App  */
    @PostMapping("/updateOrderForDelivery")
    public ResponseEntity<?> updateOrderForDelivery(@RequestBody Map<String, String> requestParam) throws FirebaseMessagingException {
        return ResponseEntity.ok(service.updateOrderForDelivery(requestParam.get("orderId")));
    }

    /*** Invoice Bill against Order, back office ***/
    @PostMapping("/generateInvoice")
    public ResponseEntity<?> generateInvoice(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.generateInvoice(requestParam.get("orderId")));
    }

    /*** Reports Section ***/
    @PostMapping("/orderDashboardReports")
    public ResponseEntity<?> dashboardOrderReports(@RequestBody Map<String, String> requestParam) {
        return ResponseEntity.ok(service.dashboardOrderReports(requestParam.get("adminId")));
    }

    /*** order between dates  ***/
    @PostMapping("/getAllOrdersBetweenDates")
    public ResponseEntity<?> getAllOrdersBetweenDates(@RequestBody ReportsDTO reports) {
        return ResponseEntity.ok(service.getAllOrdersBetweenDates(reports));
    }

    /* Delivery Reports  */
    @PostMapping("/DeliveryBoyReports")
    public ResponseEntity<?> DeliveryBoyReports(@RequestBody ReportsDTO reportsDTO) {
        return ResponseEntity.ok(service.DeliveryBoyReports(reportsDTO));
    }

    /* Customer Reports  */
    @PostMapping("/CustomerReports")
    public ResponseEntity<?> CustomerReports(@RequestBody ReportsDTO reportsDTO) {
        return ResponseEntity.ok(service.CustomerReports(reportsDTO));
    }

    /*** End of Report ***/

    /***  For Responsive Web Page  ***/

    @PostMapping("/orders")
    public ResponseEntity<?> orders(@RequestBody ResponsiveOrderUpdateDTO responseDTO) throws Exception {
        return ResponseEntity.ok(service.orders(responseDTO));
    }

    /* Payment Status api */
    @PostMapping("/checkPaymentStatus")
    public ResponseEntity<?> checkPaymentStatus(@RequestBody Map<String, String> requestParam)
            throws Exception {
        return ResponseEntity.ok(service.checkPaymentStatus(requestParam.get("order_id"),
                requestParam.get("razorpay_order_id"), requestParam.get("razorpay_payment_id"),
                requestParam.get("razorpay_signature")));
    }
}
