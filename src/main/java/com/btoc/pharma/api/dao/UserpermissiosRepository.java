package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.UserPermission;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserpermissiosRepository extends MongoRepository<UserPermission, String> {

    UserPermission findByUserIdAndRoleId(String id, String roleId);
}
