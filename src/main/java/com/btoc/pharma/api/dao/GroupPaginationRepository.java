package com.btoc.pharma.api.dao;


import com.btoc.pharma.api.model.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GroupPaginationRepository extends PagingAndSortingRepository<Group, String> {
    Page<Group> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}
