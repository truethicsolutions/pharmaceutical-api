package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Trade;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TradeRepository extends MongoRepository<Trade, String> {

    List<Trade> findAllByStatus(int status);

    Trade findByTradeName(String tradeId);

    Trade findByTradeNameIgnoreCase(String tradeId);

    Trade findByIdAndStatus(String tradeId, int i);
}
