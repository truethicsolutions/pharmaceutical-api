package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Roles;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RolePaginationRepository extends PagingAndSortingRepository<Roles, String> {

    Page<Roles> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}
