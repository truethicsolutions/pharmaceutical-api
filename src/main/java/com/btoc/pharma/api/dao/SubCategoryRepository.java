package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.SubCategory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface SubCategoryRepository extends MongoRepository<SubCategory, String> {
    SubCategory findByIdAndCategoryId(String id, String categoryId);

    List<SubCategory> findByCategoryId(String CategoryId);
}
