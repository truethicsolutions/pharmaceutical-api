package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Country;
import com.btoc.pharma.api.model.States;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface StateRepository extends MongoRepository<States,String> {
    List<States> findAllByCountryId(String countryId);

}
