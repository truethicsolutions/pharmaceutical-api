package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Company;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CompanyRepository extends MongoRepository<Company, String> {

   /* List<Company> findByUserIdAndBranchIdAndOutletIdAndStatus(String userId, String branchId,
                                                              String outletId, int i);
*/
   List<Company> findByUserIdAndStatus(String userId, int i);

    Company findByIdAndUserIdAndStatus(String id, String userId, int i);

    Company findByIdAndStatus(String companyId, int i);

    List<Company> findByAdminIdAndStatus(String userId, int i);

    Company findByCompanyNameAndStatus(String key, int i);

    Company findByCompanyNameAndAdminIdAndStatus(String companyName, String adminId, int i);

    Company findByCompanyNameIgnoreCaseAndStatus(String key, int i);

    List<Company> findTop7ByAdminIdAndStatus(String adminId, int i);

    //  List<Company> findByAdminIdAndBranchIdAndOutletIdAndStatus(String userId, String branchId, String outletId, int i);
}
