package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AdminRepository extends MongoRepository<Admin, String> {
    Admin findByUsernameAndPassword(String username, String password);

    Admin findByUsername(String username);

}
