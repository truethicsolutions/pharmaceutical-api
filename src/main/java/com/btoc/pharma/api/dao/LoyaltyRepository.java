package com.btoc.pharma.api.dao;


import com.btoc.pharma.api.model.Loyalty;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoyaltyRepository extends MongoRepository<Loyalty, String> {

    Loyalty findByIdAndStatus(String id, int i);

//    Optional<Loyalty> findByIdAndStatus(String roleId, int i);
//
//    List<Loyalty> findByUserIdAndStatus(String userId, int i);
//
//    List<Loyalty> findByAdminIdAndStatus(String userId, int i);
}
