package com.btoc.pharma.api.dao;


import com.btoc.pharma.api.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ProductPaginationRepository extends PagingAndSortingRepository<Product, String> {
    Page<Product> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}

