package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Loyalty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LoyaltyPaginationRepository extends PagingAndSortingRepository<Loyalty, String> {
    Page<Loyalty> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}
