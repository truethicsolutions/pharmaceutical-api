package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BranchPaginationRepository extends PagingAndSortingRepository<Branch, String> {
    Page<Branch> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}
