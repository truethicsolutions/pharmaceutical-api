package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UsersPaginationRepository extends PagingAndSortingRepository<Users, String> {

    Page<Users> findByUserTypeAndStatus(int i, int i1, Pageable pageable);

    Page<Users> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);

    Page<Users> findByAdminIdAndUserTypeAndStatus(String adminId, int i, int i1, Pageable pageable);
}
