package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Filters;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FiltersRepository extends MongoRepository<Filters, String> {
    Filters findByIdAndCategoryId(String id, String categoryId);

    List<Filters> findByCategoryId(String categoryId);
}
