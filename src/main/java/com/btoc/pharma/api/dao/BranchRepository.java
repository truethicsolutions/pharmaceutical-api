package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Branch;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface BranchRepository extends MongoRepository<Branch, String> {

    List<Branch> findAllByUserId(String userId);

    Branch findByIdAndStatus(String branchId, int i);

    List<Branch> findAllByUserIdAndStatus(String userId, int i);

    Branch findByIdAndUserIdAndStatus(String branchId, String userId, int i);

    List<Branch> findByAdminIdAndStatus(String userId, int i);
}
