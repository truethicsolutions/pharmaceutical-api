package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Policies;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PoliciesRepository extends MongoRepository<Policies, String> {

}
