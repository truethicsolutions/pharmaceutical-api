package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CategoryRepository extends MongoRepository<Category, String> {

    Category findByIdAndStatus(String id, int i);

    List<Category> findByUserIdAndStatus(String userId, int i);

    List<Category> findByAdminIdAndStatus(String userId, int i);

    Category findByCategoryNameAndStatus(String key, int i);

    List<Category> findTop8ByAdminIdAndStatus(String adminId, int i);

    Category findByCategoryNameAndAdminIdAndStatus(String categoryName, String adminId, int i);

    Category findByCategoryNameIgnoreCaseAndStatus(String key, int i);
}
