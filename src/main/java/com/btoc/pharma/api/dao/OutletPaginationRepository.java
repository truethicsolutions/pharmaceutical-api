package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Outlet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OutletPaginationRepository extends PagingAndSortingRepository<Outlet, String> {
    Page<Outlet> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);
}
