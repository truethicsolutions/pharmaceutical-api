package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface OrderPaginationRepository extends PagingAndSortingRepository<Order, String> {


    Page<Order> findByAdminIdAndOrderTypeAndStatusOrderByOrderDateDesc(String adminId, String prescribed,
                                                                       int i, Pageable pageable);

    Page<Order> findByAdminIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId, String status,
                                                                        int i, Date dateFrom, Date dateTo,
                                                                        Pageable pageable);

    Page<Order> findByAdminIdAndCustomerIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId,
                                                                                     String customerId, String status, int i,
                                                                                     Date dateFrom, Date dateTo,
                                                                                     Pageable pageable);

    Page<Order> findByAdminIdAndDeliveryBoyIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId,
                                                                                        String deliveryBoyId, String status, int i, Date dateFrom,
                                                                                        Date dateTo, Pageable pageable);
}
