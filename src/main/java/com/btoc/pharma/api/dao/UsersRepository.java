package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Users;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UsersRepository extends MongoRepository<Users, String> {
    List<Users> findAllByStatus(int status);

    Users findByUsernameAndPasswordAndStatus(String username, String password, int status);


    Users findByIdAndStatus(String id, int i);

    List<Users> findByAdminIdAndStatus(String adminId, int i);

    List<Users> findByUserTypeAndStatus(int i, int i1);

    List<Users> findByAdminIdAndUserTypeAndStatus(String adminId, int i, int i1);

    Users findByUsernameAndPasswordAndUserTypeAndStatus(String username, String password, int i, int i1);

    List<Users> findByRoleIdAndUserTypeAndStatus(String roleId, int i, int i1);

    Users findByMobileNumberAndPasswordAndUserTypeAndStatus(long mobile, String password, int i, int i1);

    Users findByIdAndUserTypeAndStatus(String adminId, int i, int i1);

    Users findByUsernameAndUserTypeAndStatus(String username, int i, int i1);

    Users findByUsernameIgnoreCaseAndUserTypeAndStatus(String username, int i, int i1);


    Users findByMobileNumberAndUserTypeAndStatus(long mobileNumber, int i, int i1);

    Users findByUsernameAndStatus(String username, int i);

    // Users findByUserIdAndStatus(String userId, int i);
}
