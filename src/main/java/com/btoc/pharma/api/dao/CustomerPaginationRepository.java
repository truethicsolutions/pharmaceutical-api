package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerPaginationRepository extends PagingAndSortingRepository<Customer, String> {

    Page<Customer> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);

}
