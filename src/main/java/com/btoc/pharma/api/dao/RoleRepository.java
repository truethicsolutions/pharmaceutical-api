package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Roles;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface RoleRepository extends MongoRepository<Roles, String> {
    List<Roles> findAllByStatus(int status);

    Roles findByRoleName(String roleName);

    Optional<Roles> findByIdAndStatus(String roleId, int i);

    List<Roles> findByUserIdAndStatus(String userId, int i);

    List<Roles> findByAdminIdAndStatus(String userId, int i);

    Roles findByRoleNameIgnoreCaseAndStatus(String delivery_boy, int i);

    Roles findByRoleNameAndAdminIdAndStatus(String roleName, String adminId, int i);

   /* @Query("select u from Roles u where u.roleName = :firstname or u.roleName = :lastname")
    Roles findByRoleName(@Param("firstname") String firstname,
                         @Param("lastname") String lastname);*/
}
