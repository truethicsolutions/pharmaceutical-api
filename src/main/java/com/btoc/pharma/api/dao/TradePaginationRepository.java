package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Trade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TradePaginationRepository extends PagingAndSortingRepository<Trade, String> {
    Page<Trade> findAllByStatus(int i, Pageable pageable);
}
