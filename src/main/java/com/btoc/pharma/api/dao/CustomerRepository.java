package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    Customer findByMobileNumberAndPassword(long mobileNumber, String password);

    //String findByMobileNumber(long mobileNumber);
    Customer findByMobileNumber(Long mobileNumber);

    Customer findByMobileNumberAndStatus(long parseLong, int i);

    Customer findByIdAndStatus(String customerId, int i);

    List<Customer> findByAdminIdAndStatus(String adminId, int i);
}
