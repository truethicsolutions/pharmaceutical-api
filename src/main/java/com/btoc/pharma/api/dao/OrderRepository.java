package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface OrderRepository extends MongoRepository<Order, String> {

    List<Order> findByAdminIdAndOutletIdAndStatus(String adminId, String outletId, int i);

    Order findByIdAndStatus(String id, int i);

    List<Order> findByAdminIdAndStatus(String adminId, int i);

    List<Order> findByAdminIdAndOutletIdAndCustomerIdAndStatus(String adminId, String outletId,
                                                               String customerId, int i);

    List<Order> findByAdminIdAndOutletIdAndCustomerIdAndStatusOrderByOrderDateDesc(String adminId,
                                                                                   String outletId, String customerId, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndOrderStatusAndStatus(String adminId,
                                                                     String deliveryBoyId, String confirmed, int i);

    List<Order> findByCustomerIdAndOrderStatusAndStatus(String id, String confirmed, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndStatus(String adminId, String deliveryBoyId, int i);

    Order findByIdAndOrderStatusAndStatus(String orderId, String confirmed, int i);

    List<Order> findByAdminIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId,
                                                                        String orderStatus, int i, Date orderFrom, Date orderTo);

    List<Order> findByAdminIdAndOrderDateAndStatus(String adminId, LocalDateTime todaysDate, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId,
                                                                                        String deliveryBoyId, String status, int i, Date dateFrom,
                                                                                        Date dateTo);

    List<Order> findByAdminIdAndCustomerIdAndOrderStatusAndStatusAndOrderDateBetween(String adminId,
                                                                                     String customerId, String status, int i, Date dateFrom, Date dateTo);

    List<Order> findByAdminIdAndStatusAndOrderDateBetween(String adminId, int i,
                                                          Date dateFrom, Date dateTo);

    List<Order> findTop3ByCustomerIdAndOrderStatusAndOrderDateBeforeAndStatusOrderByOrderDateDesc(String id,
                                                                                                  String delivered, LocalDateTime orderDate, int i);

    List<Order> findByAdminIdAndCustomerIdAndStatusOrderByOrderDateDesc(String adminId, String customerId, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndOrderStatusAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, String delivered, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndDeliveryStatusAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, String confirmed, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndDeliveryStatusIgnoreCaseAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, String assigned, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndDeliveryStatusIgnoreCaseAndOrderStatusNotAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, String assigned, String delivered, int i);

    List<Order> findByAdminIdAndDeliveryBoyIdAndOrderStatusIgnoreCaseAndStatusOrderByOrderDateDesc(String adminId, String deliveryBoyId, String delivered, int i);
}
