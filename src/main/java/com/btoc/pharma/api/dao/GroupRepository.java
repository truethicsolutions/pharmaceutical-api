package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface GroupRepository extends MongoRepository<Group, String> {
    //
    // List<Group> findByUserIdAndCompanyIdAndStatus(String userId, String companyId, int i);

    List<Group> findByUserIdAndStatus(String userId, int i);

    Group findByIdAndStatus(String id, int i);

    List<Group> findByAdminIdAndStatus(String userId, int i);

    Group findByGroupNameAndStatus(String key, int i);

    Group findByGroupNameIgnoreCaseAndStatus(String key, int i);

    Group findByGroupNameIgnoreCaseAndAdminIdAndStatus(String groupName, String adminId, int i);

    //  List<Group> findByAdminIdAndCompanyIdAndStatus(String userId, String companyId, int i);
}
