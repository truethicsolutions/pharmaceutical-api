package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Country;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CountryRepository extends MongoRepository<Country, String> {

    List<Country> findAllByCountryId(String countryId);
}
