package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Outlet;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface OutletRepository extends MongoRepository<Outlet, String> {

    Outlet findByIdAndStatus(String outletId, int i);

    List<Outlet> findAllByBranchId(String branchId);

    List<Outlet> findAllByUserId(String userId);

    List<Outlet> findAllByUserIdAndStatus(String userId, int i);

    Outlet findByIdAndUserIdAndBranchIdAndStatus(String outletId, String userId, String branchId, int i);

    List<Outlet> findAllByBranchIdAndStatus(String branchId, int i);

    List<Outlet> findAllByAdminIdAndStatus(String userId, int i);
}