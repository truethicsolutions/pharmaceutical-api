package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.City;
import com.btoc.pharma.api.model.Trade;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CityRepository  extends MongoRepository<City, String> {

    List<City> findAllByStateCode(String stateCode);
}
