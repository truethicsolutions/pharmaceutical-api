package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProductRepository extends MongoRepository<Product, String> {


    Product findByIdAndStatus(String id, int i);

    List<Product> findByAdminIdAndStatus(String id, int i);

    List<Product> findByCompanyIdAndStatus(String companyId, int i);

    List<Product> findByCategoryIdAndStatus(String id, int i);

    List<Product> findByAdminIdAndProductNameIgnoreCaseContainingAndStatus(String adminId, String regexString, int s);

    List<Product> findTop10ByAdminIdAndCompanyIdAndStatusOrderByIdDesc(String id, String key, int i);

    List<Product> findTop10ByAdminIdAndTopTrendingAndStatus(String id, String yes, int i);

    List<Product> findByAdminIdAndTopTrendingAndStatus(String id, String yes, int i);
}
