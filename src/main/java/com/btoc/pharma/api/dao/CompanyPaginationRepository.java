package com.btoc.pharma.api.dao;

import com.btoc.pharma.api.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CompanyPaginationRepository extends PagingAndSortingRepository<Company, String> {
    Page<Company> findByAdminIdAndStatus(String adminId, int i, Pageable pageable);

    List<Company> findByAdminIdAndStatus(String adminId, int i);
}
