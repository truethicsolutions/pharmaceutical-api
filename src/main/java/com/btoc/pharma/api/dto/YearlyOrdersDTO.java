package com.btoc.pharma.api.dto;

public class YearlyOrdersDTO {
    private int totalOrders;
    private int pendingOrders;
    private int generatedOrders;
    private int placedOrders;
    private int confirmedOrders;
    private int completedOrders;
    private int cancelledOrders;
    private double total_revenue;
    private double revenue_by_online;
    private double revenue_by_cod;


    public YearlyOrdersDTO() {
    }

    public YearlyOrdersDTO(int totalOrders, int pendingOrders, int generatedOrders, int placedOrders, int confirmedOrders, int completedOrders, int cancelledOrders,
                           double total_revenue, double revenue_by_online, double revenue_by_cod) {
        this.totalOrders = totalOrders;
        this.pendingOrders = pendingOrders;
        this.generatedOrders = generatedOrders;
        this.placedOrders = placedOrders;
        this.confirmedOrders = confirmedOrders;
        this.completedOrders = completedOrders;
        this.cancelledOrders = cancelledOrders;
        this.total_revenue = total_revenue;
        this.revenue_by_online = revenue_by_online;
        this.revenue_by_cod = revenue_by_cod;
    }

    public int getTotalOrders() {
        return totalOrders;
    }

    public void setTotalOrders(int totalOrders) {
        this.totalOrders = totalOrders;
    }

    public int getPendingOrders() {
        return pendingOrders;
    }

    public void setPendingOrders(int pendingOrders) {
        this.pendingOrders = pendingOrders;
    }

    public int getGeneratedOrders() {
        return generatedOrders;
    }

    public void setGeneratedOrders(int generatedOrders) {
        this.generatedOrders = generatedOrders;
    }

    public int getPlacedOrders() {
        return placedOrders;
    }

    public void setPlacedOrders(int placedOrders) {
        this.placedOrders = placedOrders;
    }

    public int getConfirmedOrders() {
        return confirmedOrders;
    }

    public void setConfirmedOrders(int confirmedOrders) {
        this.confirmedOrders = confirmedOrders;
    }

    public int getCompletedOrders() {
        return completedOrders;
    }

    public void setCompletedOrders(int completedOrders) {
        this.completedOrders = completedOrders;
    }

    public int getCancelledOrders() {
        return cancelledOrders;
    }

    public void setCancelledOrders(int cancelledOrders) {
        this.cancelledOrders = cancelledOrders;
    }

    public double getTotal_revenue() {
        return total_revenue;
    }

    public void setTotal_revenue(double total_revenue) {
        this.total_revenue = total_revenue;
    }

    public double getRevenue_by_online() {
        return revenue_by_online;
    }

    public void setRevenue_by_online(double revenue_by_online) {
        this.revenue_by_online = revenue_by_online;
    }

    public double getRevenue_by_cod() {
        return revenue_by_cod;
    }

    public void setRevenue_by_cod(double revenue_by_cod) {
        this.revenue_by_cod = revenue_by_cod;
    }
}
