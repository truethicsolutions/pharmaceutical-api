package com.btoc.pharma.api.dto;

public class TradeDTO {
    private String id;
    private String tradeName;
    private int status;
    private String message;
    private String responseStatus;

    public TradeDTO(String id, String tradeName, int status, String message, String responseStatus) {
        this.id = id;
        this.tradeName = tradeName;
        this.status = status;
        this.message = message;
        this.responseStatus = responseStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TradeDTO() {
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }
}
