package com.btoc.pharma.api.dto;

import lombok.Data;

@Data
public class CustomerReportDTO {

    private String customerName;
    private String email;
    private long mobileNumber;
    private String orderStatus;
    private long TotalOrder;
}
