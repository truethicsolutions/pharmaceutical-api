package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.UserPolicies;

import java.util.List;

public class RolesConfigureDTO {
    private String userId;
    private List<UserPolicies> policies;
    private String responseStatus;//response parameter
    private String message;

    public RolesConfigureDTO(String userId, List<UserPolicies> policies,
                             String responseStatus, String message) {
        this.userId = userId;
        this.policies = policies;
        this.responseStatus = responseStatus;
        this.message = message;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<UserPolicies> getPolicies() {
        return policies;
    }

    public void setPolicies(List<UserPolicies> policies) {
        this.policies = policies;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
