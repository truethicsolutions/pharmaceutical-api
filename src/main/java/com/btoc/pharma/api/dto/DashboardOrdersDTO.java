package com.btoc.pharma.api.dto;

import lombok.Data;

@Data
public class DashboardOrdersDTO {

    private int totalOrders;
    private int pendingOrders;
    private int generatedOrders;
    private int placedOrders;
    private int confirmedOrders;
    private int completedOrders;
    private int cancelledOrders;
    private double total_revenue;
    private double revenue_by_online;
    private double revenue_by_cod;
    /* Todays Count */
    private int todays_totalOrders;
    private int todays_pendingOrders;
    private int todays_generatedOrders;
    private int todays_placedOrders;
    private int todays_confirmedOrders;
    private int todays_completedOrders;
    private int todays_cancelledOrders;
    private double todays_total_revenue;
    private double todays_revenue_by_online;
    private double todays_revenue_by_cod;

    private MonthlyOrdersDTO monthlyOrders;
    private YearlyOrdersDTO yearlyOrders;

}

