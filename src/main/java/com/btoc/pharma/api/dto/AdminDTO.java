package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.AdminResponse;
import lombok.Data;

@Data
public class AdminDTO {
    private String message;
    private int responseCode;
    private AdminResponse data = new AdminResponse();


}
