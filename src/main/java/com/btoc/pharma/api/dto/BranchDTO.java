package com.btoc.pharma.api.dto;

import lombok.Data;

@Data
public class BranchDTO {

    private String id;
    private String userId;// here we are mapping Institute Id
    private String adminId;
    private String branchname;
    private String country;
    private String state;
    private String district;
    private String city;
    private String address;
    private String pincode;
    private String contactPerson;
    private String contactNumber;
    private String contactAddress;
    private int status;
    private String responseStatus;//response parameter
    private String message;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
}
