package com.btoc.pharma.api.dto;

import org.springframework.web.multipart.MultipartFile;

public class OrderResponsiveDTO {
    private String adminId;
    private String userId;
    private String firstName;
    private String lastName;
    private long mobileNumber;
    private String email;
    private String deliveryAddress;
    private String city;
    private String pincode;
    private MultipartFile orderImage;

    public OrderResponsiveDTO() {
    }

    public OrderResponsiveDTO(String adminId, String userId, String firstName, String lastName,
                              long mobileNumber, String email, String deliveryAddress, String city,
                              String pincode, MultipartFile orderImage) {
        this.adminId = adminId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.deliveryAddress = deliveryAddress;
        this.city = city;
        this.pincode = pincode;
        this.orderImage = orderImage;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public MultipartFile getOrderImage() {
        return orderImage;
    }

    public void setOrderImage(MultipartFile orderImage) {
        this.orderImage = orderImage;
    }
}
