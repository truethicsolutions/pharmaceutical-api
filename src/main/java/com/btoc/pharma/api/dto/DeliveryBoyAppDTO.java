package com.btoc.pharma.api.dto;

public class DeliveryBoyAppDTO {
    private String id;
    private String name;
    private String responseStatus;//response parameter
    private String message;
    private String email;
    private String adminId;
    private String address;
    private String gender;
    private long mobile;

    public DeliveryBoyAppDTO(String id, String name, String responseStatus, String message,
                             String email, String adminId, String address, String gender, long mobile) {
        this.id = id;
        this.name = name;
        this.responseStatus = responseStatus;
        this.message = message;
        this.email = email;
        this.adminId = adminId;
        this.address = address;
        this.gender = gender;
        this.mobile = mobile;
    }

    public DeliveryBoyAppDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public long getMobile() {
        return mobile;
    }

    public void setMobile(long mobile) {
        this.mobile = mobile;
    }
}
