package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.ForgotUserPassword;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ForgotUserPasswordRepository extends MongoRepository<ForgotUserPassword, String> {
    ForgotUserPassword findByUserId(String userId);
}
