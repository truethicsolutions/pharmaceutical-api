package com.btoc.pharma.api.dto;

import java.time.LocalDateTime;

/*** Get All orders of Delivery Boy ***/

public class DeliveryOrdersDTO {
    private String orderId;
    private String adminId;
    private String orderNo;
    private String CustomerName;
    private long mobileNumber;
    private String orderStatus;
    private LocalDateTime orderDate;
    private int totalItems;
    private double totalPrice;
    private double orderDiscount;
    private String orderType;
    private String paymentMode;
    private String paymentStatus;
    private LocalDateTime paymentDate;
    private String deliveryAddress;
    private String latitude;
    private String longitude;
    private String deliveryStatus;

    public DeliveryOrdersDTO(String adminId, String orderNo, String customerName, long mobileNumber,
                             String orderStatus, LocalDateTime orderDate, int totalItems, double totalPrice,
                             double orderDiscount, String orderType, String paymentMode,
                             String paymentStatus, LocalDateTime paymentDate, String deliveryAddress,
                             String latitude, String longitude, String deliveryStatus, String orderId) {
        this.adminId = adminId;
        this.orderNo = orderNo;
        CustomerName = customerName;
        this.mobileNumber = mobileNumber;
        this.orderStatus = orderStatus;
        this.orderDate = orderDate;
        this.totalItems = totalItems;
        this.totalPrice = totalPrice;
        this.orderDiscount = orderDiscount;
        this.orderType = orderType;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.paymentDate = paymentDate;
        this.deliveryAddress = deliveryAddress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.deliveryStatus = deliveryStatus;
        this.orderId = orderId;
    }

    public DeliveryOrdersDTO() {
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
