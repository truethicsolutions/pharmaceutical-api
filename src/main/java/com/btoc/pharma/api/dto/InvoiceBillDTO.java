package com.btoc.pharma.api.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class InvoiceBillDTO {
    private String orderNo;
    private LocalDateTime orderDate;
    private double totalPrice;
    private double totalDiscount;
    private String customerName;
    private long mobileNumber;
    private String deliveryAddress;
    private List<InvoiceProducts> products = new ArrayList<>();

    public InvoiceBillDTO(String orderNo, LocalDateTime orderDate, double totalPrice,
                          double totalDiscount, String customerName, long mobileNumber,
                          String deliveryAddress, List<InvoiceProducts> products) {
        this.orderNo = orderNo;
        this.orderDate = orderDate;
        this.totalPrice = totalPrice;
        this.totalDiscount = totalDiscount;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.deliveryAddress = deliveryAddress;
        this.products = products;
    }

    public InvoiceBillDTO() {
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public List<InvoiceProducts> getProducts() {
        return products;
    }

    public void setProducts(List<InvoiceProducts> products) {
        this.products = products;
    }
}
