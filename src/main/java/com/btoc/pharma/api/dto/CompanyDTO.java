package com.btoc.pharma.api.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * for Back office
 **/
@Data
public class CompanyDTO {

    private String id;
    private String userId;
    private String adminId;
    /*private String outletId;
    private String branchId;*/
    private String companyName;
    private String imagePath;
    private int status;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private MultipartFile companyImage;
    private String responseStatus;//response parameter
    private String message;//response parameter
}
