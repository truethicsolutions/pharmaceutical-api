package com.btoc.pharma.api.dto;

import lombok.Data;

import java.time.LocalDateTime;

/* Order Details Page of BackOffice */
@Data
public class AllOrderDetailsDTO {
    private String orderId;
    private String orderType;
    private LocalDateTime orderDate;
    private double orderAmount;
    private String orderImage;
    private String CustomerName;
    private long mobileNumber;
    private String orderStatus;
    private String orderNo;
    private String prescriptionCode;
    private String deliveryBoyName;
    private String deliveryStatus;
    private String paymentMode;
    private String paymentStatus;

}
