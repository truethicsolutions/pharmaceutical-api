package com.btoc.pharma.api.dto;

/* POJO for adding  list of Dynamic Products(specific product) in backoffice */
public class ProductsOrderDTO {
    private String productId;
    private String productName;
    private double ratePerPiece;
    private double discountAmount;
    private String discountType;
    private double discount; //discount per piece
    private double quantity;
    private String packing;

    public ProductsOrderDTO(String productId, String productName, double ratePerPiece,
                            double discountAmount, String orderImage, String discountType,
                            double discount, double quantity, String packing) {
        this.productId = productId;
        this.productName = productName;
        this.ratePerPiece = ratePerPiece;
        this.discountAmount = discountAmount;
        this.discountType = discountType;
        this.discount = discount;
        this.quantity = quantity;
        this.packing = packing;
    }

    public ProductsOrderDTO() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getRatePerPiece() {
        return ratePerPiece;
    }

    public void setRatePerPiece(double ratePerPiece) {
        this.ratePerPiece = ratePerPiece;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }
}
