package com.btoc.pharma.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Customer DTO
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppUserDTO {
    private String id;
    private String firstName;
    private String lastName;
    private long mobileNumber;
    private String gender;
    private String emailId;
    private String pincode;
    private String city;
    private String deliveryAddress;
    private long rewardsPoints;
    private String dob;
    private int totalOrders;


}
