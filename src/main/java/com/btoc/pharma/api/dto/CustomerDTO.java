package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.UserResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CustomerDTO {
    private String status;
    private String message;
    private int responseCode;
    private List<UserResponse> userResponse = new ArrayList<>();
}
