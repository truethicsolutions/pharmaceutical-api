package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.Policies;
import org.springframework.web.multipart.MultipartFile;

public class UsersDTO {
    private String id;
    private String tradeId;
    private String tradeName;
    private String institutionName;
    private String firstName;
    private String lastName;
    private String gender;
    private String address;
    private String email;
    private long mobileNumber;
    private String username;
    private String password;
    private String adminId;
    private String imagePath;
    private MultipartFile profileImage;
    private int userType;
    private int status;
    private String roleName;
    private String roleId;
    private String responseStatus;//response parameter
    private String message;//response parameter
    private Policies policies;

    public UsersDTO(String id, String tradeId, String tradeName, String institutionName,
                    String firstName, String lastName, String gender, String address, String email,
                    long mobileNumber, String username, String adminId, int userType, int status,
                    String roleName, String roleId, String responseStatus, String message, String password,
                    Policies policies, String imagePath, MultipartFile profileImage) {
        this.id = id;
        this.tradeId = tradeId;
        this.tradeName = tradeName;
        this.institutionName = institutionName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.address = address;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.username = username;
        this.password = password;
        this.adminId = adminId;
        this.userType = userType;
        this.status = status;
        this.roleName = roleName;
        this.roleId = roleId;
        this.responseStatus = responseStatus;
        this.message = message;
        this.policies = policies;
        this.imagePath = imagePath;
        this.profileImage = profileImage;
    }

    public UsersDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTradeId() {
        return tradeId;
    }

    public void setTradeId(String tradeId) {
        this.tradeId = tradeId;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Policies getPolicies() {
        return policies;
    }

    public void setPolicies(Policies policies) {
        this.policies = policies;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public MultipartFile getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(MultipartFile profileImage) {
        this.profileImage = profileImage;
    }
}
