package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.OrderProductDetails;

import java.util.ArrayList;
import java.util.List;

public class GeneratedOrdersDTO {
    private String orderImagePath;
    private String adminId;
    private String message;
    private String responseStatus;
    private List<OrderProductDetails> list = new ArrayList();

    public GeneratedOrdersDTO(String orderImagePath, String adminId, String message,
                              String responseStatus, List<OrderProductDetails> list) {
        this.orderImagePath = orderImagePath;
        this.adminId = adminId;
        this.message = message;
        this.responseStatus = responseStatus;
        this.list = list;
    }

    public GeneratedOrdersDTO() {

    }

    public String getOrderImagePath() {
        return orderImagePath;
    }

    public void setOrderImagePath(String orderImagePath) {
        this.orderImagePath = orderImagePath;
    }

    public List<OrderProductDetails> getList() {
        return list;
    }

    public void setList(List<OrderProductDetails> list) {
        this.list = list;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }
}
