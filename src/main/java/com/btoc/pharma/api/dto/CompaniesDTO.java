package com.btoc.pharma.api.dto;

import lombok.Data;

/**
 * for Mobile App
 **/
@Data
public class CompaniesDTO {

    private String id;
    private String companyName;
    private String adminId;
    private String imagePath;
}
