package com.btoc.pharma.api.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CategoryDTO {

    private String id;
    private String userId;
    private String adminId;
    /* private String groupId;
     private String companyId;*/
    private String imagePath;
    private String categoryName;
    /*private String companyName;
    private String groupName;*/
    private int status;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private String responseStatus;//response parameter
    private String message;
    private MultipartFile categoryImage;
}
