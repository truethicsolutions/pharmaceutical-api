package com.btoc.pharma.api.dto;

public class InvoiceProducts {

    private String productName;
    private double mrp;
    private double quantity;
    private String packing;
    private double sTax;
    private double cTax;
    private double iTax;
    private double discountAmount;
    private double Amount;
    private double taxableValue;

    public InvoiceProducts(String productName, double mrp, double quantity, String packing,
                           double sTax, double cTax, double iTax, double taxableValue,
                           double discountAmount, double amount) {
        this.productName = productName;
        this.mrp = mrp;
        this.quantity = quantity;
        this.packing = packing;
        this.sTax = sTax;
        this.cTax = cTax;
        this.iTax = iTax;
        this.discountAmount = discountAmount;
        this.taxableValue = taxableValue;
        Amount = amount;
    }

    public InvoiceProducts() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public double getsTax() {
        return sTax;
    }

    public void setsTax(double sTax) {
        this.sTax = sTax;
    }

    public double getcTax() {
        return cTax;
    }

    public void setcTax(double cTax) {
        this.cTax = cTax;
    }

    public double getiTax() {
        return iTax;
    }

    public void setiTax(double iTax) {
        this.iTax = iTax;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }

    public double getTaxableValue() {
        return taxableValue;
    }

    public void setTaxableValue(double taxableValue) {
        this.taxableValue = taxableValue;
    }
}
