package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.OrderProductDetails;
import com.btoc.pharma.api.model.PaymentDetails;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OrderDTO {

    private String id;
    private String adminId;
    private String userId;
    private String outletId;
    private String customerId;
    private String deliveryBoyId;
    private String branchId;
    private long orderNo;
    private LocalDateTime orderDate;
    private String productId;
    private String customerName;
    private long mobileNumber;
    private String orderStatus;
    private int orderQuantity;
    private double totalPrice;
    private double orderDiscount;
    private String paymentMode;
    private String paymentStatus;
    private String paymentDate;
    private String deliveryAddress;
    private String latitude;
    private String longitude;
    private String deliveryBoyName;
    private String deliveryStatus;
    private String remark;
    private String transactionNumber;
    private String orderType;
    private String orderImagePath;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private int status;
    private int loyaltyPoints;
    private boolean isFirstOrder;
    private MultipartFile orderImage;
    private List<OrderProductDetails> productDetailsList = new ArrayList();
    private PaymentDetails paymentDetails;

    public OrderDTO(String id, String adminId, String userId, String outletId, String customerId,
                    long orderNo, LocalDateTime orderDate, String productId, String customerName,
                    long mobileNumber, String orderStatus, int orderQuantity, double totalPrice,
                    double orderDiscount, String paymentMode, String paymentStatus, String paymentDate,
                    String deliveryAddress, String deliveryBoyName, String deliveryStatus, String remark,
                    String transactionNumber, String orderType, String orderImagePath, String createdAt,
                    String updatedAt, String createdBy, String updatedBy, int status, int loyaltyPoints,
                    MultipartFile orderImage, boolean isFirstOrder, String deliveryBoyId, String branchId,
                    List<OrderProductDetails> productDetailsList, PaymentDetails paymentDetails,
                    String latitude, String longitude) {
        this.id = id;
        this.adminId = adminId;
        this.userId = userId;
        this.outletId = outletId;
        this.customerId = customerId;
        this.deliveryBoyId = deliveryBoyId;
        this.branchId = branchId;
        this.orderNo = orderNo;
        this.orderDate = orderDate;
        this.productId = productId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.orderStatus = orderStatus;
        this.orderQuantity = orderQuantity;
        this.totalPrice = totalPrice;
        this.orderDiscount = orderDiscount;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.paymentDate = paymentDate;
        this.deliveryAddress = deliveryAddress;
        this.deliveryBoyName = deliveryBoyName;
        this.deliveryStatus = deliveryStatus;
        this.remark = remark;
        this.transactionNumber = transactionNumber;
        this.orderType = orderType;
        this.orderImagePath = orderImagePath;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.status = status;
        this.loyaltyPoints = loyaltyPoints;
        this.orderImage = orderImage;
        this.productDetailsList = productDetailsList;
        this.paymentDetails = paymentDetails;
        this.isFirstOrder = isFirstOrder;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public OrderDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOutletId() {
        return outletId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(long orderNo) {
        this.orderNo = orderNo;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getOrderQuantity() {
        return orderQuantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setOrderQuantity(int orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryBoyName() {
        return deliveryBoyName;
    }

    public void setDeliveryBoyName(String deliveryBoyName) {
        this.deliveryBoyName = deliveryBoyName;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getRemark() {
        return remark;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOrderImagePath() {
        return orderImagePath;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setOrderImagePath(String orderImagePath) {
        this.orderImagePath = orderImagePath;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getLoyaltyPoints() {
        return loyaltyPoints;
    }

    public void setLoyaltyPoints(int loyaltyPoints) {
        this.loyaltyPoints = loyaltyPoints;
    }

    public MultipartFile getOrderImage() {
        return orderImage;
    }

    public List<OrderProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    public void setProductDetailsList(List<OrderProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }

    public void setOrderImage(MultipartFile orderImage) {
        this.orderImage = orderImage;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public boolean isFirstOrder() {
        return isFirstOrder;
    }

    public void setFirstOrder(boolean firstOrder) {
        isFirstOrder = firstOrder;
    }

    public String getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(String deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }
}
