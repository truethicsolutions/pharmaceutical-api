package com.btoc.pharma.api.dto;

import java.time.LocalDateTime;

/**
 * for Backoffice
 **/
public class DeliveryBoyReportDTO {

    private String orderNo;
    private LocalDateTime orderDate;
    private LocalDateTime deliveryDate;
    private double orderAmount;
    private String paymentMode;
    private String paymentStatus;
    private String customerName;
    private long mobileNumber;

    public DeliveryBoyReportDTO(String orderNo, LocalDateTime orderDate, LocalDateTime deliveryDate,
                                double orderAmount, String paymentMode, String paymentStatus,
                                String customerName, long mobileNumber) {
        this.orderNo = orderNo;
        this.orderDate = orderDate;
        this.deliveryDate = deliveryDate;
        this.orderAmount = orderAmount;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
    }

    public DeliveryBoyReportDTO() {
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
