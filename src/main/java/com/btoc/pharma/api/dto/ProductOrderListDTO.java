package com.btoc.pharma.api.dto;

import java.util.ArrayList;
import java.util.List;
/* DTO for Product List of orders from Dynamic Page of Backoffice */

public class ProductOrderListDTO {
    private String orderId;
    private double finalAmount;
    private double orderDiscount;
    private List<ProductOrderDetailsDTO> list = new ArrayList();

    public ProductOrderListDTO(String orderId, double finalAmount,
                               List<ProductOrderDetailsDTO> list, double orderDiscount) {
        this.orderId = orderId;
        this.finalAmount = finalAmount;
        this.list = list;
        this.orderDiscount = orderDiscount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public List<ProductOrderDetailsDTO> getList() {
        return list;
    }

    public void setList(List<ProductOrderDetailsDTO> list) {
        this.list = list;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }
}

