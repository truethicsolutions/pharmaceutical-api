package com.btoc.pharma.api.dto;

public class LoyaltyDTO {
    private String id;
    private String userId;// here we are mapping Institute Id
    private String adminId;
    private String outletId;
    private String outletName;
    private boolean isfirstOrder;
    private boolean isminOrder;
    private int firstOrderPoints;
    private int minAmount;
    private int loyaltyPoint;
    private int status;
    private String responseStatus;//response parameter
    private String message;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;

    public LoyaltyDTO() {
    }

    public LoyaltyDTO(String id, String userId, String adminId, String outletId, String outletName, boolean isfirstOrder, boolean isminOrder, int firstOrderPoints, int minAmount, int loyaltyPoint, int status, String responseStatus, String message, String createdAt,
                      String updatedAt, String createdBy, String updatedBy) {
        this.id = id;
        this.userId = userId;
        this.adminId = adminId;
        this.outletId = outletId;
        this.outletName = outletName;
        this.isfirstOrder = isfirstOrder;
        this.isminOrder = isminOrder;
        this.firstOrderPoints = firstOrderPoints;
        this.minAmount = minAmount;
        this.loyaltyPoint = loyaltyPoint;
        this.status = status;
        this.responseStatus = responseStatus;
        this.message = message;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public boolean isIsfirstOrder() {
        return isfirstOrder;
    }

    public void setIsfirstOrder(boolean isfirstOrder) {
        this.isfirstOrder = isfirstOrder;
    }

    public boolean isIsminOrder() {
        return isminOrder;
    }

    public void setIsminOrder(boolean isminOrder) {
        this.isminOrder = isminOrder;
    }

    public int getFirstOrderPoints() {
        return firstOrderPoints;
    }

    public void setFirstOrderPoints(int firstOrderPoints) {
        this.firstOrderPoints = firstOrderPoints;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(int minAmount) {
        this.minAmount = minAmount;
    }

    public int getLoyaltyPoint() {
        return loyaltyPoint;
    }

    public void setLoyaltyPoint(int loyaltyPoint) {
        this.loyaltyPoint = loyaltyPoint;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
