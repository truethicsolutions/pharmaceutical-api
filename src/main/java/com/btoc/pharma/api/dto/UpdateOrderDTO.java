package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.OrderProductDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UpdateOrderDTO {
    private String orderId;
    private double orderAmount;
    private double orderDiscount;
    private String delivery_Address;
    private String paymentMode;
    private String paymentStatus;
    private String transactionNumber;
    private LocalDateTime paymentDate;
    private List<OrderProductDetails> productList = new ArrayList();

    public UpdateOrderDTO(String orderId, double orderAmount, double orderDiscount, String deliveryAddress,
                          String paymentMode, String paymentStatus, String transactionNumber,
                          LocalDateTime paymentDate, List<OrderProductDetails> productList) {
        this.orderId = orderId;
        this.orderAmount = orderAmount;
        this.orderDiscount = orderDiscount;
        this.delivery_Address = deliveryAddress;
        this.paymentMode = paymentMode;
        this.paymentStatus = paymentStatus;
        this.transactionNumber = transactionNumber;
        this.paymentDate = paymentDate;
        this.productList = productList;
    }

    public UpdateOrderDTO() {
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<OrderProductDetails> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderProductDetails> productList) {
        this.productList = productList;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public LocalDateTime getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDateTime paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getDelivery_Address() {
        return delivery_Address;
    }

    public void setDelivery_Address(String delivery_Address) {
        this.delivery_Address = delivery_Address;
    }

}
