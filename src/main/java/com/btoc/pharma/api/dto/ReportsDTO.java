package com.btoc.pharma.api.dto;

import java.util.Date;

public class ReportsDTO {
    private String adminId;
    private Date dateFrom;
    private Date dateTo;
    private String status;
    private String pageNumber;
    private String pageSize;

    public ReportsDTO(String adminId, Date dateFrom, Date dateTo,
                      String status, String pageNumber, String pageSize) {
        this.adminId = adminId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.status = status;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;

    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
