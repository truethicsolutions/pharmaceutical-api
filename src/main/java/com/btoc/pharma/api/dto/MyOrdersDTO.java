package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.DeliveryAddress;
import com.btoc.pharma.api.model.OrderProductDetails;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MyOrdersDTO {
    private String orderId;
    private LocalDateTime orderDate;
    private String orderStatus;
    private String orderType;
    private double orderAmount;
    private double orderDiscount;
    private String orderNo;
    private List<DeliveryAddress> deliveryAddress;

    private List<OrderProductDetails> productList = new ArrayList();

    public MyOrdersDTO(String orderId, LocalDateTime orderDate, String orderStatus,
                       String orderType, double orderAmount, double orderDiscount, String orderNo,
                       List<DeliveryAddress> deliveryAddress, List<OrderProductDetails> productList) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.orderStatus = orderStatus;
        this.orderType = orderType;
        this.orderAmount = orderAmount;
        this.orderDiscount = orderDiscount;
        this.deliveryAddress = deliveryAddress;
        this.productList = productList;
        this.orderNo = orderNo;
    }

    public MyOrdersDTO() {
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderProductDetails> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderProductDetails> productList) {
        this.productList = productList;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public List<DeliveryAddress> getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(List<DeliveryAddress> deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
