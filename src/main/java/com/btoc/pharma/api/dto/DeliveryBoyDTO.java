package com.btoc.pharma.api.dto;

public class DeliveryBoyDTO {
    private String id;
    private String name;

    public DeliveryBoyDTO(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public DeliveryBoyDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
