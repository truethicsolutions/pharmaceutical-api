package com.btoc.pharma.api.dto;

public class ResponsiveOrderResponseDTO {
    private String orderId;
    private String status;
    private String keyId;
    private String paymentId;
    private String customerName;
    private long mobileNumber;
    private String email;
    private String description;
    private int payableAmount;

    public ResponsiveOrderResponseDTO() {
    }

    public ResponsiveOrderResponseDTO(String orderId, String status, String keyId,
                                      String paymentId, String customerName, long mobileNumber,
                                      String email, String description, int payableAmount) {
        this.orderId = orderId;
        this.status = status;
        this.keyId = keyId;
        this.paymentId = paymentId;
        this.customerName = customerName;
        this.mobileNumber = mobileNumber;
        this.email = email;
        this.description = description;
        this.payableAmount = payableAmount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(int payableAmount) {
        this.payableAmount = payableAmount;
    }

    public long getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(long mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
