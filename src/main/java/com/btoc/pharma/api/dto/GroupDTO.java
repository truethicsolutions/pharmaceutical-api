package com.btoc.pharma.api.dto;

public class GroupDTO {
    private String id;
    private String userId;
    private String adminId;
   // private String companyId;
    private String groupName;
    //private String companyName;
    private int status;
    private String responseStatus;//response parameter
    private String message;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;

    public GroupDTO() {
    }

    public GroupDTO(String id, String userId, String companyId, String groupName, String companyName,
                    int status, String responseStatus, String message, String createdAt,
                    String updatedAt, String createdBy, String updatedBy, String adminId) {
        this.id = id;
        this.userId = userId;
        this.adminId = adminId;
        // this.companyId = companyId;
        this.groupName = groupName;
        //   this.companyName = companyName;
        this.status = status;
        this.responseStatus = responseStatus;
        this.message = message;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    /*public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }*/

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    /*public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }*/

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
}
