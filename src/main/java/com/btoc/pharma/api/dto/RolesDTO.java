package com.btoc.pharma.api.dto;


import com.btoc.pharma.api.model.Total;

public class RolesDTO {
    private String id;
    private String userId;
    private String adminId;
    private String roleName;
    private int status;
    private String message;
    private String responseCode;
    private String createdAt;
    private String updatedAt;

    public RolesDTO(String id, String userId, String roleName,
                    int status, String message, String responseCode,
                    String createdAt, String updatedAt, String adminId, Total total) {
        this.id = id;
        this.userId = userId;
        this.adminId = adminId;
        this.roleName = roleName;
        this.status = status;
        this.message = message;
        this.responseCode = responseCode;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public RolesDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

}
