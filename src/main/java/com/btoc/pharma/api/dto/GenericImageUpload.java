package com.btoc.pharma.api.dto;

public class GenericImageUpload<T> {
    private T t;
    private String name;

    public GenericImageUpload(T t, String name) {
        this.t = t;
        this.name = name;
    }

    public GenericImageUpload() {
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserIdAndOutletId(T t, String name, String adminId) {
        // Branch branch = null;
        String dir = "";
        String imagePath = null;
        dir = adminId + "_" + name;
        // imagePath = awss3Service.uploadFile(companyDTO.getCompanyImage(), dir);
        //  imagePath = companyDTO.getCompanyImage().getOriginalFilename();
        return imagePath;
    }
}
