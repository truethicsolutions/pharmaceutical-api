package com.btoc.pharma.api.dto;

public class OutletDTO {

    private String id;
    private String branchId;
    private String branchName;
    private String userId;
    private String adminId;
    private String outletName;
    private String licenseNumber;
    private String licenseExpiry;
    private String gstNumber;
    private String gstExpiry;
    private String registeredAddress;
    private String operationalAddress;
    private String pincode;
    private String bankName;
    private String accountNumber;
    private String holderName;
    private String ifscCode;
    private String branchOfBank;
    private String paymentGatewayUrl;
    private int status;
    /* Response Parameter */
    private String responseStatus;
    private String message;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;

    public OutletDTO() {
    }

    public OutletDTO(String id, String branchId, String branchName, String userId, String outletName, String licenseNumber,
                     String licenseExpiry, String gstNumber, String gstExpiry,
                     String registeredAddress, String operationalAddress, String pincode,
                     String bankName, String accountNumber, String holderName, String ifscCode,
                     String branchOfBank, String paymentGatewayUrl, int status, String responseStatus,
                     String createdAt, String createdBy, String updatedAt, String updatedBy,
                     String message, String adminId) {
        this.id = id;
        this.branchId = branchId;
        this.branchName = branchName;
        this.userId = userId;
        this.adminId = adminId;
        this.outletName = outletName;
        this.licenseNumber = licenseNumber;
        this.licenseExpiry = licenseExpiry;
        this.gstNumber = gstNumber;
        this.gstExpiry = gstExpiry;
        this.registeredAddress = registeredAddress;
        this.operationalAddress = operationalAddress;
        this.pincode = pincode;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.holderName = holderName;
        this.ifscCode = ifscCode;
        this.branchOfBank = branchOfBank;
        this.paymentGatewayUrl = paymentGatewayUrl;
        this.status = status;
        this.responseStatus = responseStatus;
        this.message = message;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = updatedAt;
        this.updatedBy = updatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBranchId() {
        return branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOutletName() {
        return outletName;
    }

    public void setOutletName(String outletName) {
        this.outletName = outletName;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getLicenseExpiry() {
        return licenseExpiry;
    }

    public void setLicenseExpiry(String licenseExpiry) {
        this.licenseExpiry = licenseExpiry;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getGstExpiry() {
        return gstExpiry;
    }

    public void setGstExpiry(String gstExpiry) {
        this.gstExpiry = gstExpiry;
    }

    public String getRegisteredAddress() {
        return registeredAddress;
    }

    public void setRegisteredAddress(String registeredAddress) {
        this.registeredAddress = registeredAddress;
    }

    public String getOperationalAddress() {
        return operationalAddress;
    }

    public void setOperationalAddress(String operationalAddress) {
        this.operationalAddress = operationalAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getBranchOfBank() {
        return branchOfBank;
    }

    public void setBranchOfBank(String branchOfBank) {
        this.branchOfBank = branchOfBank;
    }

    public String getPaymentGatewayUrl() {
        return paymentGatewayUrl;
    }

    public void setPaymentGatewayUrl(String paymentGatewayUrl) {
        this.paymentGatewayUrl = paymentGatewayUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
}
