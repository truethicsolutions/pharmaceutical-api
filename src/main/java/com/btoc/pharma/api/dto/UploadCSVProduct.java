package com.btoc.pharma.api.dto;

public class UploadCSVProduct {
    private String userId;
    private String adminId;
    private String companyName;
    private String groupName;
    private String categoryName;
    private String productName;
    private String shortDescription;
    private String fullDescription;
    private String imagePath;
    private String prescriptionType;
    private double mrp;
    private double purchaseRate;
    private double quantity;
    private String packing;
    private String taxStatus;
    private long hsnNumber;
    private double sTax;
    private double cTax;
    private double iTax;
    private String discountType;
    private double discount;

    public UploadCSVProduct(String userId, String adminId, String companyName, String groupName,
                            String categoryName, String productName, String shortDescription,
                            String fullDescription, String imagePath, String prescriptionType,
                            double mrp, double purchaseRate, double quantity, String packing,
                            String taxStatus, long hsnNumber, double sTax, double cTax,
                            double iTax, String discountType, double discount) {
        this.userId = userId;
        this.adminId = adminId;
        this.companyName = companyName;
        this.groupName = groupName;
        this.categoryName = categoryName;
        this.productName = productName;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.imagePath = imagePath;
        this.prescriptionType = prescriptionType;
        this.mrp = mrp;
        this.purchaseRate = purchaseRate;
        this.quantity = quantity;
        this.packing = packing;
        this.taxStatus = taxStatus;
        this.hsnNumber = hsnNumber;
        this.sTax = sTax;
        this.cTax = cTax;
        this.iTax = iTax;
        this.discountType = discountType;
        this.discount = discount;
    }

    public UploadCSVProduct() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPrescriptionType() {
        return prescriptionType;
    }

    public void setPrescriptionType(String prescriptionType) {
        this.prescriptionType = prescriptionType;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getPurchaseRate() {
        return purchaseRate;
    }

    public void setPurchaseRate(double purchaseRate) {
        this.purchaseRate = purchaseRate;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public long getHsnNumber() {
        return hsnNumber;
    }

    public void setHsnNumber(long hsnNumber) {
        this.hsnNumber = hsnNumber;
    }

    public double getsTax() {
        return sTax;
    }

    public void setsTax(double sTax) {
        this.sTax = sTax;
    }

    public double getcTax() {
        return cTax;
    }

    public void setcTax(double cTax) {
        this.cTax = cTax;
    }

    public double getiTax() {
        return iTax;
    }

    public void setiTax(double iTax) {
        this.iTax = iTax;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
}
