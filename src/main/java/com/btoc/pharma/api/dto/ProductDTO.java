package com.btoc.pharma.api.dto;

import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

public class ProductDTO {
    private String id;
    private String companyId;
    private String groupId;
    private String categoryId;
    private String userId;
    private String adminId;
    //private String outletId;
    private String companyName;
    private String groupName;
    private String categoryName;
    private String productName;
    private String shortDescription;
    private String fullDescription;
    private String imagePath;
    private String prescriptionType;
    private double mrp;
    private double purchaseRate;
    private double quantity;
    private String packing;
    private double ratePerPiece;
    private String topTrending;
    private long hsnNumber;
    private String batch;
    private String shf;
    private LocalDate expiryDate;
    private double sTax;
    private double cTax;
    private double iTax;
    private String taxStatus;
    private String discountType;
    private double discount;
    private double discountAmount;
    private int status;
    private MultipartFile productImage;
    private String createdAt;
    private String updatedAt;
    private String createdBy;
    private String updatedBy;
    private String responseStatus;//response parameter
    private String message;

    public ProductDTO() {
        super();
    }

    public ProductDTO(String id, String companyId, String groupId, String categoryId, String userId,
                      String outletId, String companyName, String groupName, String categoryName,
                      String productName, String shortDescription, String fullDescription,
                      String imagePath, String prescriptionType, double mrp, double purchaseRate,
                      double quantity, String packing, double ratePerPiece, long hsnNumber,
                      double sTax, double cTax, double iTax, String taxStatus, String discountType,
                      double discount, double discountAmount, int status, MultipartFile productImage,
                      String createdAt, String updatedAt, String createdBy, String updatedBy,
                      String responseStatus, String message, String adminId, String topTrending,
                      String batch, String shf, LocalDate expiryDate) {
        this.id = id;
        this.companyId = companyId;
        this.groupId = groupId;
        this.categoryId = categoryId;
        this.userId = userId;
        this.adminId = adminId;
        // this.outletId = outletId;
        this.companyName = companyName;
        this.groupName = groupName;
        this.categoryName = categoryName;
        this.productName = productName;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.imagePath = imagePath;
        this.prescriptionType = prescriptionType;
        this.mrp = mrp;
        this.purchaseRate = purchaseRate;
        this.quantity = quantity;
        this.packing = packing;
        this.ratePerPiece = ratePerPiece;
        this.hsnNumber = hsnNumber;
        this.sTax = sTax;
        this.cTax = cTax;
        this.iTax = iTax;
        this.taxStatus = taxStatus;
        this.discountType = discountType;
        this.discount = discount;
        this.discountAmount = discountAmount;
        this.status = status;
        this.productImage = productImage;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.createdBy = createdBy;
        this.updatedBy = updatedBy;
        this.responseStatus = responseStatus;
        this.message = message;
        this.topTrending = topTrending;
        this.batch = batch;
        this.shf = shf;
        this.expiryDate = expiryDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPrescriptionType() {
        return prescriptionType;
    }

    public void setPrescriptionType(String prescriptionType) {
        this.prescriptionType = prescriptionType;
    }

    public double getMrp() {
        return mrp;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getPurchaseRate() {
        return purchaseRate;
    }

    public void setPurchaseRate(double purchaseRate) {
        this.purchaseRate = purchaseRate;
    }

    public String getPacking() {
        return packing;
    }

    public void setPacking(String packing) {
        this.packing = packing;
    }

    public long getHsnNumber() {
        return hsnNumber;
    }

    public void setHsnNumber(long hsnNumber) {
        this.hsnNumber = hsnNumber;
    }

    public double getsTax() {
        return sTax;
    }

    public void setsTax(double sTax) {
        this.sTax = sTax;
    }

    public double getcTax() {
        return cTax;
    }

    public void setcTax(double cTax) {
        this.cTax = cTax;
    }

    public double getiTax() {
        return iTax;
    }

    public void setiTax(double iTax) {
        this.iTax = iTax;
    }

    public String getTaxStatus() {
        return taxStatus;
    }

    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getRatePerPiece() {
        return ratePerPiece;
    }

    public void setRatePerPiece(double ratePerPiece) {
        this.ratePerPiece = ratePerPiece;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public MultipartFile getProductImage() {
        return productImage;
    }

    public void setProductImage(MultipartFile productImage) {
        this.productImage = productImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

   /* public String getOutletId() {
        return outletId;
    }

    public void setOutletId(String outletId) {
        this.outletId = outletId;
    }*/

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getTopTrending() {
        return topTrending;
    }

    public void setTopTrending(String topTrending) {
        this.topTrending = topTrending;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getShf() {
        return shf;
    }

    public void setShf(String shf) {
        this.shf = shf;
    }

    public LocalDate getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDate expiryDate) {
        this.expiryDate = expiryDate;
    }
}
