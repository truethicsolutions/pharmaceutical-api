package com.btoc.pharma.api.dto;

import com.btoc.pharma.api.model.OrderProductDetails;

import java.util.ArrayList;
import java.util.List;

public class ResponsiveOrderUpdateDTO {
    private String orderId;
    private double totalPrice;
    private int payableAmount;
    private double orderDiscount;
    private List<OrderProductDetails> productList = new ArrayList();

    public ResponsiveOrderUpdateDTO() {
    }

    public ResponsiveOrderUpdateDTO(String orderId, double totalPrice,
                                    int payableAmount, double orderDiscount, String status,
                                    String keyId, String paymentId, String customerName,
                                    long mobileNumber, String email, String description,
                                    List<OrderProductDetails> productList) {
        this.orderId = orderId;
        this.totalPrice = totalPrice;
        this.payableAmount = payableAmount;
        this.orderDiscount = orderDiscount;
        this.productList = productList;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getPayableAmount() {
        return payableAmount;
    }

    public void setPayableAmount(int payableAmount) {
        this.payableAmount = payableAmount;
    }

    public double getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(double orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public List<OrderProductDetails> getProductList() {
        return productList;
    }

    public void setProductList(List<OrderProductDetails> productList) {
        this.productList = productList;
    }
}
