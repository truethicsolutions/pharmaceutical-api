package com.btoc.pharma.api.dto;

/* POJO of Product for orders from Dynamic Page of Backoffice */

public class ProductOrderDetailsDTO {

    private String productId;
    private int quantity;
    private double totalAmount; // product Total amount
    private double ratePerPiece;
    private double discount; // discount per Piece
    private double discountAmount;

    public ProductOrderDetailsDTO(String productId,
                                  int quantity, double totalAmount, double ratePerPiece,
                                  double discount, double discountAmount) {

        this.productId = productId;
        this.quantity = quantity;
        this.totalAmount = totalAmount;
        this.ratePerPiece = ratePerPiece;
        this.discount = discount;
        this.discountAmount = discountAmount;
    }

    public ProductOrderDetailsDTO() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getRatePerPiece() {
        return ratePerPiece;
    }

    public void setRatePerPiece(double ratePerPiece) {
        this.ratePerPiece = ratePerPiece;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(double discountAmount) {
        this.discountAmount = discountAmount;
    }
}
