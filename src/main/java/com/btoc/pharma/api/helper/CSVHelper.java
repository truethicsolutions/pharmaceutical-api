package com.btoc.pharma.api.helper;

import com.btoc.pharma.api.dao.CategoryRepository;
import com.btoc.pharma.api.dao.CompanyRepository;
import com.btoc.pharma.api.dao.GroupRepository;
import com.btoc.pharma.api.model.Category;
import com.btoc.pharma.api.model.Company;
import com.btoc.pharma.api.model.Group;
import com.btoc.pharma.api.model.Product;
import com.btoc.pharma.api.services.AWSS3Service;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class CSVHelper {

    static String[] HEADERs = {"Product Name", "Company Name", "Group Name", "Category Name",
            "Short Description", "Full Description", "Image Path", "Prescription Type", "MRP",
            "Purchase Rate", "Quantity", "Packing", "Tax Status", "HSN Number",
            "STax", "CTax", "ITax", "Discount Type", "Discount"};
    public String TYPE = "multipart/form-data";
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private FileStorageService fileStorageService;
    private FileStorageProperties fileStorageProperties;
    @Autowired
    private AWSS3Service awss3Service;

    public CSVHelper() {
    }

    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        else return "";
    }

    public boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public List<Product> csvProducts(InputStream is, String userId, String adminId, String fileDir) {
        List<Product> productList = new ArrayList<>();
        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            CSVParser csvParser = new CSVParser(fileReader,
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
            {

                Iterable<CSVRecord> csvRecords = csvParser.getRecords();

                for (CSVRecord csvRecord : csvRecords) {
                    Product product = new Product();
                    product.setUserId(userId);
                    product.setAdminId(adminId);
                    product.setProductName(csvRecord.get("Product Name"));
                    product.setCompanyId(getCompanyId(csvRecord.get("Company Name")));
                    product.setGroupId(getGroupId(csvRecord.get("Group Name")));
                    product.setCategoryId(getCategoryId(csvRecord.get("Category Name")));
                    product.setShortDescription(csvRecord.get("Short Description"));
                    product.setFullDescription(csvRecord.get("Full Description"));
                    product.setImagePath(getImagePath(csvRecord.get("Image Path"), fileDir, adminId));
                    product.setImagePath(csvRecord.get("Image Path"));
                    product.setPrescriptionType(csvRecord.get("Prescription Type"));
                    product.setMrp(Double.parseDouble(csvRecord.get("MRP")));
                    product.setPurchaseRate(Double.parseDouble(csvRecord.get("Purchase Rate")));
                    product.setQuantity(Double.parseDouble(csvRecord.get("Quantity")));
                    product.setPacking(csvRecord.get("Packing"));
                    product.setTaxStatus(csvRecord.get("Tax Status"));
                    product.setHsnNumber(Long.parseLong(csvRecord.get("HSN Number")));
                    product.setsTax(Double.parseDouble(csvRecord.get("State Tax")));
                    product.setcTax(Double.parseDouble(csvRecord.get("CTax")));
                    product.setiTax(Double.parseDouble(csvRecord.get("ITax")));
                    product.setDiscountType(csvRecord.get("Discount Type"));
                    product.setDiscount(Double.parseDouble(csvRecord.get("Discount")));
                    double mrp = Double.parseDouble(csvRecord.get("MRP"));
                    double quantity = Double.parseDouble(csvRecord.get("Quantity"));
                    double discount = Double.parseDouble(csvRecord.get("Discount"));
                    String discountYpe = csvRecord.get("Prescription Type");
                    product.setRatePerPiece(mrp / quantity);
                    if (discountYpe.equalsIgnoreCase("percentage")) {
                        double discountAmount = (mrp * discount) / 100;
                        product.setDiscountAmount(discountAmount);
                    } else if (discountYpe.equalsIgnoreCase("amount")) {
                        product.setDiscountAmount(discount);
                    }

                    productList.add(product);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
        return productList;
    }

    public String getCompanyId(String key) {
        Company company = companyRepository.findByCompanyNameAndStatus(key, 1);
        String companyName = null;
        if (company != null) {
            companyName = company.getId();
        }
        return companyName;
    }

    public String getGroupId(String key) {
        Group group = groupRepository.findByGroupNameAndStatus(key, 1);
        String groupId = null;
        if (group != null) {
            groupId = group.getId();
        }
        return groupId;
    }

    public String getCategoryId(String key) {
        Category category = categoryRepository.findByCategoryNameAndStatus(key, 1);
        String categoryId = null;
        if (category != null) {
            categoryId = category.getId();
        }
        return categoryId;
    }

    public String getImagePath(String fileName, String fileDir, String adminId) {
        BufferedImage img = null;
        String imagePath = null;
        File file = new File(fileDir + fileName); //image file path
        if (file.exists()) {
            try {
                img = ImageIO.read(file);
            } catch (IOException e) {
            }
            String ext = getFileExtension(file);
            String fName = fileDir + "newFile." + getFileExtension(file);
            File imageFile = new File(fName);
            try {
                ImageIO.write(img, ext, imageFile);
            } catch (IOException e) {
            }
            String dir = adminId + "_" + "products";
            imagePath = awss3Service.uploadFile(imageFile, dir);
            if (imageFile.exists()) {
                imageFile.delete(); //remove file from local directory
            }
        }
        return imagePath;
    }
}