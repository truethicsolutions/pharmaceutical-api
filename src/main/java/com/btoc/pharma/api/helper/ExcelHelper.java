package com.btoc.pharma.api.helper;

import com.amazonaws.services.s3.AmazonS3;
import com.btoc.pharma.api.dao.CategoryRepository;
import com.btoc.pharma.api.dao.CompanyRepository;
import com.btoc.pharma.api.dao.GroupRepository;
import com.btoc.pharma.api.dao.ProductRepository;
import com.btoc.pharma.api.model.*;
import com.btoc.pharma.api.services.AWSS3Service;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import net.lingala.zip4j.ZipFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Component
public class ExcelHelper {

    static String[] HEADERs = {"Product Name", "Company Name", "Group Name", "Category Name",
            "Short Description", "Full Description", "Image Path", "Prescription Type", "MRP",
            "Purchase Rate", "Quantity", "Packing", "Tax Status", "HSN Number",
            "STax", "CTax", "ITax", "Discount Type", "Discount", "Top Trending"};
    static String SHEET = "Products";
    public String TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

    @Autowired
    ServletContext context;
    @Autowired
    private ProductRepository repository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private FileStorageService fileStorageService;
    private FileStorageProperties fileStorageProperties;
    @Autowired
    private AWSS3Service awss3Service;
    @Autowired
    private UserName userName;
    @Autowired
    private AmazonS3 amazonS3;
    @Value("${aws.s3.bucket}")
    private String bucketName;

    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        else return "";
    }

    public boolean hasExcelFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }
/*
    public List<Product> excelToProducts(InputStream is, String userId, String adminId,
                                         String fileDir) {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            // Sheet sheet = workbook.getSheet(SHEET);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            List<Product> productList = new ArrayList<>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();
                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                // Iterator<Cell> cellsInRow = currentRow.iterator();
                Product product = new Product();
                Iterator<Cell> cellsInRow = currentRow.cellIterator();
                double mrp = 0;
                double quantity = 0;
                double discount = 0;
                String discounType = "";
                String productName = "";
                Cell currentCell = cellsInRow.next();

                if (!currentCell.getRow().getCell(0).getStringCellValue().isEmpty()
                        && currentCell.getRow().getCell(0).getStringCellValue() != null) {
                    productName = currentCell.getRow().getCell(0).getStringCellValue();
                    product.setProductName(productName);
                    String CompnayName = currentCell.getRow().getCell(1).getStringCellValue();
                    product.setCompanyId(getCompanyId(CompnayName, userId, adminId));
                    product.setGroupId(getGroupId(currentCell.getRow().getCell(2).getStringCellValue(),
                            userId, adminId));
                    product.setCategoryId(getCategoryId(currentCell.getRow().getCell(3).getStringCellValue(),
                            userId, adminId));
                    product.setShortDescription(currentCell.getRow().getCell(4).getStringCellValue());
                    product.setFullDescription(currentCell.getRow().getCell(5).getStringCellValue());
                    String imagePath = currentCell.getRow().getCell(6).getStringCellValue();
                    if (imagePath != null && imagePath != "") {
                        product.setImagePath(getImagePath(currentCell.getRow().getCell(6).getStringCellValue(),
                                fileDir, adminId));
                    }
                    product.setPrescriptionType(currentCell.getRow().getCell(7).getStringCellValue());
                    mrp = currentCell.getRow().getCell(8).getNumericCellValue();
                    product.setMrp(mrp);
                    product.setPurchaseRate(currentCell.getRow().getCell(9).getNumericCellValue());
                    quantity = currentCell.getRow().getCell(10).getNumericCellValue();
                    product.setQuantity(quantity);
                    product.setPacking(currentCell.getRow().getCell(11).getStringCellValue());
                    product.setTaxStatus(currentCell.getRow().getCell(12).getStringCellValue());
                    double hsnNumber = currentCell.getRow().getCell(13).getNumericCellValue();
                    product.setHsnNumber((long) hsnNumber);
                    product.setsTax(currentCell.getRow().getCell(14).getNumericCellValue());
                    product.setcTax(currentCell.getRow().getCell(15).getNumericCellValue());
                    product.setiTax(currentCell.getRow().getCell(16).getNumericCellValue());
                    discounType = currentCell.getRow().getCell(17).getStringCellValue();

                    product.setDiscountType(discounType.toLowerCase());
                    discount = currentCell.getRow().getCell(18).getNumericCellValue();
                    product.setDiscount(discount);
                    int topTrending = (int) currentCell.getRow().getCell(19).getNumericCellValue();
                    if (topTrending == 1)
                        product.setTopTrending("yes");
                    else
                        product.setTopTrending("no");
                }
                product.setUserId(userId);
                product.setAdminId(adminId);
                if (quantity != 0)
                    product.setRatePerPiece(mrp / quantity);
                if (!discounType.isEmpty()) {
                    if (discounType.equalsIgnoreCase("percentage")) {
                        double discountAmount = (mrp * discount) / 100;
                        product.setDiscountAmount(discountAmount);
                    } else if (discounType.equalsIgnoreCase("amount")) {
                        product.setDiscountAmount(discount);
                    }
                }
                product.setStatus(1);
                String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
                product.setCreatedAt(createdAt);
                product.setUpdatedAt(createdAt);
                productList.add(product);
            }
            workbook.close();
            return productList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }
    */

    /* Zip File upload */
    public List<Product> excelToProducts(InputStream is, String userId, String adminId,
                                         MultipartFile zipFile) {
        String destDir = "src/main/resources/unzipTest";
        String filePath = unzip(zipFile, destDir);
        File uploadFile = new File(filePath);
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            // Sheet sheet = workbook.getSheet(SHEET);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            List<Product> productList = new ArrayList<>();
            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();
                // skip header
                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }
                // Iterator<Cell> cellsInRow = currentRow.iterator();
                Product product = new Product();
                Iterator<Cell> cellsInRow = currentRow.cellIterator();
                double mrp = 0;
                double quantity = 0;
                double discount = 0;
                String discounType = "";
                //String productName = "";
                Cell currentCell = cellsInRow.next();
                String pName = currentCell.getRow().getCell(0).getStringCellValue();
                if (pName != "" && !pName.isEmpty() && pName != null) {
                    product.setProductName(pName);
                    String CompnayName = currentCell.getRow().getCell(1).getStringCellValue();
                    product.setCompanyId(getCompanyId(CompnayName, userId, adminId));
                    product.setGroupId(getGroupId(currentCell.getRow().getCell(2).getStringCellValue(),
                            userId, adminId));
                    product.setCategoryId(getCategoryId(currentCell.getRow().getCell(3).getStringCellValue(),
                            userId, adminId));
                    product.setShortDescription(currentCell.getRow().getCell(4).getStringCellValue());
                    product.setFullDescription(currentCell.getRow().getCell(5).getStringCellValue());
                    String imagePath = currentCell.getRow().getCell(6).getStringCellValue();
                    if (imagePath != null && imagePath != "") {
                        try {
                            product.setImagePath(getImagePath(imagePath, filePath, adminId));
                        } catch (Exception e) {
                            System.out.println(e.getMessage());
                        }
                    }
                    product.setPrescriptionType(currentCell.getRow().getCell(7).getStringCellValue());
                    mrp = currentCell.getRow().getCell(8).getNumericCellValue();
                    product.setMrp(mrp);
                    product.setPurchaseRate(currentCell.getRow().getCell(9).getNumericCellValue());
                    quantity = currentCell.getRow().getCell(10).getNumericCellValue();
                    product.setQuantity(quantity);
                    product.setPacking(currentCell.getRow().getCell(11).getStringCellValue());
                    product.setTaxStatus(currentCell.getRow().getCell(12).getStringCellValue());
                    double hsnNumber = currentCell.getRow().getCell(13).getNumericCellValue();
                    product.setHsnNumber((long) hsnNumber);
                    product.setsTax(currentCell.getRow().getCell(14).getNumericCellValue());
                    product.setcTax(currentCell.getRow().getCell(15).getNumericCellValue());
                    product.setiTax(currentCell.getRow().getCell(16).getNumericCellValue());
                    discounType = currentCell.getRow().getCell(17).getStringCellValue();
                    product.setDiscountType(discounType.toLowerCase());
                    discount = currentCell.getRow().getCell(18).getNumericCellValue();
                    product.setDiscount(discount);
                    int topTrending = (int) currentCell.getRow().getCell(19).getNumericCellValue();
                    if (topTrending == 1)
                        product.setTopTrending("yes");
                    else
                        product.setTopTrending("no");
                }
                product.setUserId(userId);
                product.setAdminId(adminId);
                if (quantity != 0)
                    product.setRatePerPiece(mrp / quantity);
                if (!discounType.isEmpty()) {
                    if (discounType.equalsIgnoreCase("percentage")) {
                        double discountAmount = (mrp * discount) / 100;
                        product.setDiscountAmount(discountAmount);
                    } else if (discounType.equalsIgnoreCase("amount")) {
                        product.setDiscountAmount(discount);
                    }
                }
                product.setStatus(1);
                String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
                product.setCreatedAt(createdAt);
                product.setUpdatedAt(createdAt);
                productList.add(product);
            }
            workbook.close();
            String[] entries = uploadFile.list();
            for (String s : entries) {
                File currentFile = new File(uploadFile.getPath(), s);
                currentFile.delete();
            }
            boolean flag = uploadFile.delete();
            return productList;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse Excel file: " + e.getMessage());
        }
    }

    private String unzip(MultipartFile fileZip, String destDir) {
        BufferedImage img = null;
        String imagePath = null;
        BufferedOutputStream bos = null;
        File imageFile = null;
        File node = new File(destDir);
        try {
            // File destDir = new File("src/main/resources/unzipTest");
            File zip = File.createTempFile(UUID.randomUUID().toString(), "temp");
            FileOutputStream outFile = new FileOutputStream(zip);
            IOUtils.copy(fileZip.getInputStream(), outFile);
            outFile.close();
            ZipFile zipFile = new ZipFile(zip);
            zipFile.extractAll(destDir);
            String[] subNode = node.list();
            System.out.println("" + subNode.length);
            imagePath = destDir + File.separator + subNode[0];

        } catch (IOException e) {
            e.printStackTrace();
        }

        return imagePath;
    }

    public String getCompanyId(String key, String userId, String adminId) {
        Company company = companyRepository.findByCompanyNameAndStatus(key, 1);
        String companyId = null;
        if (company != null) {
            companyId = company.getId();
        } else {
            Company mNewCompany = new Company();
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            mNewCompany.setUserId(userId);
            mNewCompany.setAdminId(adminId);
            mNewCompany.setCompanyName(key);
            mNewCompany.setCreatedBy(userName.getUserName(userId));
            mNewCompany.setUpdatedBy(userName.getUserName(userId));
            mNewCompany.setCreatedAt(createdAt);
            mNewCompany.setUpdatedAt(createdAt);
            mNewCompany.setStatus(1);
            Company mUpdatedCompany = companyRepository.save(mNewCompany);
            companyId = mUpdatedCompany.getId();
        }
        return companyId;
    }

    public String getGroupId(String key, String userId, String adminId) {
        Group group = groupRepository.findByGroupNameIgnoreCaseAndStatus(key, 1);
        String groupId = null;
        if (group != null) {
            groupId = group.getId();
        } else {
            Group mGroup = new Group();
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            mGroup.setUserId(userId);
            mGroup.setAdminId(adminId);
            mGroup.setGroupName(key);
            mGroup.setCreatedAt(createdAt);
            mGroup.setUpdatedAt(createdAt);
            mGroup.setCreatedBy(userName.getUserName(userId));
            mGroup.setUpdatedBy(userName.getUserName(userId));
            mGroup.setStatus(1);
            Group mUpdateGroup = groupRepository.save(mGroup);
            groupId = mUpdateGroup.getId();
        }
        return groupId;
    }

    public String getCategoryId(String key, String userId, String adminId) {
        Category category = categoryRepository.findByCategoryNameIgnoreCaseAndStatus(key, 1);
        String categoryId = null;
        if (category != null) {
            categoryId = category.getId();
        } else {
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            Category mCategory = new Category();
            mCategory.setUserId(userId);
            mCategory.setAdminId(adminId);
            mCategory.setCategoryName(key);
            mCategory.setCreatedAt(createdAt);
            mCategory.setUpdatedAt(createdAt);
            mCategory.setCreatedBy(userName.getUserName(userId));
            mCategory.setUpdatedBy(userName.getUserName(userId));
            mCategory.setStatus(1);
            Category mUpdateCategory = categoryRepository.save(mCategory);
            categoryId = mUpdateCategory.getId();
        }
        return categoryId;
    }

    /* Upload Zip file for images */
    public String getImagePath(String fileName, String fileDir, String adminId) {
        BufferedImage img = null;
        String imagePath = null;
        File file = null; //image file path
        if (fileDir != null && fileName != null && fileDir != "" && fileName != "") {
            file = new File(fileDir + "\\" + fileName);
            if (file.exists() && file != null) {
                try {
                    img = ImageIO.read(file);

                } catch (IOException e) {
                }
                String ext = getFileExtension(file);
                String fName = fileDir + "newFile." + getFileExtension(file);
                File imageFile = new File(fName);
                try {
                    ImageIO.write(img, ext, imageFile);
                } catch (IOException e) {
                }
                String dir = adminId + "_" + "products";
                imagePath = awss3Service.uploadFile(imageFile, dir);
                if (imageFile.exists()) {
                    imageFile.delete(); //remove file from local directory
                }
            }
        }
        return imagePath;
    }

}