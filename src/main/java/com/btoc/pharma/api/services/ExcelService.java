package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.ProductRepository;
import com.btoc.pharma.api.helper.ExcelHelper;
import com.btoc.pharma.api.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class ExcelService {

    @Autowired
    ProductRepository repository;

    @Autowired(required = true)
    private ExcelHelper excelHelper;

    /*
    public void save(MultipartFile file, String userId, String adminId, String fileDir) {
        try {
            List<Product> products = excelHelper.excelToProducts(file.getInputStream(),
                    userId, adminId, fileDir);
            repository.saveAll(products);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }*/

    /* zip file */
    public void save(MultipartFile file, String userId, String adminId, MultipartFile zipFile) {
        try {
            List<Product> products = excelHelper.excelToProducts(file.getInputStream(),
                    userId, adminId, zipFile);
            repository.saveAll(products);
        } catch (IOException e) {
            throw new RuntimeException("fail to store excel data: " + e.getMessage());
        }
    }
}