package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.BranchPaginationRepository;
import com.btoc.pharma.api.dao.BranchRepository;
import com.btoc.pharma.api.dao.UsersRepository;
import com.btoc.pharma.api.dto.BranchDTO;
import com.btoc.pharma.api.model.Branch;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.Users;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BranchService {

    @Autowired
    private BranchRepository repository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private BranchPaginationRepository branchPaginationRepository;

    @Autowired
    private FileStorageService fileStorageService;
    private FileStorageProperties fileStorageProperties;

    public BranchDTO createBranch(BranchDTO branchDto) {

        try {

            Branch br = repository.save(convertTo(branchDto));
            if (br != null) {
                branchDto.setMessage("Branch created successfully");
                branchDto.setId(br.getId());
                branchDto.setStatus(br.getStatus());
                branchDto.setCreatedAt(br.getCreatedAt());
                branchDto.setCreatedBy(br.getCreatedBy());
                branchDto.setUpdatedAt(br.getUpdatedAt());
                branchDto.setUpdatedBy(br.getUpdatedBy());
                branchDto.setResponseStatus("success");

            } else {
                branchDto.setMessage("Error in branch creation");
                branchDto.setResponseStatus("error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            branchDto.setMessage("Error in branch creation");
            branchDto.setResponseStatus("error");
        }
        return branchDto;
    }

    private Branch convertTo(BranchDTO branchDto) {

        Branch branch = new Branch();
        String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        branch.setUserId(branchDto.getUserId());
        branch.setAdminId(branchDto.getAdminId());
        branch.setBranchname(branchDto.getBranchname());
        branch.setCountry(branchDto.getCountry());
        branch.setState(branchDto.getState());
        branch.setDistrict(branchDto.getDistrict());
        branch.setCity(branchDto.getCity());
        branch.setAddress(branchDto.getAddress());
        branch.setPincode(branchDto.getPincode());
        branch.setContactPerson(branchDto.getContactPerson());
        branch.setContactNumber(branchDto.getContactNumber());
        branch.setContactAddress(branchDto.getContactAddress());
        branch.setStatus(1);
        branch.setCreatedAt(mCreatedAt);
        branch.setUpdatedAt(mCreatedAt);
        branch.setCreatedBy(getUserName(branchDto.getUserId()));
        branch.setUpdatedBy(getUserName(branchDto.getUserId()));
        return branch;
    }
    /* check for the User name is available or nor if yes then return username
         otherwise return null;
     */
    public String getUserName(String id) {
        Optional<Users> optionalUsers = usersRepository.findById(id);
        Users mUsers = null;
        String userName = null;
        if (optionalUsers != null) {
            mUsers = optionalUsers.get();
            userName = mUsers.getUsername();
        }
        return userName;
    }
    public BranchDTO updateBranch(BranchDTO branchDtO) {
        Branch branch = repository.findByIdAndStatus(branchDtO.getId(), 1);
        if (branch != null) {
            String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
            branch.setUpdatedAt(mCreatedAt);
            branch.setBranchname(branchDtO.getBranchname());
            branch.setCountry(branchDtO.getCountry());
            branch.setState(branchDtO.getState());
            branch.setDistrict(branchDtO.getDistrict());
            branch.setCity(branchDtO.getCity());
            branch.setAddress(branchDtO.getAddress());
            branch.setPincode(branchDtO.getPincode());
            branch.setUpdatedBy(getUserName(branchDtO.getUserId()));
            branch.setContactPerson(branchDtO.getContactPerson());
            branch.setContactNumber(branchDtO.getContactNumber());
            branch.setContactAddress(branchDtO.getContactAddress());
            if (repository.save(branch) != null) {
                branchDtO.setMessage("Branch updated successfully");
                branchDtO.setResponseStatus("success");
            } else {
                branchDtO.setMessage("Error in branch updation");
                branchDtO.setResponseStatus("error");
            }
        }
        return branchDtO;
    }

    public Object getBranch(String userId) {
        List<Branch> branches = repository.findByAdminIdAndStatus(userId, 1);
        List<BranchDTO> branchDTOList = new ArrayList<>();
        if (branches.size() > 0) {
            for (Branch mBranch : branches) {
                branchDTOList.add(convertToDTO(mBranch));
            }
            return branchDTOList;
        } else
            return branchDTOList;
    }

    public Object deleteBranch(String id) {
        Optional<Branch> mBranch = repository.findById(id);
        Branch mUpdatedBranch = mBranch.get();
        ResponseMessage response = ResponseMessage.getInstance();
        if (mUpdatedBranch != null) {
            String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
            mUpdatedBranch.setUpdatedAt(mUpdatedAt);
            //  mUpdatedTrade.setUpdatedBy(mUpdatedTrade.getUpdatedBy());
            mUpdatedBranch.setStatus(0);
            try {
                Branch entity = repository.save(mUpdatedBranch);
                if (entity != null) {
                    response.setMessage("Record deleted successfully");
                    response.setResponseStatus("success");
                } else {
                    response.setMessage("Error in record deletion");
                    response.setResponseStatus("error");
                }
            } catch (Exception e) {
                response.setMessage("Error in record deletion");
                response.setResponseStatus("error");
                e.printStackTrace();
            }
        }
        return response;

    }

    public BranchDTO findBranch(String id) {
        BranchDTO branchDTO = new BranchDTO();
        try {
            Branch branch = repository.findByIdAndStatus(id, 1);
            if (branch != null) {
                branchDTO = convertToDTO(branch);
                branchDTO.setResponseStatus("success");

            } else {
                branchDTO.setMessage("Branch not found");
                branchDTO.setResponseStatus("error");
                return branchDTO;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return branchDTO;
    }

    private BranchDTO convertToDTO(Branch branch) {
        BranchDTO branchDTO = new BranchDTO();
        branchDTO.setId(branch.getId());
        branchDTO.setUserId(branch.getUserId());
        branchDTO.setAdminId(branch.getAdminId());
        branchDTO.setBranchname(branch.getBranchname());
        branchDTO.setCountry(branch.getCountry());
        branchDTO.setState(branch.getState());
        branchDTO.setDistrict(branch.getDistrict());
        branchDTO.setCity(branch.getCity());
        branchDTO.setAddress(branch.getAddress());
        branchDTO.setPincode(branch.getPincode());
        branchDTO.setStatus(branch.getStatus());
        branchDTO.setContactPerson(branch.getContactPerson());
        branchDTO.setContactNumber(branch.getContactNumber());
        branchDTO.setContactAddress(branch.getContactAddress());
        branchDTO.setResponseStatus("success");
        return branchDTO;
    }

    public Object getAllBranch(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Branch> sliceResult = branchPaginationRepository.findByAdminIdAndStatus(adminId, 1, pageable);
        List<Branch> list = sliceResult.toList();
        List<BranchDTO> branchDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Branch mBranch : list) {
                branchDTOList.add(convertToDTO(mBranch));
            }
        }
        GenericData<BranchDTO> data = new GenericData(branchDTOList, sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

   /* public List<BranchDTO> getAllBranch(String adminId) {
        List<Branch> branches = repository.findAllByAdminId(adminId);
        List<BranchDTO> branchDTOList = new ArrayList<>();
        if (branches.size() > 0) {
            for (Branch br : branches) {
                branchDTOList.add(convertToDTO(br));
            }
        }
        return branchDTOList;
    }*/

}
