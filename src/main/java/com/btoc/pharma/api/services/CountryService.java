package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.CountryRepository;
import com.btoc.pharma.api.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    public List<Country> getCountry() {
        List<Country> list = countryRepository.findAllByCountryId("101");

        return list;
    }
}
