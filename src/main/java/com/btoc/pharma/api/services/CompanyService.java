package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.*;
import com.btoc.pharma.api.dto.CompaniesDTO;
import com.btoc.pharma.api.dto.CompanyDTO;
import com.btoc.pharma.api.model.BaseUrl;
import com.btoc.pharma.api.model.Company;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.UserName;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyService {
    @Autowired
    private CompanyRepository repository;

    @Autowired
    private UserName userName;

    @Autowired
    private AWSS3Service awss3Service;

    @Autowired
    private OutletRepository outletRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private CompanyPaginationRepository companyPaginationRepository;

    @Autowired
    private BaseUrl baseUrl;

    public Object registerCompany(CompanyDTO companyDTO) {
        Company mExisting = null;
        mExisting = repository.findByCompanyNameAndAdminIdAndStatus(companyDTO.getCompanyName(),
                companyDTO.getAdminId(), 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (mExisting != null) {
            responseMessage.setMessage("Company name already exist");
            responseMessage.setResponseStatus("200");
        } else {
            Company company = convertTo(companyDTO);
            CompanyDTO mCompanyDTO = new CompanyDTO();
            company.setStatus(1);
            Company mCompany = repository.save(company);
            if (mCompany != null) {
                // mCompanyDTO = convertToDTO(mCompany);
                responseMessage.setMessage("Company created successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in company creation");
                responseMessage.setResponseStatus("error");
            }

        }
        return responseMessage;
    }

    public Company convertTo(CompanyDTO companyDTO) {
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        String imagePath = null;
        Company company = new Company();
        if (companyDTO.getCompanyImage() != null) {
            imagePath = getUserIdAndOutletId(companyDTO);
        }
        try {
            company.setUserId(companyDTO.getUserId());
            company.setAdminId(companyDTO.getAdminId());
          /*  company.setOutletId(companyDTO.getOutletId());
            company.setBranchId(companyDTO.getBranchId());*/
            company.setCompanyName(companyDTO.getCompanyName());
            if (company.getUserId() != null) {
                company.setCreatedBy(userName.getUserName(companyDTO.getUserId()));
                company.setUpdatedBy(userName.getUserName(companyDTO.getUserId()));
            } else {
                company.setCreatedBy(userName.getUserName(companyDTO.getAdminId()));
                company.setUpdatedBy(userName.getUserName(companyDTO.getAdminId()));
            }
            company.setCreatedAt(createdAt);
            company.setUpdatedAt(createdAt);
            if (imagePath != null)
                company.setImagePath(imagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return company;
    }
    public CompanyDTO convertToDTO(Company company) {

        CompanyDTO companyDTO = new CompanyDTO();
        /*Branch branch = branchRepository.findByIdAndStatus(company.getBranchId(), 1);
        Outlet outlet = outletRepository.findByIdAndStatus(company.getOutletId(), 1);
        if (branch != null) {
            branchName = branch.getBranchname();
        }
        if (outlet != null) {
            outletName = outlet.getOutletName();
        }*/
        companyDTO.setId(company.getId());
        companyDTO.setUserId(company.getUserId());
        companyDTO.setAdminId(company.getAdminId());
       /* companyDTO.setOutletId(company.getOutletId());
        companyDTO.setBranchId(company.getBranchId());*/
        companyDTO.setCompanyName(company.getCompanyName());
       /* companyDTO.setBranchName(branchName);
        companyDTO.setOutletName(outletName);*/
        if (company.getImagePath() != null)
            companyDTO.setImagePath(baseUrl.getBASE_URL() + company.getImagePath());
        companyDTO.setStatus(company.getStatus());
        companyDTO.setCreatedAt(company.getCreatedAt());
        companyDTO.setUpdatedAt(company.getUpdatedAt());
        return companyDTO;
    }

    /*public List<CompanyDTO> getAllCompanies(String userId, String branchId, String outletId) {
     *//*  List<Company> companyList = repository.findByUserIdAndBranchIdAndOutletIdAndStatus(userId,
                branchId, outletId, 1);*//*
        List<Company> companyList = repository.findByAdminIdAndBranchIdAndOutletIdAndStatus(userId,
                branchId, outletId, 1);

        List<CompanyDTO> companyDTOList = new ArrayList<>();

        if (companyList.size() > 0) {
            for (Company mCompany : companyList) {
                companyDTOList.add(convertToDTO(mCompany));
            }
        }
        return companyDTOList;
    }*/

    public List<CompanyDTO> getCompanies(String userId) {
        List<Company> companyList = repository.findByAdminIdAndStatus(userId, 1);
        List<CompanyDTO> companyDTOList = new ArrayList<>();
        if (companyList.size() > 0) {
            for (Company mCompany : companyList) {
                companyDTOList.add(convertToDTO(mCompany));
            }
        }
        return companyDTOList;
    }

    public String getUserIdAndOutletId(CompanyDTO companyDTO) {
        // Branch branch = null;
        String dir = "";
        String imagePath = null;
        dir = companyDTO.getAdminId() + "_" + "companies";
        imagePath = awss3Service.uploadFile(companyDTO.getCompanyImage(), dir);
        //  imagePath = companyDTO.getCompanyImage().getOriginalFilename();
        return imagePath;
    }

    public Object updateCompany(CompanyDTO companyDTO) {
        Company company = repository.findByIdAndStatus(companyDTO.getId(), 1);
        Company mUpdatedCompany = null;
        String imagePath = null;
        //  CompanyDTO mUpdatedDTO = new CompanyDTO();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        if (company != null) {
            company.setCompanyName(companyDTO.getCompanyName());
            company.setUpdatedAt(createdAt);
            if (companyDTO.getCompanyImage() != null) {
                imagePath = getUserIdAndOutletId(companyDTO);
            }
            if (imagePath != null)
                company.setImagePath(imagePath);
            mUpdatedCompany = repository.save(company);
            if (mUpdatedCompany != null) {
                //  mUpdatedDTO = convertToDTO(mUpdatedCompany);
                responseMessage.setMessage("Company Updated Successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in updation");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public Object deleteCompany(String id) {
        Company company = repository.findByIdAndStatus(id, 1);
        CompanyDTO companyDTO = new CompanyDTO();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (company != null) {
            company.setStatus(0);
            if (repository.save(company) != null) {
                responseMessage.setMessage("Company deleted successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in company deletion");
                responseMessage.setResponseStatus("error");
            }
        } else {
            responseMessage.setMessage("Error in company deletion");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }
    public Object fetchAllCompany(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Company> sliceResult = companyPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Company> list = sliceResult.toList();
        List<CompanyDTO> companyDTOList = new ArrayList<>(); /* list of companies for back office */
        if (list.size() > 0) {
            for (Company mCompany : list) {
                companyDTOList.add(convertToDTO(mCompany));
            }
        }
        GenericData<CompanyDTO> data = new GenericData(companyDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object fetchCompanies(String adminId) {
        List<Company> list = repository.findTop7ByAdminIdAndStatus(adminId, 1);
        List<CompaniesDTO> companyDTOList = new ArrayList<>(); /* list of companies for mobile app */
        if (list.size() > 0) {
            for (Company mCompany : list) {
                CompaniesDTO companiesDTO = new CompaniesDTO();
                companiesDTO.setId(mCompany.getId());
                companiesDTO.setAdminId(mCompany.getAdminId());
                companiesDTO.setCompanyName(mCompany.getCompanyName());
                companiesDTO.setImagePath(baseUrl.getBASE_URL() + mCompany.getImagePath());
                companyDTOList.add(companiesDTO);
            }
        }
        return companyDTOList;
    }

    public Object featuredCompanies(String adminId) {
        List<Company> list = repository.findByAdminIdAndStatus(adminId, 1);
        List<CompaniesDTO> companyDTOList = new ArrayList<>(); /* list of companies for mobile app */
        if (list.size() > 0) {
            for (Company mCompany : list) {
                CompaniesDTO companiesDTO = new CompaniesDTO();
                companiesDTO.setId(mCompany.getId());
                companiesDTO.setAdminId(mCompany.getAdminId());
                companiesDTO.setCompanyName(mCompany.getCompanyName());
                if (mCompany.getImagePath() != null)
                    companiesDTO.setImagePath(baseUrl.getBASE_URL() + mCompany.getImagePath());
                companyDTOList.add(companiesDTO);
            }
        }
        return companyDTOList;
    }
}
