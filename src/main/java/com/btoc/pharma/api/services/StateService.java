package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.StateRepository;
import com.btoc.pharma.api.model.Outlet;
import com.btoc.pharma.api.model.States;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateService {
    @Autowired
    private StateRepository stateRepository;

    public List<States> getState(String countryId) {

        List<States> states = stateRepository.findAllByCountryId(countryId);
        if (states.size() > 0) {
            return states;
        } else
            return null;

    }
}
