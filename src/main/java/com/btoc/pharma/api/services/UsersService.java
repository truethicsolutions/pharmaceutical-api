package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.*;
import com.btoc.pharma.api.dto.DeliveryBoyAppDTO;
import com.btoc.pharma.api.dto.ForgotUserPasswordRepository;
import com.btoc.pharma.api.dto.UsersDTO;
import com.btoc.pharma.api.model.*;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.IOException;
import java.util.*;

@Service
public class UsersService {
    @Autowired
    private UsersRepository repository;
    @Autowired
    private TradeRepository tradeRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserpermissiosRepository userpermissiosRepository;
    @Autowired
    AWSS3Service awss3Service;
    @Autowired
    private UsersPaginationRepository usersPaginationRepository;
    @Autowired
    private BaseUrl baseUrl;
    @Autowired
    private ForgotUserPasswordRepository forgotUserPasswordRepository;

    public Object registerAdmin(UsersDTO entity) {

        Users mExisting = null;
        mExisting = repository.findByUsernameIgnoreCaseAndUserTypeAndStatus(entity.getUsername(), 1, 1);
        if (mExisting != null) {
            ResponseMessage responseMessage = ResponseMessage.getInstance();
            responseMessage.setMessage("Username already exist");
            responseMessage.setResponseStatus("200");
            return responseMessage;
        } else {
            Users users = convertToUsers(entity, "register");
            Trade trade = tradeRepository.findByTradeName(entity.getTradeId());

            Users mNewUser = repository.save(users);
            UserPermission userPermission = new UserPermission();

            /** Configure Master Permissions  **/
            MasterPolicies masterPolicies = new MasterPolicies();
            masterPolicies.setBranchPermission("write");
            masterPolicies.setCatlogPermission("write");
            masterPolicies.setOutletPermission("write");
            masterPolicies.setProductPermission("write");
            masterPolicies.setRewardPermission("write");
            masterPolicies.setRolePermission("write");
            masterPolicies.setUserPermission("write");

            /** Customer Permissions **/
            CustomerPolicies customerPolicies = new CustomerPolicies();
            customerPolicies.setCustomerPermission("write");

            /** Order Permissions **/
            OrderPolicies orderPolicies = new OrderPolicies();
            orderPolicies.setOrderPermission("write");

            /** Report Permissions **/
            ReportsPolicies reportsPolicies = new ReportsPolicies();
            reportsPolicies.setDeliveryPermission("write");
            reportsPolicies.setPaymentPermission("write");
            reportsPolicies.setReportsPermission("write");

            /** set Policies to Permissions **/
            userPermission.setMaster(masterPolicies);
            userPermission.setCustomer(customerPolicies);
            userPermission.setOrder(orderPolicies);
            userPermission.setReports(reportsPolicies);
            userPermission.setUserId(mNewUser.getId());
            userPermission.setRoleId(mNewUser.getRoleId());
            userpermissiosRepository.save(userPermission);
            if (mNewUser != null) {
                entity.setId(mNewUser.getId());
                Users adminUsers = createAdmin(mNewUser);
                entity.setAdminId(adminUsers.getAdminId());
                entity.setTradeId(mNewUser.getTradeId());
                entity.setStatus(mNewUser.getStatus());
                entity.setUserType(mNewUser.getUserType());
                if (trade != null) {
                    entity.setTradeName(trade.getTradeName());
                }
                entity.setMessage("Institute created successfully");
                entity.setResponseStatus("success");

            } else {
                entity.setMessage("Error in institute creation");
                entity.setResponseStatus("error");
            }

            return entity;
        }
    }

    private Users createAdmin(Users mNewUser) {
        Users mUsers = repository.findByIdAndStatus(mNewUser.getId(), 1);
        mUsers.setAdminId(mNewUser.getId());
        return repository.save(mUsers);
    }

    public Object updateAdmin(UsersDTO entity) {
        Users mUpdatedUsers = convertToUsers(entity, "update");
        Users users = null;
        try {
            if (mUpdatedUsers != null)
                users = repository.save(mUpdatedUsers);
            if (users != null) {
                entity.setTradeId(users.getTradeId());
                entity.setStatus(users.getStatus());
                entity.setUserType(users.getUserType());
                if (users.getUserType() != 1) {
                    entity.setAdminId(users.getAdminId());
                }
                entity.setRoleName(roleRepository.findByIdAndStatus(users.getRoleId(), 1).get().getRoleName());
                entity.setMessage("Institute updated successfully");
                entity.setResponseStatus("success");
            } else {
                entity.setMessage("Error in institute updation");
                entity.setResponseStatus("error");
            }
        } catch (Exception e) {
            entity.setMessage("Error in institute updation");
            entity.setResponseStatus("error");
            e.printStackTrace();
        }
        return entity;
    }

    public List<UsersDTO> getAllAdmin() {
        List<Users> list = repository.findByUserTypeAndStatus(1, 1);
        List<UsersDTO> usersDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Users mUsers : list) {
                if (mUsers.getAdminId() != null)
                    usersDTOList.add(convertToDTO(mUsers));
            }
            return usersDTOList;
        } else
            return usersDTOList;
    }
    public Object loginAdmin(String username, String password) {
        UsersDTO usersDTO = null;
        Users users = repository.findByUsernameAndPasswordAndUserTypeAndStatus(username, password, 1, 1);
        if (users != null) {
            usersDTO = convertToDTO(users);
            usersDTO.setMessage("Login Success");
            usersDTO.setResponseStatus("success");
            return usersDTO;
        } else {
            ResponseMessage response = ResponseMessage.getInstance();
            response.setMessage("Invalid username and password");
            response.setResponseStatus("error");
            return response;
        }
    }
    private UsersDTO convertToDTO(Users users) {
        UsersDTO usersDTO = new UsersDTO();
        Trade trade;
        Roles roles;
        String roleName = null, tradeName = null;
        UserPermission userPermission = null;
        Policies policies = new Policies();
        /* get User Permission from userId */
        userPermission = userpermissiosRepository.findByUserIdAndRoleId(users.getAdminId(),
                users.getRoleId());
        if (userPermission != null) {
            policies.setMaster(userPermission.getMaster());
            policies.setCustomer(userPermission.getCustomer());
            policies.setOrder(userPermission.getOrder());
            policies.setReports(userPermission.getReports());
        }
        /* getting Roles from Role Id*/
        Optional<Roles> rolesOptional = roleRepository.findByIdAndStatus(users.getRoleId(), 1);
        if (rolesOptional != null) {
            roles = rolesOptional.get();
            if (roles != null) {
                roleName = roles.getRoleName();
                usersDTO.setRoleName(roleName);
            }
        }
        /* Getting Trade Name by Trade Id*/
        Trade tradeOptinoal = tradeRepository.findByIdAndStatus(users.getTradeId(), 1);
        if (tradeOptinoal != null) {
            tradeName = tradeOptinoal.getTradeName();
            usersDTO.setTradeName(tradeName);
        }
        usersDTO.setPolicies(policies);
        usersDTO.setId(users.getId());
        usersDTO.setAdminId(users.getAdminId());
        usersDTO.setTradeId(users.getTradeId());
        usersDTO.setRoleId(users.getRoleId());
        usersDTO.setUserType(users.getUserType());
        usersDTO.setFirstName(users.getFirstName());
        usersDTO.setLastName(users.getLastName());
        usersDTO.setGender(users.getGender());
        usersDTO.setAddress(users.getAddress());
        usersDTO.setEmail(users.getEmail());
        usersDTO.setMobileNumber(users.getMobileNumber());
        usersDTO.setInstitutionName(users.getInstitutionName());
        usersDTO.setUsername(users.getUsername());
        usersDTO.setStatus(users.getStatus());
        if (users.getImagePath() != null && users.getImagePath() != "") {
            usersDTO.setImagePath(baseUrl.getBASE_URL() + users.getImagePath());
        }
        return usersDTO;
    }

    /*public Users convertToUsers(UsersDTO usersDTO, String register) {
        Users users = null;
        String instituteCode;
        String code;
        Trade trade;
        Roles roles = null;
        if (register.equalsIgnoreCase("register")) {
            users = new Users();
            String tradeId = usersDTO.getTradeId();
            String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
            if (usersDTO.getUserType() != 1) {
                users.setRoleId(usersDTO.getRoleId());
                users.setAdminId(usersDTO.getAdminId());
                Users mUsers = repository.findByIdAndStatus(usersDTO.getAdminId(), 1);
                users.setCreatedBy(mUsers.getFirstName() + mUsers.getLastName());
                users.setUpdatedBy(mUsers.getFirstName() + mUsers.getLastName());
                users.setUsername(usersDTO.getUsername());
                users.setPassword(usersDTO.getPassword());
                users.setUserType(usersDTO.getUserType());
            } else {
                roles = roleRepository.findByRoleName("admin");
                if (roles != null)
                    users.setRoleId(roles.getId());
                users.setUserType(usersDTO.getUserType());
                users.setInstitutionName(usersDTO.getInstitutionName());
                users.setUsername(usersDTO.getUsername());
                users.setPassword(usersDTO.getPassword());
                users.setCreatedBy("SuperAdmin");
                users.setUpdatedBy("SuperAdmin");
            }
            users.setTradeId(tradeId);
            users.setCreatedAt(mCreatedAt);
            users.setUpdatedAt(mCreatedAt);
            users.setStatus(1);
        } else {
            users = repository.findByIdAndStatus(usersDTO.getId(), 1);
            if (users != null) {
                String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
                users.setUpdatedAt(mUpdatedAt);
                users.setInstitutionName(usersDTO.getInstitutionName());
                if (users.getUserType() != 1) {
                    users.setRoleId(usersDTO.getRoleId());
                }
            }
        }
        if (users != null) {
            users.setFirstName(usersDTO.getFirstName());
            users.setLastName(usersDTO.getLastName());
            users.setGender(usersDTO.getGender());
            users.setAddress(usersDTO.getAddress());
            users.setEmail(usersDTO.getEmail());
            users.setMobileNumber(usersDTO.getMobileNumber());
        }
        return users;
    }
*/
    public Users convertToUsers(UsersDTO usersDTO, String register) {
        Users users = null;
        //Users mUpdatedUsers = null;
        String instituteCode;
        String code;
        Trade trade;
        Roles roles = null;
        //   trade = tradeRepository.findByTradeName(usersDTO.getTradeName());
        if (register.equalsIgnoreCase("register")) {
            users = new Users();
            String tradeId = usersDTO.getTradeId();
            String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
            if (usersDTO.getUserType() != 1) {
                users.setRoleId(usersDTO.getRoleId());
                users.setAdminId(usersDTO.getAdminId());
                Users mUsers = repository.findByIdAndStatus(usersDTO.getAdminId(), 1);
                users.setCreatedBy(mUsers.getFirstName() + mUsers.getLastName());
                users.setUpdatedBy(mUsers.getFirstName() + mUsers.getLastName());
                users.setUsername(usersDTO.getUsername());
                users.setPassword(usersDTO.getPassword());
                users.setUserType(usersDTO.getUserType());
            } else {
                roles = roleRepository.findByRoleName("admin");
                if (roles != null)
                    users.setRoleId(roles.getId());
                users.setUserType(usersDTO.getUserType());
                users.setInstitutionName(usersDTO.getInstitutionName());
                users.setUsername(usersDTO.getUsername());
                users.setPassword(usersDTO.getPassword());
                users.setCreatedBy("SuperAdmin");
                users.setUpdatedBy("SuperAdmin");
            }
            /* specify the url type register or Update */

           /* code = usersDTO.getInstitutionName().substring(0, 3);
            long millis = myDateObj
                    .atZone(ZoneId.systemDefault())
                    .toInstant().toEpochMilli();
            instituteCode = (code + millis).toUpperCase();*/
            users.setTradeId(tradeId);
            users.setCreatedAt(mCreatedAt);
            users.setUpdatedAt(mCreatedAt);
            users.setStatus(1);
            users.setFirstName(usersDTO.getFirstName());
            users.setLastName(usersDTO.getLastName());
            users.setGender(usersDTO.getGender());
            users.setAddress(usersDTO.getAddress());
            users.setEmail(usersDTO.getEmail());
            users.setMobileNumber(usersDTO.getMobileNumber());
        } else if (register.equalsIgnoreCase("update")) {
            users = repository.findByIdAndStatus(usersDTO.getId(), 1);
            if (users != null) {
                String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
                users.setUpdatedAt(mUpdatedAt);
                users.setInstitutionName(usersDTO.getInstitutionName());
                if (users.getUserType() != 1) {
                    users.setRoleId(usersDTO.getRoleId());
                }
                users.setFirstName(usersDTO.getFirstName());
                users.setLastName(usersDTO.getLastName());
                users.setGender(usersDTO.getGender());
                users.setAddress(usersDTO.getAddress());
                users.setEmail(usersDTO.getEmail());
                users.setMobileNumber(usersDTO.getMobileNumber());

            }
        } else if (register.equalsIgnoreCase("profile-update")) {
            users = repository.findByIdAndStatus(usersDTO.getId(), 1);
            String imagePath = null;
            if (users != null) {
                String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
                users.setUpdatedAt(mUpdatedAt);
                users.setFirstName(usersDTO.getFirstName());
                users.setLastName(usersDTO.getLastName());
                users.setGender(usersDTO.getGender());
                users.setAddress(usersDTO.getAddress());
                users.setEmail(usersDTO.getEmail());
                users.setMobileNumber(usersDTO.getMobileNumber());
                if (usersDTO.getProfileImage() != null) {
                    imagePath = uploadProfileImage(usersDTO);
                    if (imagePath != null) {
                        users.setImagePath(imagePath);
                    }
                }
            }
        }
        return users;
    }

    private String uploadProfileImage(UsersDTO usersDTO) {
        String dir = null;
        String imagePath = null;
        dir = usersDTO.getAdminId() + "_profile";
        imagePath = awss3Service.uploadFile(usersDTO.getProfileImage(), dir);
        return imagePath;
    }

    public Object deleteAdmin(String id) {

        Users mUsers = repository.findByIdAndStatus(id, 1);
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        mUsers.setUpdatedAt(mUpdatedAt);
        //  mUpdatedTrade.setUpdatedBy(mUpdatedTrade.getUpdatedBy());
        mUsers.setStatus(0);
        ResponseMessage response = ResponseMessage.getInstance();
        try {
            Users entity = repository.save(mUsers);
            if (entity != null) {
                response.setMessage("Record deleted successfully");
                response.setResponseStatus("success");
            } else {
                response.setMessage("Error in record deletion");
                response.setResponseStatus("error");
            }

        } catch (Exception e) {
            response.setMessage("Error in record deletion");
            response.setResponseStatus("error");
            e.printStackTrace();
        }
        return response;
    }

    public Object getAdmin(String id) {
        UsersDTO usersDTO = new UsersDTO();
        try {
            Users list = repository.findByIdAndStatus(id, 1);
            if (list != null) {
                usersDTO = convertToDTO(list);
                usersDTO.setResponseStatus("success");

            } else {
                usersDTO.setMessage("Institute not found");
                usersDTO.setResponseStatus("error");

            }
        } catch (Exception e) {

        }
        return usersDTO;
    }

    public Object rolesConfigure(UserPermission userPermission) {
      /*  Optional<Users> mUsers = repository.findById(userPermission.getUserId());
        Users users = mUsers.get();
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        users.setUpdatedAt(mUpdatedAt);
        List<Policies> mPolicies = new ArrayList<>();
        mPolicies = userPermission.getPolicies();*/
        UserPermission mNewUser = userpermissiosRepository.save(userPermission);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (mNewUser != null) {
            responseMessage.setMessage("Roles configured successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in roles configured");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object registerUser(UsersDTO usersDTO) {
        Users mExsiting = null;
        mExsiting = repository.findByMobileNumberAndUserTypeAndStatus(usersDTO.getMobileNumber(),
                0, 1);
        if (mExsiting != null) {

            ResponseMessage responseMessage = ResponseMessage.getInstance();
            responseMessage.setMessage("User is already registered with this mobile number," +
                    "try with different mobile number");
            responseMessage.setResponseStatus("200");
            return responseMessage;
        } else {
            Users users = convertToUsers(usersDTO, "register");
            Trade trade = tradeRepository.findByIdAndStatus(usersDTO.getTradeId(), 1);
            Users mNewUser = repository.save(users);
            if (mNewUser != null) {
                usersDTO.setId(mNewUser.getId());
                usersDTO.setTradeId(mNewUser.getTradeId());
                usersDTO.setStatus(mNewUser.getStatus());
                usersDTO.setUserType(mNewUser.getUserType());
                usersDTO.setAdminId(mNewUser.getAdminId());
                if (trade != null) {
                    usersDTO.setTradeName(trade.getTradeName());
                }
                usersDTO.setMessage("User created successfully");
                usersDTO.setResponseStatus("success");

            } else {
                usersDTO.setMessage("Error in user creation");
                usersDTO.setResponseStatus("error");
            }
            return usersDTO;
        }
    }

    public Object updateUser(UsersDTO usersDTO) {
        Users mUpdatedUsers = convertToUsers(usersDTO, "update");
        Users users = null;
        try {
            if (mUpdatedUsers != null)
                users = repository.save(mUpdatedUsers);
            if (users != null) {
                usersDTO.setTradeId(users.getTradeId());
                usersDTO.setStatus(users.getStatus());
                usersDTO.setUserType(users.getUserType());
                if (users.getAdminId() != null) {
                    usersDTO.setAdminId(users.getAdminId());
                    usersDTO.setRoleName(roleRepository.findByIdAndStatus(users.getRoleId(),
                            1).get().getRoleName());
                }
                usersDTO.setMessage("User updated successfully");
                usersDTO.setResponseStatus("success");
            } else {
                usersDTO.setMessage("Error in user updation");
                usersDTO.setResponseStatus("error");
            }
        } catch (Exception e) {
            usersDTO.setMessage("Error in user updation");
            usersDTO.setResponseStatus("error");
            e.printStackTrace();
        }
        return usersDTO;
    }

    public Object updateAdminProfile(UsersDTO usersDTO) {
        Users mUpdatedUsers = convertToUsers(usersDTO, "profile-update");
        Users users = null;
        UsersDTO mUpdatedDTO = new UsersDTO();
        ResponseMessage responseMessage = ResponseMessage.getInstance();


                users = repository.save(mUpdatedUsers);
                if (users != null) {
                    mUpdatedDTO = convertToDTO(users);
                    mUpdatedDTO.setMessage("User updated successfully");
                    mUpdatedDTO.setResponseStatus("success");
                    return mUpdatedDTO;
                } else {
                    responseMessage.setMessage("Error in updation");
                    responseMessage.setResponseStatus("error");
                    return responseMessage;
                }
     }
    public List<UsersDTO> getAllUsers(String adminId) {
        List<Users> list = repository.findByAdminIdAndUserTypeAndStatus(adminId, 0, 1);
        List<UsersDTO> usersDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Users mUsers : list)
                usersDTOList.add(convertToDTO(mUsers));
            return usersDTOList;
        } else
            return usersDTOList;
    }

    public Object loginUser(String mobileNumber, String password) {
        UsersDTO usersDTO = null;
        long mobile = Long.parseLong(mobileNumber);
        // Users users = repository.findByUsernameAndPasswordAndUserTypeAndStatus(username, password, 0, 1);
        Users users = repository.findByMobileNumberAndPasswordAndUserTypeAndStatus(mobile, password, 0, 1);
        if (users != null) {
            usersDTO = convertToDTO(users);
            usersDTO.setMessage("Login Success");
            usersDTO.setResponseStatus("success");
            return usersDTO;
        } else {
            ResponseMessage response = ResponseMessage.getInstance();
            response.setMessage("You are not authorized user");
            response.setResponseStatus("error");
            return response;
        }
    }

    public Object findAllAdmin(String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Users> sliceResult = usersPaginationRepository.findByUserTypeAndStatus(1, 1, pageable);
        List<Users> list = sliceResult.toList();
        List<UsersDTO> usersDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Users mUsers : list) {
                if (mUsers.getAdminId() != null)
                    usersDTOList.add(convertToDTO(mUsers));
            }
        }
        GenericData<UsersDTO> data = new GenericData(usersDTOList,
                sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object findAllUsers(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);

        Page<Users> sliceResult = usersPaginationRepository.findByAdminIdAndUserTypeAndStatus(adminId, 0,
                1, pageable);
        List<Users> list = sliceResult.toList();
        List<UsersDTO> usersDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Users mUsers : list) {
                usersDTOList.add(convertToDTO(mUsers));
            }
        }
        GenericData<UsersDTO> data = new GenericData(usersDTOList, sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object loginDeliveryUser(String mobileNumber, String password) {

        DeliveryBoyAppDTO deliveryBoyAppDTO = new DeliveryBoyAppDTO();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        long mobile = Long.parseLong(mobileNumber);
        Users users = repository.findByMobileNumberAndPasswordAndUserTypeAndStatus(mobile,
                password, 0, 1);
        if (users != null) {
            deliveryBoyAppDTO.setId(users.getId());
            deliveryBoyAppDTO.setAdminId(users.getAdminId());
            deliveryBoyAppDTO.setName(users.getFirstName() + " " + users.getLastName());
            deliveryBoyAppDTO.setEmail(users.getEmail());
            deliveryBoyAppDTO.setAddress(users.getAddress());
            deliveryBoyAppDTO.setGender(users.getGender());
            deliveryBoyAppDTO.setMobile(users.getMobileNumber());
            deliveryBoyAppDTO.setMessage("Login success");
            deliveryBoyAppDTO.setResponseStatus("success");
            return deliveryBoyAppDTO;
        } else {
            responseMessage.setMessage("Login Fail");
            responseMessage.setResponseStatus("error");
            return responseMessage;
        }

    }

    public Object loginAsAdmin(String adminId) {
        UsersDTO usersDTO = null;
        Users users = repository.findByIdAndUserTypeAndStatus(adminId, 1, 1);
        if (users != null) {
            usersDTO = convertToDTO(users);
            usersDTO.setMessage("Success");
            usersDTO.setResponseStatus("200");
            return usersDTO;
        } else {
            ResponseMessage response = ResponseMessage.getInstance();
            response.setMessage("Error");
            response.setResponseStatus("400");
            return response;
        }
    }

    public JSONObject forgetAdminPassword(String username) {
        Users mUser = repository.findByUsernameAndUserTypeAndStatus(username, 1, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        JSONObject response = new JSONObject();
        String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        if (mUser != null) {
            try {
                String otp = sendmail();
                ForgotUserPassword password = null;
                password = forgotUserPasswordRepository.findByUserId(mUser.getId());
                if (password != null) {
                    password.setOtp(otp);
                    password.setCreatedAt(mCreatedAt);
                } else {
                    password = new ForgotUserPassword();
                    password.setUserId(mUser.getId());
                    password.setOtp(otp);
                    password.setCreatedAt(mCreatedAt);
                }
                if (forgotUserPasswordRepository.save(password) != null) {
                    response.put("userId", mUser.getId());
                    response.put("message", "OTP sent on mail");
                    response.put("responseStatus", "success");
                }
            } catch (Exception e) {
            }
        }
        return response;
    }

    private String sendmail() throws AddressException, MessagingException, IOException {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("ashwin@truethic.in",
                        "Truethic@2905");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("ashwinrs@gmail.com", false));
		/*msg.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("shrikant@truethic.in,ashwin@truethic.in"));*/
        msg.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("rahul@truethic.in,sneha@truethic.in"));
        msg.setSubject("Verify OTP");
        msg.setContent("Please Verify OTP", "text/html");
        msg.setSentDate(new Date());
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        Random random = new Random();
        String otp = String.format("%04d", random.nextInt(10000));
        messageBodyPart.setContent("Please Verify Your OTP:" + otp, "text/html");
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        msg.setContent(multipart);
        Transport.send(msg);
        return otp;
    }

    public Object verifyAdminOtp(String userId, String otp) {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        ForgotUserPassword mUser = forgotUserPasswordRepository.findByUserId(userId);
        String adminOtp = mUser.getOtp();
        if (adminOtp.equalsIgnoreCase(otp)) {
            responseMessage.setMessage("Otp verified");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Otp not verified");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object resetAdminPassword(String userId, String password) {
        Users mUser = repository.findByIdAndUserTypeAndStatus(userId, 1, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        mUser.setPassword(password);
        if (repository.save(mUser) != null) {
            responseMessage.setMessage("Password reset successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in password reset");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object createToken(String userId, String fcmToken) {
        Users users = repository.findByIdAndStatus(userId, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (users != null) {
            users.setFcmToken(fcmToken);
            if (repository.save(users) != null) {
                responseMessage.setResponseStatus("success");
                responseMessage.setMessage("Token registered with Delivery boy user");
            }
        }
        return responseMessage;
    }
}
