package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.TradePaginationRepository;
import com.btoc.pharma.api.dao.TradeRepository;
import com.btoc.pharma.api.dto.TradeDTO;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.Trade;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TradeService {
    @Autowired
    private TradeRepository repository;

    @Autowired
    private TradePaginationRepository tradePaginationRepository;

    public Object registerTrade(Trade trade) {
        TradeDTO dto = null;
        Trade mTrade = repository.findByTradeNameIgnoreCase(trade.getTradeName());
        if (mTrade != null) {
            ResponseMessage responseMessage = ResponseMessage.getInstance();
            responseMessage.setMessage("Trade already exist");
            responseMessage.setResponseStatus("200");
            return responseMessage;
        } else {
            if (trade != null) {
                String mDate = DateAndTimeUtil.getDateAndTimeUtil();
                trade.setStatus(1);
                trade.setCreatedAt(mDate);
                trade.setCreatedBy("superAdmin");
                trade.setUpdatedAt(mDate);
                trade.setUpdatedBy("superAdmin");
                Trade entity = repository.save(trade);
                dto = convertToDTO(entity);
                if (dto != null) {
                    dto.setMessage("Trade created successfully");
                    dto.setResponseStatus("success");
                } else {
                    dto.setMessage("Error in trade creation");
                    dto.setResponseStatus("error");
                }
            }
            return dto;
        }
    }

    public TradeDTO updateTrade(String id, String tradeName) {

        Trade mUpdatedTrade = repository.findByIdAndStatus(id, 1);
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        mUpdatedTrade.setUpdatedAt(mUpdatedAt);
        mUpdatedTrade.setTradeName(tradeName);
        // mUpdatedTrade.setUpdatedBy(mUpdatedTrade.getUpdatedBy());
        TradeDTO dto = null;
        try {
            Trade entity = repository.save(mUpdatedTrade);
            dto = convertToDTO(entity);
            if (dto != null) {
                dto.setMessage("Trade updated successfully");
                dto.setResponseStatus("success");
            } else {
                dto.setMessage("Error in trade updation");
                dto.setResponseStatus("error");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }
    public List<Trade> getTrades() {
        List<Trade> list = repository.findAllByStatus(1);

        return list;
    }

    public TradeDTO convertToDTO(Trade trade) {
        TradeDTO tradeDTO = new TradeDTO();
        tradeDTO.setId(trade.getId());
        tradeDTO.setTradeName(trade.getTradeName());
        tradeDTO.setStatus(trade.getStatus());
        return tradeDTO;
    }

    public Object deleteTrade(String id) {
        Optional<Trade> mTrade = repository.findById(id);
        Trade mUpdatedTrade = mTrade.get();
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        mUpdatedTrade.setUpdatedAt(mUpdatedAt);
        //  mUpdatedTrade.setUpdatedBy(mUpdatedTrade.getUpdatedBy());
        mUpdatedTrade.setStatus(0);
        ResponseMessage response = ResponseMessage.getInstance();
        try {
            Trade entity = repository.save(mUpdatedTrade);
            response.setMessage("Record deleted successfully");
            response.setResponseStatus("success");
        } catch (Exception e) {
            response.setMessage("Error in record deletion");
            response.setResponseStatus("error");
            e.printStackTrace();
        }
        return response;
    }

    public Trade getTrades(String id) {
        Optional<Trade> trade = repository.findById(id);
        Trade t = trade.get();
        return t;
    }

    public Object getAllTrades(String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Trade> sliceResult = tradePaginationRepository.findAllByStatus(1, pageable);
        List<Trade> list = sliceResult.toList();
        List<TradeDTO> tradeDTO = new ArrayList<>();
        if (list.size() > 0) {
            for (Trade mTrade : list) {
                tradeDTO.add(convertToDTO(mTrade));
            }
        }
        GenericData<TradeDTO> data = new GenericData(tradeDTO, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }
}
