package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.CustomerPaginationRepository;
import com.btoc.pharma.api.dao.CustomerRepository;
import com.btoc.pharma.api.dao.OrderRepository;
import com.btoc.pharma.api.dto.AppUserDTO;
import com.btoc.pharma.api.dto.CustomerDTO;
import com.btoc.pharma.api.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private CustomerPaginationRepository customerPaginationRepository;


    public CustomerDTO loginUser(String mobileNumber, String password) {

        long mobile = Long.parseLong(mobileNumber);
        Customer response = repository.findByMobileNumberAndPassword(mobile, password);
        CustomerDTO customerDTO = new CustomerDTO();
        if (response != null) {
            customerDTO.setMessage("Login success");
            customerDTO.setResponseCode(200);
            customerDTO.setStatus("success");
            customerDTO.setUserResponse(convertToUserResponse(response));
        } else {
            customerDTO.setMessage("Fail to login");
            customerDTO.setResponseCode(200);
			customerDTO.setStatus("error");
		}
		return customerDTO;
	}

	public ResponseEntity<ResponseMessage> registerUser(UserResponse usersResponse) {

		ResponseMessage responseMessage = ResponseMessage.getInstance();
		Customer customer = convertToUser(usersResponse);
		if (repository.save(customer) != null) {

			responseMessage.setMessage("success");
			responseMessage.setResponseStatus("200");
		} else {
			responseMessage.setMessage("error");
			responseMessage.setResponseStatus("200");
		}

		return new ResponseEntity<>(responseMessage, HttpStatus.OK);
	}

	public ResponseEntity<ResponseMessage> updateUser(UserResponse userResponse) {

        ResponseMessage responseMessage = ResponseMessage.getInstance();
        Customer mCustomer = repository.findByIdAndStatus(userResponse.getCustomerId(), 1);
        mCustomer.setfName(userResponse.getfName());
        mCustomer.setlName(userResponse.getlName());
        mCustomer.setEmailId(userResponse.getEmailId());
        List<DeliveryAddress> addressList = mCustomer.getDeliveryAddress();
        addressList.set(0, userResponse.getDeliveryAddress().get(0));
        mCustomer.setDeliveryAddress(addressList);
        if (repository.save(mCustomer) != null) {
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("200");
        } else {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("200");
        }
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

	/*public ResponseEntity<ResponseMessage> getAlreadyRegisteredUser(long mobileNumber)
			throws AddressException, MessagingException, IOException {
		Users users = repository.findByMobileNumber(mobileNumber);
		ResponseMessage response = ResponseMessage.getInstance();
		if(msg!=null) {

			response.setMessage("Already Exist");
			response.setStatus("AE");
			return new ResponseEntity<>(response,HttpStatus.OK);
		}

		else {
			String otp = sendmail();
			response.setMessage(otp);
			response.setStatus("NE");
			return new ResponseEntity<>(response,HttpStatus.OK);
		}
	}*/
	public List<UserResponse> convertToUserResponse(Customer customer) {
        List<UserResponse> response = new ArrayList<>();
        UserResponse userDTO = new UserResponse();
        /*String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        customer.setCreatedAt(createdAt);
        customer.setUpdatedAt(createdAt);*/
        userDTO.setOutletId(customer.getOutletId());
        userDTO.setAdminId(customer.getAdminId());
        userDTO.setCustomerId(customer.getId());
        userDTO.setfName(customer.getfName());
        userDTO.setlName(customer.getlName());
        userDTO.setPassword(customer.getPassword());
        userDTO.setMobileNumber(customer.getMobileNumber());
        userDTO.setAddress(customer.getAddress());
        userDTO.setGender(customer.getGender());
        userDTO.setDob(customer.getDob());
        userDTO.setEmailId(customer.getEmailId());
        userDTO.setUserName(customer.getUserName());
        userDTO.setProfileImage(customer.getProfileImage());
        userDTO.setUserRoleType(customer.getUserRoleType());
        userDTO.setUserRoleName(customer.getUserRoleName());
        userDTO.setDeliveryAddress(customer.getDeliveryAddress());
        userDTO.setCountry(customer.getCountry());
        userDTO.setState(customer.getState());
        userDTO.setCity(customer.getCity());
        userDTO.setDistrict(customer.getDistrict());
        userDTO.setPincode(customer.getPincode());
        userDTO.setRewardsPoints(customer.getRewardsPoints());
        userDTO.setStatus(customer.getStatus());
        userDTO.setAppUser(customer.getAppUser());
        response.add(userDTO);
        return response;
    }

	public Customer convertToUser(UserResponse userResponse) {
        Customer customer = new Customer();
        customer.setOutletId(userResponse.getOutletId());
        customer.setAdminId(userResponse.getAdminId());
        customer.setfName(userResponse.getfName());
        customer.setlName(userResponse.getlName());
        customer.setMobileNumber(userResponse.getMobileNumber());
        customer.setPassword(userResponse.getPassword());
        customer.setAddress(userResponse.getAddress());
        customer.setGender(userResponse.getGender());
        customer.setDob(userResponse.getDob());
        customer.setEmailId(userResponse.getEmailId());
        customer.setUserName(userResponse.getUserName());
        customer.setProfileImage(userResponse.getProfileImage());
        customer.setUserRoleType(userResponse.getUserRoleType());
        customer.setUserRoleName(userResponse.getUserRoleName());
        customer.setCountry(customer.getCountry());
        customer.setState(customer.getState());
        customer.setCity(customer.getCity());
        customer.setDistrict(customer.getDistrict());
        customer.setPincode(customer.getPincode());
        customer.setRewardsPoints(customer.getRewardsPoints());
        customer.setDeliveryAddress(userResponse.getDeliveryAddress());
        return customer;
    }

	private String sendmail() throws AddressException, MessagingException, IOException {

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("ashwin@truethic.in",
                        "Truethic@2905");
            }
        });
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress("ashwinrs@gmail.com", false));
		/*msg.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("shrikant@truethic.in,ashwin@truethic.in"));*/
        msg.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("shrikant@truethic.in,ashwin@truethic.in"));
        msg.setSubject("Verify OTP");
        msg.setContent("Please Verify OTP", "text/html");
        msg.setSentDate(new Date());
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        Random random = new Random();
        String id = String.format("%04d", random.nextInt(10000));
        messageBodyPart.setContent("Please Verify Your OTP:" + id, "text/html");
        Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);
		msg.setContent(multipart);
		Transport.send(msg);
		return id;
	}

	public CustomerDTO getUser(String mobileNumber) throws AddressException, MessagingException, IOException {

		Customer response = repository.findByMobileNumber(Long.parseLong(mobileNumber));
		CustomerDTO customerDTO = new CustomerDTO();
		if (response != null) {
			customerDTO.setMessage("success");
			customerDTO.setStatus("AE");
			customerDTO.setResponseCode(200);
			customerDTO.setUserResponse(convertToUserResponse(response));
		} else {
			try {
				/*String otp = sendmail();
				customerDTO.setMessage("OTP - " + otp);*/
                customerDTO.setStatus("NE");
                customerDTO.setResponseCode(200);
            } catch (Exception e) {
				e.printStackTrace();
			}
		}
		return customerDTO;
	}

	public ResponseEntity<ResponseMessage> forgetPassword(Long mobileNumber,
														  HttpServletRequest request,
														  HttpSession httpSession)
			throws AddressException, MessagingException, IOException {
		Customer response = repository.findByMobileNumber(mobileNumber);
		ResponseMessage responseMessage = ResponseMessage.getInstance();
		if (response != null) {
			try {
                responseMessage.setMessage("Already registered");
                responseMessage.setResponseStatus("success");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            responseMessage.setMessage("Not registered");
            responseMessage.setResponseStatus("error");
        }
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    public ResponseEntity<ResponseMessage> updatePassword(String mobileNumber, String password,
                                                          HttpServletRequest request,
                                                          HttpSession httpSession) {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        try {

            Customer customer = repository.findByMobileNumberAndStatus(
                    Long.parseLong(mobileNumber), 1);
            //customer.setPassword(password);
            if (customer != null) {
                customer.setPassword(password);
                if (repository.save(customer) != null) {
                    responseMessage.setMessage("Password reset successfully");
                    responseMessage.setResponseStatus("success");
                } else {
                    responseMessage.setMessage("Error in password reset ");
                    responseMessage.setResponseStatus("error");
                }
            } else {
                responseMessage.setMessage("otp not verified");
                responseMessage.setResponseStatus("error");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    public Object registerCustomer(String mobileNumber, String password, String adminId,
                                   String outletId, String firstName, String lastName) {
        Customer customer = new Customer();
        long mobile = Long.parseLong(mobileNumber);
        customer.setMobileNumber(mobile);
        customer.setPassword(password);
        customer.setAdminId(adminId);
        customer.setOutletId(outletId);
        customer.setfName(firstName);
        customer.setlName(lastName);
        customer.setStatus(1);
        customer.setAppUser(1);
        Customer mNewCustomer = null;
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        try {
            mNewCustomer = repository.save(customer);
            if (mNewCustomer != null) {
                responseMessage.setMessage("Customer registered successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in customer registration");
                responseMessage.setResponseStatus("error");
            }
        } catch (Exception e) {
            responseMessage.setMessage("Error in customer registration");
            responseMessage.setResponseStatus("error");
            e.printStackTrace();
        }
        return responseMessage;
    }

    public Object getCustomers(String adminId, String pageNumber, String pageSize) {

        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Customer> sliceResult = customerPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);

        List<Customer> customers = sliceResult.toList();
        List<AppUserDTO> customerDTOList = new ArrayList();
        List<Order> orderList = new ArrayList<>();
        if (customers.size() > 0) {
            for (Customer mCustomer : customers) {
                AppUserDTO appUser = new AppUserDTO();
                appUser.setId(mCustomer.getId());
                appUser.setFirstName(mCustomer.getfName());
                appUser.setLastName(mCustomer.getlName());
                appUser.setGender(mCustomer.getGender());
                appUser.setEmailId(mCustomer.getEmailId());
                appUser.setDob(mCustomer.getDob());
                appUser.setCity(mCustomer.getCity());
                appUser.setMobileNumber(mCustomer.getMobileNumber());
                appUser.setPincode(mCustomer.getPincode());
                appUser.setRewardsPoints(mCustomer.getRewardsPoints());
                List<DeliveryAddress> addresses = new ArrayList<>();
                addresses = mCustomer.getDeliveryAddress();
                if (addresses.size() > 0)
                    appUser.setDeliveryAddress(addresses.get(0).getAddress());
                orderList = orderRepository.findByCustomerIdAndOrderStatusAndStatus(mCustomer.getId(),
                        "confirmed", 1);
                appUser.setTotalOrders(orderList.size());
                customerDTOList.add(appUser);
            }
        }
        GenericData<AppUserDTO> data = new GenericData(customerDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object findCustomer(String id) {
        Customer customer = repository.findByIdAndStatus(id, 1);
      /*HashMap<String, T> map = new HashMap<>();
      map.put("id",id);
      */
        return customer;
    }

    public Object updateDeliveryAddress(DeliveryAddress deliveryAddress) {
        Customer customer = repository.findByIdAndStatus(deliveryAddress.getCustomerId(), 1);
        List<DeliveryAddress> addressList = new ArrayList<>();
        List<DeliveryAddress> existingAdressList = customer.getDeliveryAddress();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (customer != null) {

            for (DeliveryAddress mAddress : existingAdressList) {
                addressList.add(mAddress);
            }
            addressList.add(deliveryAddress);
            customer.setDeliveryAddress(addressList);
            Customer mCustomer = repository.save(customer);
            if (mCustomer != null) {
                responseMessage.setMessage("Delivery address added successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error id added Delivery address");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public Object registeredToken(String id, String fcmToken) {
        Customer customer = repository.findByIdAndStatus(id, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (customer != null) {
            customer.setFcmToken(fcmToken);
            if (repository.save(customer) != null) {
                responseMessage.setResponseStatus("success");
                responseMessage.setMessage("Token registered with customer");
            }
        }
        return responseMessage;
    }


}
