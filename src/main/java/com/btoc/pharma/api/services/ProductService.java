package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.*;
import com.btoc.pharma.api.dto.ProductDTO;
import com.btoc.pharma.api.model.*;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository repository;

    @Autowired
    private MongoOperations mongoOperations;

    @Autowired
    private AWSS3Service awss3Service;

    @Autowired
    private OutletRepository outletRepository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private UserName userName;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ProductPaginationRepository productPaginationRepository;

    private FileStorageProperties fileStorageProperties;

    @Autowired
    private BaseUrl baseUrl;

    public Object createProduct(ProductDTO productDTO) {

        Product product = convertTo(productDTO, "create");
        Product productDb = repository.save(product);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (productDb != null) {
            responseMessage.setMessage("Product created successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in product creation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object updateProduct(ProductDTO productDTO) {

        Product product = convertTo(productDTO, "update");
        Product productDb = null;
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (product != null)
            productDb = repository.save(product);
        if (productDb != null) {
            responseMessage.setMessage("Product updation successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in product updation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public List<ProductDTO> getProductbyBranch(long branchId) {

        List<ProductDTO> productDTOList = new ArrayList<>();

        return productDTOList;
    }

    private ProductDTO convertToDTO(Product product) {

        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setUserId(product.getUserId());
        productDTO.setAdminId(product.getAdminId());
        // productDTO.setOutletId(product.getOutletId());
        productDTO.setCompanyId(product.getCompanyId());
        productDTO.setGroupId(product.getGroupId());
        productDTO.setCategoryId(product.getCategoryId());
        productDTO.setCompanyName(getCompanyName(product.getCompanyId()));
        productDTO.setGroupName(getGroupName(product.getGroupId()));
        productDTO.setCategoryName(getCategoryName(product.getCategoryId()));
        productDTO.setProductName(product.getProductName());
        productDTO.setShortDescription(product.getShortDescription());
        productDTO.setFullDescription(product.getFullDescription());
        productDTO.setPrescriptionType(product.getPrescriptionType());
        productDTO.setMrp(product.getMrp());
        productDTO.setPurchaseRate(product.getPurchaseRate());
        productDTO.setRatePerPiece(product.getRatePerPiece());
        productDTO.setDiscountAmount(product.getDiscountAmount());
        productDTO.setDiscountType(product.getDiscountType());
        productDTO.setDiscount(product.getDiscount());
        productDTO.setQuantity(product.getQuantity());
        productDTO.setPacking(product.getPacking());
        productDTO.setHsnNumber(product.getHsnNumber());
        productDTO.setsTax(product.getsTax());
        productDTO.setcTax(product.getcTax());
        productDTO.setiTax(product.getiTax());
        productDTO.setTaxStatus(product.getTaxStatus());
        productDTO.setTopTrending(product.getTopTrending());
        if (product.getImagePath() != null)
            productDTO.setImagePath(baseUrl.getBASE_URL() + product.getImagePath());
        productDTO.setStatus(product.getStatus());
        return productDTO;
    }

    public String getCompanyName(String id) {
        Company company = companyRepository.findByIdAndStatus(id, 1);
        String companyName = null;
        if (company != null) {
            companyName = company.getCompanyName();
        }
        return companyName;
    }

    public String getGroupName(String id) {
        Group group = groupRepository.findByIdAndStatus(id, 1);
        String groupName = null;
        if (group != null) {
            groupName = group.getGroupName();
        }
        return groupName;
    }

    public String getCategoryName(String id) {
        Category category = categoryRepository.findByIdAndStatus(id, 1);
        String categoryName = null;
        if (category != null) {
            categoryName = category.getCategoryName();
        }
        return categoryName;
    }

    public List<ProductDTO> getProductbyCategory(long branchId, String categoryId) {
        //List<Product> productList = repository.findByBranchIdAndCategoryId(branchId, categoryId);
        List<ProductDTO> productDTOList = new ArrayList<>();

        return productDTOList;
    }

    public Product convertTo(ProductDTO productDTO, String key) {
        Product product = null;
        String imagePath = null;

        if (key.equalsIgnoreCase("create")) {
            product = new Product();
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            product.setStatus(1);
            product.setCreatedAt(createdAt);
            product.setUpdatedAt(createdAt);
            if (productDTO.getUserId() != null) {
                product.setCreatedBy(userName.getUserName(productDTO.getUserId()));
                product.setUpdatedBy(userName.getUserName(productDTO.getUserId()));
            } else {
                product.setCreatedBy(userName.getUserName(productDTO.getAdminId()));
                product.setUpdatedBy(userName.getUserName(productDTO.getAdminId()));
            }
            product.setTopTrending(productDTO.getTopTrending());
            if (productDTO.getDiscountType().equalsIgnoreCase("percentage")) {
                double discountAmount = (productDTO.getMrp() * productDTO.getDiscount()) / 100;
                product.setDiscountAmount(discountAmount);
            } else if (productDTO.getDiscountType().equalsIgnoreCase("amount")) {
                product.setDiscountAmount(productDTO.getDiscount());
            }
            if (productDTO.getProductImage() != null) {
                imagePath = getUserIdAndOutletId(productDTO);
            }
            if (imagePath != null)
                product.setImagePath(imagePath);
        } else {
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            product = repository.findByIdAndStatus(productDTO.getId(), 1);
            if (product != null) {
                product.setUpdatedAt(createdAt);
                product.setUpdatedBy(userName.getUserName(productDTO.getUserId()));
                product.setTopTrending(productDTO.getTopTrending());
                double quantity = productDTO.getQuantity();
                if (productDTO.getDiscountType().equalsIgnoreCase("percentage")) {
                    double discountAmount = (productDTO.getMrp() * productDTO.getDiscount()) / 100;
                    product.setDiscountAmount(discountAmount);
                } else if (productDTO.getDiscountType().equalsIgnoreCase("amount")) {
                    product.setDiscountAmount(productDTO.getDiscount());
                }
                if (productDTO.getProductImage() != null) {
                    imagePath = getUserIdAndOutletId(productDTO);
                }
                if (imagePath != null)
                    product.setImagePath(imagePath);
            }
        }
        if (product != null) {
            product.setUserId(productDTO.getUserId());
            product.setAdminId(productDTO.getAdminId());
            // product.setOutletId(productDTO.getOutletId());
            product.setCompanyId(productDTO.getCompanyId());
            product.setGroupId(productDTO.getGroupId());
            product.setCategoryId(productDTO.getCategoryId());
            product.setProductName(productDTO.getProductName());
            product.setShortDescription(productDTO.getShortDescription());
            product.setFullDescription(productDTO.getFullDescription());
            product.setPrescriptionType(productDTO.getPrescriptionType());
            product.setMrp(productDTO.getMrp());
            product.setPurchaseRate(productDTO.getPurchaseRate());
            product.setDiscount(productDTO.getDiscount());
            product.setDiscountType(productDTO.getDiscountType());
            double amount = productDTO.getMrp() / productDTO.getQuantity();
            product.setRatePerPiece(amount);
            product.setQuantity(productDTO.getQuantity());
            product.setPacking(productDTO.getPacking());
            if (productDTO.getTaxStatus().equalsIgnoreCase("yes")) {
                product.setHsnNumber(productDTO.getHsnNumber());
                product.setsTax(productDTO.getsTax());
                product.setcTax(productDTO.getcTax());
                product.setiTax(productDTO.getiTax());
            }
            product.setTaxStatus(productDTO.getTaxStatus());
           /* product.setDiscountAmout(productDTO.getDiscountAmout());
            product.setDiscountPer(productDTO.getDiscountPer());*/
        }
        return product;
    }

    public String getUserIdAndOutletId(ProductDTO productDTO) {
        String dir = null;
        String imagePath = null;
        dir = productDTO.getAdminId() + "_" + "products";
        imagePath = awss3Service.uploadFile(productDTO.getProductImage(), dir);
        return imagePath;
    }

    public List<ProductDTO> getProducts(String id) {
        // List<Product> product = repository.findByOutletIdAndStatus(id, 1);
        List<Product> product = repository.findByAdminIdAndStatus(id, 1);
        List<ProductDTO> productDTO = new ArrayList<>();
        if (product.size() > 0) {
            for (Product mProduct : product) {
                productDTO.add(convertToDTO(mProduct));
            }
        }
        return productDTO;
    }

    public Object fetchProduct(String id) {
        Product product = repository.findByIdAndStatus(id, 1);
        ProductDTO productDTO = new ProductDTO();
        if (product != null) {
            productDTO = convertToDTO(product);
        }
        return productDTO;
    }

    public Object deleteProduct(String id) {
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        Product product = repository.findByIdAndStatus(id, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (product != null) {
            product.setUpdatedAt(createdAt);
            product.setUpdatedBy(product.getUpdatedBy());
            product.setStatus(0);
            Product mUpdatedProduct = repository.save(product);
            if (mUpdatedProduct != null) {
                responseMessage.setMessage("product deleted successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in product deletion");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public Object fetchAllProduct(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Product> sliceResult = productPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Product> list = sliceResult.toList();
        List<ProductDTO> productDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Product mProduct : list) {
                productDTOList.add(convertToDTO(mProduct));
            }
        }
        GenericData<ProductDTO> data = new GenericData(productDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public JSONArray getAllProducts(String id, String key) throws JSONException {
        JSONArray productList = new JSONArray();
        List<Product> list = null;
        if (key.equalsIgnoreCase("company")) {
            list = new ArrayList<>();
            list = repository.findByCompanyIdAndStatus(id, 1);
        } else if (key.equalsIgnoreCase("category")) {
            list = new ArrayList<>();
            list = repository.findByCategoryIdAndStatus(id, 1);
        } else if (key.equalsIgnoreCase("trending")) {
            list = new ArrayList<>();
            list = repository.findTop10ByAdminIdAndTopTrendingAndStatus(id, "yes", 1);
        } else if (key.equalsIgnoreCase("Alltrending")) {
            list = new ArrayList<>();
            list = repository.findByAdminIdAndTopTrendingAndStatus(id, "yes", 1);
        }
        if (list != null) {
            for (Product mList : list) {
                JSONObject response = new JSONObject();
                JsonObject re = new JsonObject();
                response.put("productId", mList.getId());
                response.put("adminId", mList.getAdminId());
                response.put("companyId", mList.getCompanyId());
                response.put("groupId", mList.getGroupId());
                response.put("categoryId", mList.getCategoryId());
                response.put("productName", mList.getProductName());
                response.put("shortDescription", mList.getShortDescription());
                response.put("fullDescription", mList.getFullDescription());
                response.put("imagePath", baseUrl.getBASE_URL() + mList.getImagePath());
                response.put("prescriptionType", mList.getPrescriptionType());
                response.put("mrp", mList.getMrp());
                response.put("purchaseRate", mList.getPurchaseRate());
                response.put("quantity", mList.getQuantity());
                response.put("packing", mList.getPacking());
                response.put("ratePerPiece", mList.getRatePerPiece());
                response.put("hsnNumber", mList.getHsnNumber());
                response.put("sTax", mList.getsTax());
                response.put("cTax", mList.getcTax());
                response.put("iTax", mList.getiTax());
                response.put("taxStatus", mList.getTaxStatus());
                response.put("discountType", mList.getDiscountType());
                response.put("discount", mList.getDiscount());
                response.put("discountAmount", mList.getDiscountAmount());
                response.put("status", mList.getStatus());
                productList.put(response);
            }
        }
        return productList;
    }

    public JSONArray getRelatedProducts(String id, String key) throws JSONException {
        JSONArray productList = new JSONArray();
        List<Product> list = null;
        list = new ArrayList<>();
        list = repository.findTop10ByAdminIdAndCompanyIdAndStatusOrderByIdDesc(id, key, 1);
        if (list != null) {
            for (Product mList : list) {
                JSONObject response = new JSONObject();
                response.put("productId", mList.getId());
                response.put("adminId", mList.getAdminId());
                response.put("companyId", mList.getCompanyId());
                response.put("groupId", mList.getGroupId());
                response.put("categoryId", mList.getCategoryId());
                response.put("productName", mList.getProductName());
                response.put("shortDescription", mList.getShortDescription());
                response.put("fullDescription", mList.getFullDescription());
                response.put("imagePath", baseUrl.getBASE_URL() + mList.getImagePath());
                response.put("prescriptionType", mList.getPrescriptionType());
                response.put("mrp", mList.getMrp());
                response.put("purchaseRate", mList.getPurchaseRate());
                response.put("quantity", mList.getQuantity());
                response.put("packing", mList.getPacking());
                response.put("ratePerPiece", mList.getRatePerPiece());
                response.put("hsnNumber", mList.getHsnNumber());
                response.put("sTax", mList.getsTax());
                response.put("cTax", mList.getcTax());
                response.put("iTax", mList.getiTax());
                response.put("taxStatus", mList.getTaxStatus());
                response.put("discountType", mList.getDiscountType());
                response.put("discount", mList.getDiscount());
                response.put("discountAmount", mList.getDiscountAmount());
                response.put("status", mList.getStatus());
                productList.put(response);
            }
        }
        return productList;
    }


    public JSONArray searchProducts(String adminId, String key) throws JSONException {

        JSONArray productList = new JSONArray();
        List<Product> list = null;

        list = repository.findByAdminIdAndProductNameIgnoreCaseContainingAndStatus(adminId, key, 1);
        for (Product mList : list) {
            JSONObject response = new JSONObject();
            response.put("productId", mList.getId());
            response.put("adminId", mList.getAdminId());
            response.put("companyId", mList.getCompanyId());
            response.put("groupId", mList.getGroupId());
            response.put("categoryId", mList.getCategoryId());
            response.put("productName", mList.getProductName());
            response.put("shortDescription", mList.getShortDescription());
            response.put("fullDescription", mList.getFullDescription());
            response.put("imagePath", baseUrl.getBASE_URL() + mList.getImagePath());
            response.put("prescriptionType", mList.getPrescriptionType());
            response.put("mrp", mList.getMrp());
            response.put("purchaseRate", mList.getPurchaseRate());
            response.put("quantity", mList.getQuantity());
            response.put("packing", mList.getPacking());
            response.put("ratePerPiece", mList.getRatePerPiece());
            response.put("hsnNumber", mList.getHsnNumber());
            response.put("sTax", mList.getsTax());
            response.put("cTax", mList.getcTax());
            response.put("iTax", mList.getiTax());
            response.put("taxStatus", mList.getTaxStatus());
            response.put("discountType", mList.getDiscountType());
            response.put("discount", mList.getDiscount());
            response.put("discountAmount", mList.getDiscountAmount());
            response.put("status", mList.getStatus());
            productList.put(response);
        }
        return productList;
    }

    public Object updateRatePerPiece(String id, String ratePerPiece, String discountAmount) {
        double rate_piece = Double.parseDouble(ratePerPiece);
        Product product = repository.findByIdAndStatus(id, 1);
        product.setRatePerPiece(rate_piece);
        product.setMrp(rate_piece * product.getQuantity());
        String discountType = product.getDiscountType();
        double discountAmt = Double.parseDouble(discountAmount);
        if (discountType.equalsIgnoreCase("percentage") ||
                discountType.equalsIgnoreCase("per")) {

            double disc = (discountAmt * 100) / product.getMrp();
            product.setDiscount(disc);
            product.setDiscountAmount(discountAmt);
        } else {
            product.setDiscount(discountAmt);
            product.setDiscountAmount(discountAmt);
        }
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        try {
            repository.save(product);
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("200");
        } catch (Exception e) {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("400");
        }
        return responseMessage;
    }
}
