package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.RolePaginationRepository;
import com.btoc.pharma.api.dao.RoleRepository;
import com.btoc.pharma.api.dao.UserpermissiosRepository;
import com.btoc.pharma.api.dto.RolesDTO;
import com.btoc.pharma.api.model.*;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class RoleService {

    @Autowired
    private RoleRepository repository;
    @Autowired
    private UserpermissiosRepository userpermissiosRepository;

    @Autowired
    private RolePaginationRepository rolePaginationRepository;

    public Object registerRoles(Roles role) {
        Roles mRole = repository.findByRoleNameAndAdminIdAndStatus(role.getRoleName(), role.getAdminId(), 1);
        if (mRole != null) {
            ResponseMessage responseMessage = ResponseMessage.getInstance();
            responseMessage.setMessage("Role already exist");
            responseMessage.setResponseStatus("200");
            return responseMessage;
        } else {
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            role.setCreatedAt(createdAt);
            role.setUpdatedAt(createdAt);
            role.setStatus(1);
            role.setRoleName(role.getRoleName().toLowerCase());
            RolesDTO rolesDTO = convertToDTO(repository.save(role));
            if (rolesDTO != null) {
                UserPermission userPermission = new UserPermission();
                userPermission.setRoleId(role.getId());
                userPermission.setUserId(role.getUserId());

                /** Configure Master Permissions  **/
                MasterPolicies masterPolicies = new MasterPolicies();
                masterPolicies.setBranchPermission("none");
                masterPolicies.setCatlogPermission("none");
                masterPolicies.setOutletPermission("none");
                masterPolicies.setProductPermission("none");
                masterPolicies.setRewardPermission("none");
                masterPolicies.setRolePermission("none");
                masterPolicies.setUserPermission("none");

                /** Customer Permissions **/
                CustomerPolicies customerPolicies = new CustomerPolicies();
                customerPolicies.setCustomerPermission("none");

                /** Order Permissions **/
                OrderPolicies orderPolicies = new OrderPolicies();
                orderPolicies.setOrderPermission("none");

                /** Report Permissions **/

                ReportsPolicies reportsPolicies = new ReportsPolicies();
                reportsPolicies.setDeliveryPermission("none");
                reportsPolicies.setPaymentPermission("none");
                reportsPolicies.setReportsPermission("none");

                /** set Policies to Permissions **/
                userPermission.setMaster(masterPolicies);
                userPermission.setCustomer(customerPolicies);
                userPermission.setOrder(orderPolicies);
                userPermission.setReports(reportsPolicies);

                userpermissiosRepository.save(userPermission);
                rolesDTO.setMessage("Role created successfully");
                rolesDTO.setResponseCode("success");
            } else {
                rolesDTO.setMessage("Role creation error");
                rolesDTO.setResponseCode("error");
            }
            return rolesDTO;
        }
    }

    public RolesDTO updateRoles(String id, String roleName) {
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        Roles roles = null;
        RolesDTO rolesDTO = null;
        try {
            Optional<Roles> entity = repository.findByIdAndStatus(id, 1);
            roles = entity.get();
            roles.setUpdatedAt(mUpdatedAt);
            roles.setRoleName(roleName);
            Roles updatedRoles = repository.save(roles);
            rolesDTO = convertToDTO(updatedRoles);
            if (rolesDTO != null) {
                rolesDTO.setMessage("Role updated scucessfully");
                rolesDTO.setResponseCode("success");
            } else {
                rolesDTO.setMessage("Error in role updatations");
                rolesDTO.setResponseCode("error");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rolesDTO;
    }

    public Object deleteRoles(String id) {
        Optional<Roles> mRole = repository.findById(id);
        Roles mUpdatedRoles = mRole.get();
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        mUpdatedRoles.setUpdatedAt(mUpdatedAt);
        mUpdatedRoles.setStatus(0);
        ResponseMessage response = ResponseMessage.getInstance();
        try {
            Roles entity = repository.save(mUpdatedRoles);
            response.setMessage("Role deleted successfully");
            response.setResponseStatus("success");
        } catch (Exception e) {
            response.setMessage("Error in role deletions");
            response.setResponseStatus("error");
            e.printStackTrace();
        }
        return response;
    }

    public List<RolesDTO> getRoles(String userId) {

        List<Roles> list = repository.findByAdminIdAndStatus(userId, 1);
        List<RolesDTO> rolesDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Roles mRoles : list) {
                rolesDTOList.add(convertToDTO(mRoles));
            }
        }
        return rolesDTOList;
    }

    public RolesDTO convertToDTO(Roles roles) {
        RolesDTO rolesDTO = new RolesDTO();
        Total total = new Total();
        rolesDTO.setId(roles.getId());
        rolesDTO.setRoleName(roles.getRoleName());
        rolesDTO.setStatus(roles.getStatus());
        rolesDTO.setUserId(roles.getUserId());
        rolesDTO.setAdminId(roles.getAdminId());
        rolesDTO.setResponseCode("success");
        rolesDTO.setCreatedAt(roles.getCreatedAt());
        rolesDTO.setUpdatedAt(roles.getUpdatedAt());
        return rolesDTO;
    }

    public Object getPermission(String userId, String roleId) {

        UserPermission userPermission = userpermissiosRepository.findByUserIdAndRoleId(userId, roleId);
        if (userPermission != null) {
            return userPermission;
        } else {
            return null;
        }
    }

    public Object updatePermission(UserPermission userPermission) {
        Optional<UserPermission> userPermissionOptional = userpermissiosRepository.
                findById(userPermission.getId());
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        UserPermission mUpdatedPermssion = userPermissionOptional.get();

        mUpdatedPermssion.setMaster(userPermission.getMaster());
        mUpdatedPermssion.setCustomer(userPermission.getCustomer());
        mUpdatedPermssion.setOrder(userPermission.getOrder());
        mUpdatedPermssion.setReports(userPermission.getReports());

        if (userpermissiosRepository.save(mUpdatedPermssion) != null) {
            responseMessage.setMessage("Role configured success");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in role configurations");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public GenericData<?> getAllRoles(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Roles> sliceResult = rolePaginationRepository.findByAdminIdAndStatus(adminId, 1, pageable);
        //  Page<Roles> sliceResult = rolePaginationRepository.findAll(pageable);
        //Page<Roles> sliceResult = rolePaginationRepository.findAll(pageable);

        List<Roles> list = sliceResult.toList();
        List<RolesDTO> rolesDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Roles mRoles : list) {
                rolesDTOList.add(convertToDTO(mRoles));
            }
        }
        GenericData<RolesDTO> data = new GenericData(rolesDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }
}
