package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.LoyaltyPaginationRepository;
import com.btoc.pharma.api.dao.LoyaltyRepository;
import com.btoc.pharma.api.dao.OutletRepository;
import com.btoc.pharma.api.dto.LoyaltyDTO;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.Loyalty;
import com.btoc.pharma.api.model.Outlet;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LoyaltyService {

    @Autowired
    private LoyaltyRepository loyaltyRepository;
    @Autowired
    private LoyaltyPaginationRepository loyaltyPaginationRepository;
    @Autowired
    private OutletRepository outletRepository;


    public ResponseMessage registerLoyalty(Loyalty loyalty) {
        String mDate = DateAndTimeUtil.getDateAndTimeUtil();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        loyalty.setStatus(1);
        loyalty.setCreatedAt(mDate);
        loyalty.setUpdatedAt(mDate);
//        loyalty.setUserId();
        Loyalty newLoyalty = loyaltyRepository.save(loyalty);
        if (newLoyalty != null) {
            responseMessage.setMessage("Loyalty created successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in loyalty creation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object updateLoyalty(Loyalty loyalty) {

        Loyalty mLoyalty = loyaltyRepository.findByIdAndStatus(loyalty.getId(), 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (mLoyalty != null) {
            mLoyalty.setFirstOrderPoints(loyalty.getFirstOrderPoints());
            mLoyalty.setMinAmount(loyalty.getMinAmount());
            mLoyalty.setLoyaltyPoint(loyalty.getLoyaltyPoint());
            if (loyaltyRepository.save(mLoyalty) != null) {
                responseMessage.setMessage("Loyalty updated successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in updation");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public Object fetchAllLoyalty(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Loyalty> sliceResult = loyaltyPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Loyalty> list = sliceResult.toList();
        List<LoyaltyDTO> loyaltyDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Loyalty mLoyalty : list) {
                loyaltyDTOList.add(convertToDTO(mLoyalty));
            }
        }
        GenericData<LoyaltyDTO> data = new GenericData(loyaltyDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    private LoyaltyDTO convertToDTO(Loyalty loyalty) {

        String outletName = null;

        Outlet outlet = outletRepository.findByIdAndStatus(loyalty.getOutletId(), 1);

        if (outlet != null) {
            outletName = outlet.getOutletName();
        }
        LoyaltyDTO loyaltyDTO = new LoyaltyDTO();
        loyaltyDTO.setId(loyalty.getId());
        loyaltyDTO.setAdminId((loyalty.getAdminId()));
        loyaltyDTO.setUserId(loyalty.getUserId());
        loyaltyDTO.setOutletId(loyalty.getOutletId());
        loyaltyDTO.setIsfirstOrder(loyalty.isIsfirstOrder());
        loyaltyDTO.setIsminOrder(loyalty.isIsminOrder());
        loyaltyDTO.setFirstOrderPoints(loyalty.getFirstOrderPoints());
        loyaltyDTO.setMinAmount(loyalty.getMinAmount());
        loyaltyDTO.setLoyaltyPoint(loyalty.getLoyaltyPoint());
        loyaltyDTO.setOutletName(outletName);
        loyaltyDTO.setStatus(loyalty.getStatus());
//        loyaltyDTO.setCreatedAt(mDate);
//        loyaltyDTO.setUpdatedAt(mDate);
        return loyaltyDTO;
    }
   /* public Outlet getOutlet(String outletId, int i) {
        Outlet outlet = outletRepository.findByIdAndStatus(outletId, i);
        return outlet;
    }*/
}
