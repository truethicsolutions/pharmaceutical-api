package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.CityRepository;
import com.btoc.pharma.api.model.City;
import com.btoc.pharma.api.model.States;
import com.btoc.pharma.api.model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CityService {

    @Autowired
    private CityRepository cityRepository;

    public List<City> getCity(String stateCode) {
        List<City> list = cityRepository.findAllByStateCode(stateCode);
        if (list.size() > 0) {
            return list;
        } else
            return null;



    }


}
