package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.FiltersRepository;
import com.btoc.pharma.api.dto.FiltersDTO;
import com.btoc.pharma.api.model.Filters;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FiltersService {

    @Autowired
    private FiltersRepository repository;

    public ResponseEntity<ResponseMessage> createFilters(FiltersDTO filtersDTO) {
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        filtersDTO.setCreatedAt(createdAt);
        filtersDTO.setUpdatedAt(createdAt);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        Filters filters = new Filters();
        filters.setCategoryId(filtersDTO.getCategoryId());
        filters.setBranchId(filtersDTO.getBranchId());
        filters.setTitle(filtersDTO.getTitle());
        filters.setStatus(1);
        filters.setCreatedAt(filtersDTO.getCreatedAt());
        filters.setCreatedBy(filtersDTO.getCreatedBy());
        filters.setUpdatedAt(filtersDTO.getUpdatedAt());
        filters.setUpdatedBy(filters.getUpdatedBy());
        Filters mFilters = repository.save(filters);
        if (mFilters != null) {
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("1");
        } else {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("0");
        }
        return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
    }

    public ResponseEntity<ResponseMessage> updateFilters(FiltersDTO filtersDTO) {
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        filtersDTO.setCreatedAt(createdAt);
        filtersDTO.setUpdatedAt(createdAt);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        Filters filters = new Filters();
        filters.setCategoryId(filtersDTO.getCategoryId());
        filters.setBranchId(filtersDTO.getBranchId());
        filters.setTitle(filtersDTO.getTitle());
        filters.setStatus(1);
        filters.setCreatedAt(filtersDTO.getCreatedAt());
        filters.setCreatedBy(filtersDTO.getCreatedBy());
        filters.setUpdatedAt(filtersDTO.getUpdatedAt());
        filters.setUpdatedBy(filters.getUpdatedBy());
        Filters mFilters = repository.save(filters);
        if (mFilters != null) {
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("1");
        } else {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("0");
        }
        return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
    }

    public FiltersDTO getFilters(String id, String categoryId) {
        Filters filters = repository.findByIdAndCategoryId(id, categoryId);
        return convertToDTO(filters);
    }

    public List<FiltersDTO> getAllFilters(String categoryId) {
        List<Filters> filtersList = repository.findByCategoryId(categoryId);
        List<FiltersDTO> filtersDTOList = new ArrayList<>();
        for (Filters mFilters : filtersList) {
            filtersDTOList.add(convertToDTO(mFilters));
        }
        return filtersDTOList;

    }

    public FiltersDTO convertToDTO(Filters filters) {
        FiltersDTO filtersDTO = new FiltersDTO();
        if (filters != null) {
            filtersDTO.setBranchId(filters.getBranchId());
            filtersDTO.setCategoryId(filters.getCategoryId());
            filtersDTO.setTitle(filters.getTitle());
            filtersDTO.setStatus(filters.getStatus());
            filtersDTO.setCreatedAt(filters.getCreatedAt());
            filtersDTO.setCreatedBy(filters.getCreatedBy());
            filtersDTO.setUpdatedAt(filters.getUpdatedAt());
            filtersDTO.setUpdatedBy(filters.getUpdatedBy());
        }
        return filtersDTO;
    }
}
