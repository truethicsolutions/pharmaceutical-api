package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.PoliciesRepository;
import com.btoc.pharma.api.model.Policies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PoliciesService {

	@Autowired
	private PoliciesRepository repository;

	public Policies registerPolicies(Policies entity) {
		Policies policies = null;
		try {
			if (entity != null) {
				policies = repository.save(entity);

			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return policies;

	}

	public Policies updatePolicies(Policies entity) {
		Policies policies = null;
		try {
			if (entity != null) {
				policies = repository.save(entity);

			}
		} catch (Exception e) {
			e.printStackTrace();

		}

		return policies;
	}

	public List<Policies> getAllPolicies() {

		List<Policies> list = repository.findAll();

		return list;
	}

}
