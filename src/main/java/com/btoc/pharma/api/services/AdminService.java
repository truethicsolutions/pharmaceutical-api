package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.AdminRepository;
import com.btoc.pharma.api.dto.AdminDTO;
import com.btoc.pharma.api.model.Admin;
import com.btoc.pharma.api.model.AdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {
	@Autowired
	AdminRepository repository;

	public ResponseMessage registerAdmin(Admin entity) {
		entity.setStatus(1);
		Admin admin = repository.save(entity);
		//AdminDTO adminDTO = new AdminDTO();
		ResponseMessage responseMessage = ResponseMessage.getInstance();
		if (admin != null) {
			responseMessage.setMessage("success");
			responseMessage.setResponseStatus("200");

		} else {
			responseMessage.setMessage("error");
			responseMessage.setResponseStatus("400");
		}
		return responseMessage;
	}

	public AdminDTO updateAdmin(Admin entity) {
		/*if (entity != null) {
			Admin admin = repository.findById(entity.getId());
			try {
				admin = repository.save(admin);
			} catch (Exception e) {
				e.printStackTrace();
			}

			return admin;
		} else*/
		return null;
	}

	public List<Admin> getAllAdmin() {
		List<Admin> list = repository.findAll();
		if (list.size() > 0)
			return list;
		else
			return list;

	}

	public AdminDTO getAdmin(String adminId) {
		/*AdminDTO admin = repository.findByAdminId(adminId);
		if (admin != null)
			return admin;
		else
			return admin;*/
		return null;
	}

	/*public AdminDTO loginAdmin(Admin entity) {
        Admin admin = repository.findByUsernameAndPassword(entity.getUsername(), entity.getPassword());
        AdminDTO adminDTO = new AdminDTO();
        if (admin != null) {
            adminDTO.setMessage("success");
            adminDTO.setResponseCode(200);
            adminDTO.setData(convertToResponse(admin));
        } else {
            adminDTO.setMessage("error");
            adminDTO.setResponseCode(400);
            adminDTO.setData(null);
        }
		return adminDTO;
	}*/

	public AdminResponse convertToResponse(Admin adminDTO) {
		AdminResponse adminResponse = new AdminResponse();
		adminResponse.setFname(adminDTO.getFname());
		adminResponse.setLname(adminDTO.getLname());
		adminResponse.setEmail(adminDTO.getEmail());
		adminResponse.setRoleName(adminDTO.getRoleName());
		adminResponse.setCode(adminDTO.getCode());
		adminResponse.setUsername(adminDTO.getUsername());
		adminResponse.setStatus(adminDTO.getStatus());
		return adminResponse;
	}
}
