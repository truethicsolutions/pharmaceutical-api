package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.BranchRepository;
import com.btoc.pharma.api.dao.OutletPaginationRepository;
import com.btoc.pharma.api.dao.OutletRepository;
import com.btoc.pharma.api.dao.UsersRepository;
import com.btoc.pharma.api.dto.OutletDTO;
import com.btoc.pharma.api.model.Branch;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.Outlet;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class OutletService {

    @Autowired
    private OutletRepository repository;
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private OutletPaginationRepository outletPaginationRepository;

    public OutletDTO createOutlet(OutletDTO outletDTO) {
        try {
            Outlet entity = repository.save(convertTo(outletDTO, "register"));
            if (entity != null) {
                outletDTO.setMessage("Outlet created successfully");
                outletDTO.setResponseStatus("success");
                outletDTO.setId(entity.getId());
                outletDTO.setStatus(1);
            } else {
                outletDTO.setMessage("Error in outlet creation");
                outletDTO.setResponseStatus("error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            outletDTO.setMessage("Error in outlet creation");
            outletDTO.setResponseStatus("error");
        }
        return outletDTO;
    }

    public OutletDTO updateOutlet(OutletDTO outletDTO) {
        try {
            Outlet outlet = repository.save(convertTo(outletDTO, "update"));
            if (outlet != null) {
                outletDTO.setMessage("Outlet updated successfully");
                outletDTO.setResponseStatus("success");
            } else {
                outletDTO.setMessage("Error in outlet updation");
                outletDTO.setResponseStatus("error");
            }
        } catch (Exception e) {
            outletDTO.setMessage("Error in outlet updation");
            outletDTO.setResponseStatus("error");
            e.printStackTrace();
        }
        return outletDTO;
    }

   /* public Outlet getOutlet(String userId) {
    }

    public List<Outlet> getBranchOutlets(String branchId) {
    }*/

    private Outlet convertTo(OutletDTO outletDTO, String register) {

        Outlet outlet = null;
        LocalDateTime myDateObj = LocalDateTime.now();
        String mCreatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        if (register.equalsIgnoreCase("register")) {
            outlet = new Outlet();
            outlet.setStatus(1);
            outlet.setUserId(outletDTO.getUserId());
            outlet.setAdminId(outletDTO.getAdminId());
            outlet.setCreatedAt(mCreatedAt);
            outlet.setUpdatedAt(mCreatedAt);
        } else {
            outlet = repository.findByIdAndStatus(outletDTO.getId(), 1);
            outlet.setUpdatedAt(mCreatedAt);
        }
        outlet.setBranchId(outletDTO.getBranchId());
        outlet.setOutletName(outletDTO.getOutletName());
        outlet.setLicenseNumber(outletDTO.getLicenseNumber());
        outlet.setLicenseExpiry(outletDTO.getLicenseExpiry());
        outlet.setGstNumber(outletDTO.getGstNumber());
        outlet.setGstExpiry(outletDTO.getGstExpiry());
        outlet.setRegisteredAddress(outletDTO.getRegisteredAddress());
        outlet.setOperationalAddress(outletDTO.getOperationalAddress());
        outlet.setPincode(outletDTO.getPincode());
        outlet.setBankName(outletDTO.getBankName());
        outlet.setAccountNumber(outletDTO.getAccountNumber());
        outlet.setHolderName(outletDTO.getHolderName());
        outlet.setIfscCode(outletDTO.getIfscCode());
        outlet.setBranchOfBank(outletDTO.getBranchOfBank());
        outlet.setPaymentGatewayUrl(outletDTO.getPaymentGatewayUrl());
        return outlet;
    }

    public Object deleteOutlet(String id) {
        Outlet mUpdatedOutlet = repository.findByIdAndStatus(id, 1);
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        mUpdatedOutlet.setUpdatedAt(mUpdatedAt);
        //  mUpdatedTrade.setUpdatedBy(mUpdatedTrade.getUpdatedBy());
        mUpdatedOutlet.setStatus(0);
        ResponseMessage response = ResponseMessage.getInstance();
        try {
            Outlet entity = repository.save(mUpdatedOutlet);
            if (entity != null) {
                response.setMessage("Record deleted successfully");
                response.setResponseStatus("success");
            } else {
                response.setMessage("Error in record deletion");
                response.setResponseStatus("error");
            }

        } catch (Exception e) {
            response.setMessage("Error in record deletion");
            response.setResponseStatus("error");
            e.printStackTrace();
        }
        return response;
    }

    public List<Outlet> getBranchOutlets(String branchId) {
        List<Outlet> outlets = repository.findAllByBranchId(branchId);
        if (outlets.size() > 0) {
            return outlets;
        } else
            return null;
    }

    public Outlet getOutlet(String id) {
        Outlet outlet = repository.findByIdAndStatus(id, 1);
        if (outlet != null) {
            return outlet;
        } else
            return null;
    }

    public List<OutletDTO> getAllOutlets(String userId) {
        List<Outlet> list = repository.findAllByAdminIdAndStatus(userId, 1);

        List<OutletDTO> outletDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Outlet mDto : list) {
                outletDTOList.add(convertToDTO(mDto));
            }
            return outletDTOList;
        } else
            return outletDTOList;
    }

    public OutletDTO convertToDTO(Outlet outlet) {
        OutletDTO outletDTO = new OutletDTO();
        String branchName = null;
        Branch branch = branchRepository.findByIdAndStatus(outlet.getBranchId(), 1);
        if (branch != null)
            branchName = branch.getBranchname();
        outletDTO.setId(outlet.getId());
        outletDTO.setUserId(outlet.getUserId());
        outletDTO.setAdminId(outlet.getAdminId());
        outletDTO.setBranchId(outlet.getBranchId());
        outletDTO.setBranchName(branchName);
        outletDTO.setOutletName(outlet.getOutletName());
        outletDTO.setLicenseNumber(outlet.getLicenseNumber());
        outletDTO.setLicenseExpiry(outlet.getLicenseExpiry());
        outletDTO.setGstNumber(outlet.getGstNumber());
        outletDTO.setGstExpiry(outlet.getGstExpiry());
        outletDTO.setRegisteredAddress(outlet.getRegisteredAddress());
        outletDTO.setOperationalAddress(outlet.getOperationalAddress());
        outletDTO.setPincode(outlet.getPincode());
        outletDTO.setBankName(outlet.getBankName());
        outletDTO.setAccountNumber(outlet.getAccountNumber());
        outletDTO.setHolderName(outlet.getHolderName());
        outletDTO.setIfscCode(outlet.getIfscCode());
        outletDTO.setBranchOfBank(outlet.getBranchOfBank());
        outletDTO.setPaymentGatewayUrl(outlet.getPaymentGatewayUrl());
        outletDTO.setStatus(outlet.getStatus());
        return outletDTO;
    }

    public List<OutletDTO> getBranchOutlet(String branchId) {
        List<Outlet> list = repository.findAllByBranchIdAndStatus(branchId, 1);
        List<OutletDTO> outletDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Outlet mDto : list) {
                outletDTOList.add(convertToDTO(mDto));
            }
            return outletDTOList;
        } else
            return outletDTOList;
    }

    public GenericData findAllOutlet(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Outlet> sliceResult = outletPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Outlet> list = sliceResult.toList();
        List<OutletDTO> OutletDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Outlet mOutlet : list) {
                OutletDTOList.add(convertToDTO(mOutlet));
            }
        }
        GenericData<OutletDTO> data = new GenericData(OutletDTOList, sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }
}
