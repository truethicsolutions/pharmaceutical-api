package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.*;
import com.btoc.pharma.api.dto.CategoryDTO;
import com.btoc.pharma.api.model.BaseUrl;
import com.btoc.pharma.api.model.Category;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.UserName;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repository;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private BranchRepository branchRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    CategoryPaginationRepository categoryPaginationRepository;

    @Autowired
    private AWSS3Service awss3Service;

    @Autowired
    private UserName userName;

    @Autowired
    private BaseUrl baseUrl;
    @Autowired
    private FileStorageService fileStorageService;

    private FileStorageProperties fileStorageProperties;


    public Object createCategory(CategoryDTO categoryDTO) {

        Category mExisting = null;
        mExisting = repository.findByCategoryNameAndAdminIdAndStatus(categoryDTO.getCategoryName(),
                categoryDTO.getAdminId(), 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (mExisting != null) {
            responseMessage.setMessage("Category name already exist");
            responseMessage.setResponseStatus("200");
        } else {
            Category category = convertTo(categoryDTO);
            category.setStatus(1);
            Category mCategory = repository.save(category);
            CategoryDTO mCategoryDTO = new CategoryDTO();
            if (mCategory != null) {
                responseMessage.setMessage("Category created successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in Category creation");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public String getUserIdAndOutletId(CategoryDTO categoryDTO) {
        String dir = null;
        String imagePath = null;
        dir = categoryDTO.getAdminId() + "_" + "categories";
        imagePath = awss3Service.uploadFile(categoryDTO.getCategoryImage(), dir);
        return imagePath;
    }

    private Category convertTo(CategoryDTO categoryDTO) {

        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        String imagePath = null;
        Category category = new Category();
        if (categoryDTO.getCategoryImage() != null) {
            imagePath = getUserIdAndOutletId(categoryDTO);
        }
        try {
            category.setUserId(categoryDTO.getUserId());
            category.setAdminId(categoryDTO.getAdminId());
            /*category.setCompanyId(categoryDTO.getCompanyId());
            category.setGroupId(categoryDTO.getGroupId());*/
            category.setCategoryName(categoryDTO.getCategoryName());
            category.setCreatedAt(createdAt);
            category.setUpdatedAt(createdAt);
            if (categoryDTO.getUserId() != null) {
                category.setCreatedBy(userName.getUserName(categoryDTO.getUserId()));
                category.setUpdatedBy(userName.getUserName(categoryDTO.getUserId()));
            } else {
                category.setCreatedBy(userName.getUserName(categoryDTO.getAdminId()));
                category.setUpdatedBy(userName.getUserName(categoryDTO.getAdminId()));
            }
            //  awss3Service.uploadFile(categoryDTO.getCategoryImage(), dir);
            if (imagePath != null)
                category.setImagePath(imagePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return category;
    }

   /* public ResponseEntity<ResponseMessage> updateCategory(CategoryDTO categoryDTO) {
        String updatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        categoryDTO.setUpdatedAt(updatedAt);
        fileStorageProperties = new FileStorageProperties();
        String branchName = "_dir" + String.valueOf(categoryDTO.getBranchId()); //get from session
        // String industryCode = categoryDTO.getIndustryCode();//get from session
        // get industry code from session, now temporary we are assigning industry code manually,replace it
        // with session value of industry code and create sub directory under branch,here we are
        // not created subdirectory of category
        fileStorageProperties.setUploadDir("./uploads" + "/ph" + "/" + branchName + "/catgory");
        String filename = fileStorageService.storeFile(categoryDTO.getImage(), fileStorageProperties, "_category");
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        try {
            Category category = new Category();
            //  category.setBranchId(categoryDTO.getBranchId());
            category.setCreatedAt(categoryDTO.getCreatedAt());
            category.setCreatedBy(categoryDTO.getCreatedBy());
            category.setUpdatedAt(categoryDTO.getUpdatedAt());
            category.setUpdatedBy(categoryDTO.getUpdatedBy());
            category.setStatus(categoryDTO.getStatus());
            Category crDb = repository.save(category);
            if (crDb != null) {
                responseMessage.setMessage("success");
                responseMessage.setResponseStatus("1");
            } else {
                responseMessage.setMessage("error");
                responseMessage.setResponseStatus("0");
            }
        } catch (Exception e) {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("0");
        }
        return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
    }

    public CategoryDTO getCategory(String id) {
        Optional<Category> category = repository.findById(id);

        return convertToDTO(category);
    }
*/
  /*  public List<CategoryDTO> getAllCategories(long branchId) {
        List<Category> categories = repository.findByBranchId(branchId);
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        for (Category mCategory : categories)
            categoryDTOList.add(convertToDTOCatogory(mCategory));
        return categoryDTOList;

    }*/

    public CategoryDTO convertToDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setStatus(category.getStatus());
        categoryDTO.setId(category.getId());
        categoryDTO.setUserId(category.getUserId());
        categoryDTO.setAdminId(category.getAdminId());
       /* categoryDTO.setGroupId(category.getGroupId());
        categoryDTO.setCompanyId(category.getCompanyId());
        categoryDTO.setCompanyName(getCompany(category.getCompanyId(), 1));
        categoryDTO.setGroupName(getGroup(category.getGroupId(), 1));*/
        categoryDTO.setCategoryName(category.getCategoryName());
        if (category.getImagePath() != null)
            categoryDTO.setImagePath(baseUrl.getBASE_URL() + category.getImagePath());
        categoryDTO.setCreatedAt(category.getCreatedAt());
        categoryDTO.setCreatedBy(category.getCreatedBy());
        categoryDTO.setUpdatedAt(category.getUpdatedAt());
        categoryDTO.setUpdatedBy(category.getUpdatedBy());
        return categoryDTO;
    }

    /*public String getGroup(String id, int i) {
        String groupName = null;
        Group group = null;
        group = groupRepository.findByIdAndStatus(id, i);
        if (group != null)
            groupName = group.getGroupName();
        return groupName;
    }

    public String getCompany(String companyId, int i) {
        String companyName = null;
        Company company = null;
        company = companyRepository.findByIdAndStatus(companyId, i);
        if (company != null)
            companyName = company.getCompanyName();
        return companyName;
    }*/

   /* public List<CategoryDTO> fetchCategories(String userId, String groupId) {
        //   List<Category> categoryList = repository.findByUserIdAndGroupIdAndStatus(userId, groupId, 1);
        List<Category> categoryList = repository.findByAdminIdAndGroupIdAndStatus(userId, groupId, 1);

        List<CategoryDTO> categoryDTOList = new ArrayList<>();

        if (categoryList.size() > 0) {
            for (Category mCategory : categoryList) {
                categoryDTOList.add(convertToDTO(mCategory));
            }
        }
        return categoryDTOList;
    }*/

    public Object updateCategory(CategoryDTO categoryDTO) {
        Category category = repository.findByIdAndStatus(categoryDTO.getId(), 1);
        Category mUpdatedCategory = null;
        String imagePath = null;
        String mUpdatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (category != null) {
            category.setCategoryName(categoryDTO.getCategoryName());
            category.setUpdatedAt(mUpdatedAt);
            if (categoryDTO.getCategoryImage() != null) {
                imagePath = getUserIdAndOutletId(categoryDTO);
            }
            if (imagePath != null)
                category.setImagePath(imagePath);
            mUpdatedCategory = repository.save(category);
            if (mUpdatedCategory != null) {
                responseMessage.setMessage("Company Updated Successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in updation");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

    public Object deleteCategory(String id) {
        Category category = repository.findByIdAndStatus(id, 1);
        //  CategoryDTO categoryDTO = new CategoryDTO();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (category != null) {
            category.setStatus(0);
            if (repository.save(category) != null) {
                responseMessage.setMessage("Category deleted successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in Category deletion");
                responseMessage.setResponseStatus("error");
            }
        } else {
            responseMessage.setMessage("Error in Category deletion");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public List<CategoryDTO> getAllCategories(String userId) {

        List<Category> categoryList = repository.findByAdminIdAndStatus(userId, 1);

        List<CategoryDTO> categoryDTOList = new ArrayList<>();

        if (categoryList.size() > 0) {
            for (Category mCategory : categoryList) {
                categoryDTOList.add(convertToDTO(mCategory));
            }
        }
        return categoryDTOList;
    }

    public Object fetchAllCategories(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Category> sliceResult = categoryPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Category> list = sliceResult.toList();
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Category mCategory : list) {
                categoryDTOList.add(convertToDTO(mCategory));
            }
        }
        GenericData<CategoryDTO> data = new GenericData(categoryDTOList, sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object getTopCategories(String adminId) {
        List<Category> list = repository.findTop8ByAdminIdAndStatus(adminId, 1);
        List<CategoryDTO> categoryDTOList = new ArrayList<>();

        if (list.size() > 0) {
            for (Category mCategory : list) {
                categoryDTOList.add(convertToDTO(mCategory));
            }
        }
        return categoryDTOList;
    }
}


