package com.btoc.pharma.api.services;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;

@Service
public class AWSS3Service {

    // private static final Logger LOGGER = LoggerFactory.getLogger(AWSS3ServiceImpl.class);

    @Autowired
    private AmazonS3 amazonS3;
    @Value("${aws.s3.bucket}")
    private String bucketName;


    // @Async annotation ensures that the method is executed in a different background thread
    // but not consume the main thread.
    @Async
    public String uploadFile(final MultipartFile multipartFile, String dirName) {
        // LOGGER.info("File upload in progress.");
        String imagePath = null;
        try {
            final File file = convertMultiPartFileToFile(multipartFile);
            imagePath = uploadFileToS3Bucket(bucketName, file, dirName);
            // LOGGER.info("File upload is completed.");
            file.delete();    // To remove the file locally created in the project folder.
        } catch (final AmazonServiceException ex) {
            // LOGGER.info("File upload is failed.");
            //LOGGER.error("Error= {} while uploading file.", ex.getMessage());
        }
        return imagePath;
    }

    @Async
    public String uploadFile(final File file, String dirName) {
        // LOGGER.info("File upload in progress.");
        String imagePath = null;
        try {
            // final File file = convertMultiPartFileToFile(multipartFile);
            imagePath = uploadFileToS3Bucket(bucketName, file, dirName);
            // LOGGER.info("File upload is completed.");
            file.delete();    // To remove the file locally created in the project folder.
        } catch (final AmazonServiceException ex) {
            // LOGGER.info("File upload is failed.");
            //LOGGER.error("Error= {} while uploading file.", ex.getMessage());
        }
        return imagePath;
    }

    private File convertMultiPartFileToFile(final MultipartFile multipartFile) {
        final File file = new File(multipartFile.getOriginalFilename());
        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(multipartFile.getBytes());
        } catch (final IOException ex) {
            //  LOGGER.error("Error converting the multi-part file to file= ", ex.getMessage());
        }
        return file;
    }

    private String uploadFileToS3Bucket(final String bucketName, final File file, String dirName) {
        final String uniqueFileName = LocalDateTime.now() + "_" + file.getName();
        // LOGGER.info("Uploading file with name= " + uniqueFileName);
        final PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, dirName + "/" + uniqueFileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead);
        amazonS3.putObject(putObjectRequest);

        String imagePath = getFile(uniqueFileName, dirName);
        return imagePath;
    }

    public String getFile(String key, String dirName) {

        URL path = amazonS3.getUrl(bucketName, dirName + "/" + key);
        String imagePath = path.getPath();
        return imagePath;
    }

}