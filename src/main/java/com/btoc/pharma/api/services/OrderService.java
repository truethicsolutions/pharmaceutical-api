package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.*;
import com.btoc.pharma.api.dto.*;
import com.btoc.pharma.api.model.*;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Utils;
import org.joda.time.DateTimeComparator;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class OrderService {

    @Autowired
    private OrderRepository repository;

    @Autowired
    FirebaseMessagingService firebaseService;

    @Autowired
    private OrderPaginationRepository orderPaginationRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;
   /* @Autowired
    private LoyaltyRepository loyaltyRepository;*/

    @Autowired
    private AWSS3Service awss3Service;

    @Autowired
    private UserName userName;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BaseUrl baseUrl;

    /* for non prescribed product orders */
    public ResponseEntity<ResponseMessage> createOrder(OrderDTO orderDTO) {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (orderDTO != null) {
            String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
            orderDTO.setCreatedAt(createdAt);
            orderDTO.setUpdatedAt(createdAt);
            Order order = convertToOrder(orderDTO, "non-prescribed");
            if (repository.save(order) != null) {
                responseMessage.setMessage("Order placed successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Order not placed...");
                responseMessage.setResponseStatus("Error");
            }
        } else {
            responseMessage.setMessage("Order not placed...");
            responseMessage.setResponseStatus("Error");
        }
        return new ResponseEntity<>(responseMessage, HttpStatus.OK);
    }

    public Object placePrescibedOrder(ProductOrderListDTO productList) throws FirebaseMessagingException {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (productList != null) {
            Order order = converToOrderList(productList);
            if (repository.save(order) != null) {
                responseMessage.setMessage("Order placed successfully");
                responseMessage.setResponseStatus("success");
                Note note = new Note();
                note.setSubject("order generated sucessfully");
                note.setContent("confirm you're order");
                Map<String, String> data = new HashMap<>();
                data.put("option", "1");
                note.setData(data);
                //sendPushNotification(note,order.getCustomerId());
            } else {
                responseMessage.setMessage("Order not placed...");
                responseMessage.setResponseStatus("Error");
            }
        } else {
            responseMessage.setMessage("Order not placed...");
            responseMessage.setResponseStatus("Error");
        }
        return responseMessage;
    }

    private void sendPushNotification(Note note, String customerId) throws FirebaseMessagingException {
        Customer customer = customerRepository.findByIdAndStatus(customerId, 1);
        String fcmToken = customer.getFcmToken();
        String result = firebaseService.sendNotification(note, fcmToken);
        System.out.println(result);
    }

    private Order converToOrderList(ProductOrderListDTO productList) {
        Order order = repository.findByIdAndStatus(productList.getOrderId(), 1);
        double orderDiscount = 0, totalPrice = 0;
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        order.setUpdatedAt(createdAt);
        order.setTotalPrice(productList.getFinalAmount());
        List<OrderProductDetails> ordersList = new ArrayList<>();
        List<ProductOrderDetailsDTO> list = productList.getList();
        for (ProductOrderDetailsDTO mProductList : list) {
            Product product = productRepository.findByIdAndStatus(mProductList.getProductId(), 1);
            OrderProductDetails orderProductDetails = new OrderProductDetails();
            orderProductDetails.setStatus(1);
            orderProductDetails.setProductId(mProductList.getProductId());
            orderProductDetails.setQuantity(mProductList.getQuantity());
            orderProductDetails.setTotalAmount(mProductList.getTotalAmount());
            orderProductDetails.setDiscountAmount(mProductList.getDiscountAmount());
            orderProductDetails.setDiscount(mProductList.getDiscount());
            orderDiscount = orderDiscount + mProductList.getDiscountAmount();
            //totalPrice = totalPrice + mProductList.getTotalAmount();
            if (product != null) {
                orderProductDetails.setName(product.getProductName());
                orderProductDetails.setMrp(product.getRatePerPiece());
                orderProductDetails.setPckQuantity(product.getQuantity());
                orderProductDetails.setDiscountType(product.getDiscountType());
                orderProductDetails.setPacking(product.getPacking().toLowerCase());
            }
            ordersList.add(orderProductDetails);
        }
        order.setProductDetailsList(ordersList);
        order.setOrderDiscount(orderDiscount);
        order.setOrderStatus("generated");
        return order;
    }

    /*public List<OrderDTO> getAllOrder(long branchId) {
        List<Order> orderList = repository.findByBranchId(branchId);
        List<OrderDTO> orderDTOList = new ArrayList<>();
        for (Order mOrder : orderList) {
            orderDTOList.add(convertToDTO(mOrder));
        }
        return orderDTOList;
    }

    public OrderDTO getOrder(String id, long branchId) {
        Order order = repository.findByIdAndBranchId(id, branchId);
        return convertToDTO(order);
    }*/

    private OrderDTO convertToDTO(Order order) {
        OrderDTO orderDTO = new OrderDTO();
        /*orderDTO.setBranchId(order.getBranchId());
        orderDTO.setCustomerId(order.getCustomerId());
        orderDTO.setOrderNo(order.getOrderNo());
        orderDTO.setOrderDate(order.getOrderDate());
        orderDTO.setProductId(order.getProductId());
        orderDTO.setCustomerName(order.getCustomerName());
        orderDTO.setMobileNumber(order.getMobileNumber());
        orderDTO.setOrderStatus(order.getOrderStatus());
        orderDTO.setOrderquantity(order.getOrderquantity());
        orderDTO.setTotalPrice(order.getTotalPrice());
        orderDTO.setPaymentMode(order.getPaymentMode());
        orderDTO.setPaymentStatus(order.getPaymentStatus());
        orderDTO.setPaymentDate(order.getPaymentDate());
        orderDTO.setDeliveryAddress(order.getDeliveryAddress());
        orderDTO.setDeliveryBoyName(order.getDeliveryBoyName());
        orderDTO.setDeliveryStatus(order.getDeliveryStatus());
        orderDTO.setCancellationNote(order.getCancellationNote());
        orderDTO.setTransactionNumber(order.getTransactionNumber());
        orderDTO.setOrderType(order.getOrderType());
        orderDTO.setPrescribedImage(order.getPrescribedImage());
        orderDTO.setCreatedAt(order.getCreatedAt());
        orderDTO.setCreatedBy(order.getCreatedBy());
        orderDTO.setUpdatedAt(order.getUpdatedAt());
        orderDTO.setUpdatedBy(order.getUpdatedBy());
        orderDTO.setProductDetailsList(order.getProductDetailsList());*/
        return orderDTO;
    }

    public Object uploadPrescription(OrderDTO orderDTO) {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        Order order = convertToOrder(orderDTO, "prescribed"); //covert Request object into Order
        Order mNewOrder = repository.save(order);
        if (mNewOrder != null) {
            responseMessage.setMessage("Order submitted successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in order creation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    private Order convertToOrder(OrderDTO orderDTO, String key) {
        Order order = new Order();
        Customer customer = null;
        String imagePath = null;
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        LocalDateTime myDateObj = LocalDateTime.now();
        long millis = myDateObj
                .atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
        order.setAdminId(orderDTO.getAdminId());
        order.setOutletId(orderDTO.getOutletId());
        order.setCustomerId(orderDTO.getCustomerId());
        customer = getCustomer(orderDTO.getCustomerId());
        if (customer != null) {
            order.setCustomerName(customer.getfName() + " " + customer.getlName());
            order.setMobileNumber(customer.getMobileNumber());
        }
        order.setStatus(1);
        order.setOrderFrom(1);
        order.setCreatedAt(createdAt);
        order.setUpdatedAt(createdAt);
       /* order.setCreatedBy(userName.getUserName(orderDTO.getAdminId()));
        order.setUpdatedBy(userName.getUserName(orderDTO.getAdminId()));*/
        if (orderDTO.getUserId() != null) {
            order.setUserId(orderDTO.getUserId());
            order.setCreatedBy(userName.getUserName(orderDTO.getUserId()));
            order.setUpdatedBy(userName.getUserName(orderDTO.getUserId()));
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime orderDate = LocalDateTime.parse(createdAt, formatter);
        order.setOrderDate(orderDate);
        order.setOrderNo("OR" + millis);
        order.setPrescriptionCode(orderDTO.getCustomerId() + "-" + customer.getMobileNumber());
        order.setDeliveryStatus("not assigned");
        if (key.equalsIgnoreCase("prescribed")) {
            if (orderDTO.getOrderImage() != null) {
                imagePath = getUserIdAndOutletId(orderDTO);
            }
            if (imagePath != null)
                order.setOrderImagePath(imagePath);
            order.setOrderType("prescribed");
            order.setOrderStatus("pending");
            order.setPaymentStatus("pending");
        } else {
            order.setOrderType("non-prescribed");
            order.setOrderStatus("placed");
            order.setPaymentMode(orderDTO.getPaymentMode());
            order.setPaymentStatus(orderDTO.getPaymentStatus());
            order.setTransactionNumber(orderDTO.getTransactionNumber());
            order.setDeliveryAddress(orderDTO.getDeliveryAddress());
            order.setTotalPrice(orderDTO.getTotalPrice());
            order.setOrderDiscount(orderDTO.getOrderDiscount());
            List<OrderProductDetails> mNewList = new ArrayList<>();
            List<OrderProductDetails> mDbList = orderDTO.getProductDetailsList();
            for (OrderProductDetails mList : mDbList) {
                OrderProductDetails orderList = new OrderProductDetails();
                orderList.setProductId(mList.getProductId());
                orderList.setName(mList.getName());
                orderList.setDiscountAmount(mList.getDiscountAmount());
                orderList.setMrp(mList.getMrp());
                orderList.setTotalAmount(mList.getTotalAmount());
                orderList.setQuantity(mList.getQuantity());
                orderList.setDiscount(mList.getDiscount());
                orderList.setPckQuantity(mList.getPckQuantity());
                orderList.setDiscountType(mList.getDiscountType());
                orderList.setPacking(mList.getPacking());
                orderList.setStatus(1);
                mNewList.add(orderList);

            }
            order.setProductDetailsList(mNewList);
        }
        return order;
    }

    public String getCustomerName(String id) {
        String customerName = null;
        Customer customer = customerRepository.findByIdAndStatus(id, 1);
        if (customer != null) {
            customerName = customer.getfName() + customer.getlName();
        }
        return customerName;
    }

    public Customer getCustomer(String id) {
        Customer customer = customerRepository.findByIdAndStatus(id, 1);
        return customer;
    }

    public String getUserIdAndOutletId(OrderDTO orderDTO) {
        String dir = null;
        String imagePath = null;
        dir = orderDTO.getAdminId() + "_" + orderDTO.getOutletId();
        imagePath = awss3Service.uploadFile(orderDTO.getOrderImage(), dir);
        return imagePath;
    }

    /*public Object getOutletProducts(String outletId) {
        List<Product> list = productRepository.findByOutletIdAndStatus(outletId, 1);
        List<ProductsOrderDTO> productsOrderDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Product mProduct : list) {
                productsOrderDTOList.add(convertToProductDTO(mProduct));
            }
            return productsOrderDTOList;
        } else
            return productsOrderDTOList;
    }*/

    private ProductsOrderDTO convertToProductDTO(Product mProduct) {
        ProductsOrderDTO productsOrderDTO = new ProductsOrderDTO();
        productsOrderDTO.setProductId(mProduct.getId());
        productsOrderDTO.setDiscountAmount(mProduct.getDiscountAmount());
        productsOrderDTO.setProductName(mProduct.getProductName());
        productsOrderDTO.setRatePerPiece(mProduct.getRatePerPiece());
        productsOrderDTO.setDiscountType(mProduct.getDiscountType());
        productsOrderDTO.setDiscount(mProduct.getDiscount());
        productsOrderDTO.setQuantity(mProduct.getQuantity());
        productsOrderDTO.setPacking(mProduct.getPacking());
        return productsOrderDTO;
    }

    /* All Orders of Institutes from Back office */
    public Object getAllOrder(String adminId) {
        List<Order> list = repository.findByAdminIdAndStatus(adminId, 1);
        List<AllOrderDetailsDTO> allOrderDetailsDTOList = new ArrayList();
        if (list != null) {
            for (Order mOrder : list) {
                allOrderDetailsDTOList.add(convertAllOrderDetailsDTO(mOrder));
            }
        }
        return allOrderDetailsDTOList;
    }

    private AllOrderDetailsDTO convertAllOrderDetailsDTO(Order mOrder) {
        AllOrderDetailsDTO detailsDTO = new AllOrderDetailsDTO();
        Customer customer = customerRepository.findByIdAndStatus(mOrder.getCustomerId(), 1);
        Users users = usersRepository.findByIdAndStatus(mOrder.getDeliveryBoyId(), 1);
        detailsDTO.setOrderId(mOrder.getId());
        detailsDTO.setOrderType(mOrder.getOrderType());
        detailsDTO.setOrderDate(mOrder.getOrderDate());
        detailsDTO.setOrderImage(baseUrl.getBASE_URL() + mOrder.getOrderImagePath());
        detailsDTO.setOrderStatus(mOrder.getOrderStatus());
        detailsDTO.setPaymentMode(mOrder.getPaymentMode());
        detailsDTO.setPaymentStatus(mOrder.getPaymentStatus());
        detailsDTO.setDeliveryStatus(mOrder.getDeliveryStatus());
        detailsDTO.setOrderAmount(mOrder.getTotalPrice());
        if (users != null) {
            detailsDTO.setDeliveryBoyName(users.getFirstName() + " " + users.getLastName());
        }
        if (customer != null) {
            detailsDTO.setCustomerName(customer.getfName() + " " + customer.getlName());
            detailsDTO.setMobileNumber(customer.getMobileNumber());
        }
        detailsDTO.setOrderNo(mOrder.getOrderNo());
        detailsDTO.setPrescriptionCode(mOrder.getPrescriptionCode());
        return detailsDTO;
    }

    public Object findProduct(String productId) {
        Product product = productRepository.findByIdAndStatus(productId, 1);
        ProductsOrderDTO productsOrderDTO = new ProductsOrderDTO();
        if (productsOrderDTO != null) {
            productsOrderDTO = convertToProductDTO(product);
        }
        return productsOrderDTO;
    }

    public Object getMyOrders(String adminId, String outletId, String customerId) {
       /* List<Order> list = repository.findByAdminIdAndOutletIdAndCustomerIdAndStatusOrderByOrderDateDesc(adminId,
                outletId, customerId, 1);*/
        List<Order> list = repository.findByAdminIdAndCustomerIdAndStatusOrderByOrderDateDesc(adminId,
                customerId, 1);
   /* List<Order> list = repository.findByAdminIdAndOutletIdAndCustomerIdAndStatusOrderByOrderDateDesc(
                                                                    adminId, outletId, customerId, 1);*/
        List<MyOrdersDTO> myOrdersDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Order mOrderList : list) {
                MyOrdersDTO myOrders = new MyOrdersDTO();
                myOrders.setOrderDate(mOrderList.getOrderDate());
                myOrders.setOrderStatus(mOrderList.getOrderStatus());
                myOrders.setOrderType(mOrderList.getOrderType());
                myOrders.setOrderAmount(mOrderList.getTotalPrice());
                myOrders.setOrderId(mOrderList.getId());
                myOrders.setOrderNo(mOrderList.getOrderNo());
                myOrders.setOrderDiscount(mOrderList.getOrderDiscount());
                myOrders.setDeliveryAddress(getAddress(mOrderList.getCustomerId()));
                List<OrderProductDetails> mUpdatelist = mOrderList.getProductDetailsList();
                List<OrderProductDetails> mNewList = new ArrayList<>();
                for (OrderProductDetails mList : mUpdatelist) {
                    if (mList.getStatus() != 0) {
                        OrderProductDetails orderList = new OrderProductDetails();
                        orderList.setProductId(mList.getProductId());
                        orderList.setName(mList.getName());
                        orderList.setDiscountAmount(mList.getDiscountAmount());
                        orderList.setMrp(mList.getMrp());
                        orderList.setTotalAmount(mList.getTotalAmount());
                        orderList.setQuantity(mList.getQuantity());
                        orderList.setDiscount(mList.getDiscount());
                        orderList.setPckQuantity(mList.getPckQuantity());
                        orderList.setDiscountType(mList.getDiscountType());
                        orderList.setStatus(mList.getStatus());
                        mNewList.add(orderList);
                        myOrders.setProductList(mNewList);
                    }
                }
                myOrdersDTOList.add(myOrders);
            }
        }
        return myOrdersDTOList;
    }

    public List<DeliveryAddress> getAddress(String id) {
        Customer customer = customerRepository.findByIdAndStatus(id, 1);
        List<DeliveryAddress> addressList = new ArrayList<>();
        if (customer != null) {
            addressList = customer.getDeliveryAddress();
        }
        return addressList;
    }

    public Object updateOrder(UpdateOrderDTO updateOrderDTO) {
        Order order = repository.findByIdAndStatus(updateOrderDTO.getOrderId(), 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        List<OrderProductDetails> newProductList = new ArrayList<>();
        List<OrderProductDetails> mDblist = updateOrderDTO.getProductList();
        double orderAmount = 0;
        double orderDiscount = 0;
        if (order != null) {
            for (OrderProductDetails mNewList : mDblist) {
                OrderProductDetails orderDetails = new OrderProductDetails();
                orderDetails.setProductId(mNewList.getProductId());
                orderDetails.setName(mNewList.getName());
                orderDetails.setQuantity(mNewList.getQuantity());
                orderDetails.setMrp(mNewList.getMrp());
                orderDetails.setTotalAmount(mNewList.getTotalAmount());
                orderDetails.setDiscount(mNewList.getDiscount());
                orderDetails.setDiscountAmount(mNewList.getDiscountAmount());
                orderDetails.setPckQuantity(mNewList.getPckQuantity());
                orderDetails.setDiscountType(mNewList.getDiscountType());
                orderDetails.setStatus(mNewList.getStatus());
               /* if(mNewList.getStatus()!=0) {
                    orderAmount = orderAmount + mNewList.getTotalAmount();
                    orderDiscount = orderDiscount + orderDetails.getDiscountAmount();
                }*/
                newProductList.add(orderDetails);
            }
            order.setProductDetailsList(newProductList);
            order.setTotalPrice(updateOrderDTO.getOrderAmount());
            order.setOrderDiscount(updateOrderDTO.getOrderDiscount());
            order.setDeliveryAddress(updateOrderDTO.getDelivery_Address());
            order.setPaymentStatus(updateOrderDTO.getPaymentStatus());
            order.setPaymentMode(updateOrderDTO.getPaymentMode());
            order.setTransactionNumber(updateOrderDTO.getTransactionNumber());
            order.setPaymentDate(updateOrderDTO.getPaymentDate());
            order.setOrderStatus("placed");
            Order mOrder = repository.save(order);
            if (mOrder != null) {
                responseMessage.setMessage("Order placed successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Error in order placing");
                responseMessage.setResponseStatus("error");
            }
        } else {
            responseMessage.setMessage("Error in order placing");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object getPOrders(String adminId, String prescribed, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Order> sliceResult = orderPaginationRepository.findByAdminIdAndOrderTypeAndStatusOrderByOrderDateDesc(
                adminId, prescribed, 1, pageable);
       /* Page<Order> sliceResult = orderPaginationRepository.findByAdminIdAndOrderTypeAndStatus(adminId,
                prescribed, 1, pageable);*/
        List<Order> list = sliceResult.toList();
        List<AllOrderDetailsDTO> allOrderDetailsDTOList = new ArrayList();
        if (list.size() > 0) {
            for (Order mOrder : list) {
                allOrderDetailsDTOList.add(convertAllOrderDetailsDTO(mOrder));
            }
        }
        GenericData<AllOrderDetailsDTO> data = new GenericData(allOrderDetailsDTOList,
                sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }

    public Object getGeneratedOrders(String id) {
        Order order = repository.findByIdAndStatus(id, 1);
        List<OrderProductDetails> productList = order.getProductDetailsList();
        List<OrderProductDetails> mProductList = new ArrayList();
        GeneratedOrdersDTO generatedOrdersDTO = new GeneratedOrdersDTO();
        if (order.getOrderStatus().equalsIgnoreCase("placed")) {
            generatedOrdersDTO.setMessage("Order is already placed");
            generatedOrdersDTO.setResponseStatus("already_done");
        }
        for (OrderProductDetails mList : productList) {
            OrderProductDetails orderList = new OrderProductDetails();
            orderList.setProductId(mList.getProductId());
            orderList.setName(mList.getName());
            orderList.setQuantity(mList.getQuantity());
            orderList.setPckQuantity(mList.getPckQuantity());
            orderList.setMrp(mList.getMrp());
            orderList.setTotalAmount(mList.getTotalAmount());
            orderList.setDiscountAmount(mList.getDiscountAmount());
            orderList.setDiscountType(mList.getDiscountType());
            orderList.setDiscount(mList.getDiscount());
            orderList.setPacking(mList.getPacking());
            orderList.setStatus(mList.getStatus());
            mProductList.add(orderList);
        }
        generatedOrdersDTO.setOrderImagePath(baseUrl.getBASE_URL() + order.getOrderImagePath());
        generatedOrdersDTO.setAdminId(order.getAdminId());
        generatedOrdersDTO.setList(mProductList);
        generatedOrdersDTO.setResponseStatus("200");

        return generatedOrdersDTO;
    }

    public Object getDeliveryBoy(String adminId) {
        List<Users> users = new ArrayList<>();
        List<Roles> roles = new ArrayList();
        List<DeliveryBoyDTO> deliveryBoyDTO = new ArrayList<>();
        try {
            roles = roleRepository.findByAdminIdAndStatus(adminId, 1);
            String roleId = null;
            if (roles.size() > 0) {
                for (Roles mRole : roles) {
                    if (mRole.getRoleName().equalsIgnoreCase("delivery boy")) {
                        roleId = mRole.getId();
                        break;
                    }
                }
                users = usersRepository.findByRoleIdAndUserTypeAndStatus(roleId, 0, 1);
                if (users.size() > 0) {
                    for (Users mUser : users) {
                        String name = mUser.getFirstName() + " " + mUser.getLastName();
                        String id = mUser.getId();
                        DeliveryBoyDTO mDeliveryBoy = new DeliveryBoyDTO();
                        mDeliveryBoy.setId(id);
                        mDeliveryBoy.setName(name);
                        deliveryBoyDTO.add(mDeliveryBoy);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveryBoyDTO;
    }

    public Object assignDeliveryBoy(String id, String userId, String deliveryBoyId) throws FirebaseMessagingException {

        Order order = repository.findByIdAndStatus(id, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (order != null) {
            order.setUserId(userId);
            order.setDeliveryBoyId(deliveryBoyId);
            order.setDeliveryStatus("assigned");
            order.setOrderStatus("confirmed");
            Order mOrder = repository.save(order);
            if (mOrder != null) {
                responseMessage.setMessage("Delivery boy assigned");
                responseMessage.setResponseStatus("success");
                /* For customer notification */
                Note note = new Note();
                note.setSubject("order is ready to delivery");
                note.setContent("check you're order status");
                Map<String, String> data = new HashMap<>();
                data.put("option", "2");
                note.setData(data);
                //sendPushNotification(note, order.getCustomerId());
                /* For Delivery Boys notification */
                Note note1 = new Note();
                note.setSubject("order is ready to delivery");
                note.setContent("pick you're order");
                Map<String, String> data1 = new HashMap<>();
                data.put("option", "4");
                note.setData(data1);
                //    sendPushNotificationDeliveryBoy(note, order.getDeliveryBoyId());

            } else {
                responseMessage.setMessage("Error in assigining delivery boy");
                responseMessage.setResponseStatus("error");
            }
        } else {
            responseMessage.setMessage("Error in assigining delivery boy");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    private void sendPushNotificationDeliveryBoy(Note note, String deliveryBoyId) throws FirebaseMessagingException {
        Users users = usersRepository.findByIdAndStatus(deliveryBoyId, 1);
        String fcmToken = users.getFcmToken();
        String result = firebaseService.sendNotification(note, fcmToken);
        System.out.println(result);

    }

    public Object getDeliveryOrders(String adminId, String deliveryBoyId) {
        List<Order> list = repository.findByAdminIdAndDeliveryBoyIdAndDeliveryStatusIgnoreCaseAndOrderStatusNotAndStatusOrderByOrderDateDesc(adminId,
                deliveryBoyId, "assigned", "delivered", 1);
        List<DeliveryOrdersDTO> ordersDTOList = new ArrayList<>();
        if (list != null) {
            for (Order mOrder : list) {
                DeliveryOrdersDTO deliveryOrdersDTO = convertToDeliveryOrdersDTO(mOrder);
                ordersDTOList.add(deliveryOrdersDTO);
            }
        }
        return ordersDTOList;
    }

    /* Order Delivery of Customer from mobile app */
    public Object updateOrderForDelivery(String orderId) throws FirebaseMessagingException {
        Order order = repository.findByIdAndStatus(orderId, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        order.setOrderStatus("delivered");
        order.setPaymentStatus("paid");
        String deliveryDate = DateAndTimeUtil.getDateAndTimeUtil();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime deliveredDate = LocalDateTime.parse(deliveryDate, formatter);
        order.setDeliveryDate(deliveredDate);
        Order mOrder = repository.save(order);
        if (mOrder != null) {
            responseMessage.setMessage("Order delivered successfully");
            responseMessage.setResponseStatus("success");
            Note note = new Note();
            note.setSubject("order delivered sucessfully");
            note.setContent("confirm you're order");
            Map<String, String> data = new HashMap<>();
            data.put("option", "3");
            note.setData(data);
            //   sendPushNotification(note,order.getCustomerId());
        } else {
            responseMessage.setMessage("Error in order delivery");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object getDeliveredOrders(String adminId, String deliveryBoyId) {
        List<Order> list = repository.findByAdminIdAndDeliveryBoyIdAndOrderStatusIgnoreCaseAndStatusOrderByOrderDateDesc(adminId,
                deliveryBoyId, "delivered", 1);
        List<DeliveryOrdersDTO> ordersDTOList = new ArrayList<>();
        if (list != null) {
            for (Order mOrder : list) {
                DeliveryOrdersDTO deliveryOrdersDTO = convertToDeliveryOrdersDTO(mOrder);
                ordersDTOList.add(deliveryOrdersDTO);
            }
        }
        return ordersDTOList;
    }

    public DeliveryOrdersDTO convertToDeliveryOrdersDTO(Order mOrder) {
        DeliveryOrdersDTO deliveryOrdersDTO = new DeliveryOrdersDTO();
        deliveryOrdersDTO.setOrderId(mOrder.getId());
        deliveryOrdersDTO.setAdminId(mOrder.getAdminId());
        deliveryOrdersDTO.setOrderNo(mOrder.getOrderNo());
        deliveryOrdersDTO.setCustomerName(getCustomerName(mOrder.getCustomerId()));
        deliveryOrdersDTO.setMobileNumber(mOrder.getMobileNumber());
        deliveryOrdersDTO.setOrderStatus(mOrder.getOrderStatus());
        deliveryOrdersDTO.setOrderDate(mOrder.getOrderDate());
        deliveryOrdersDTO.setTotalPrice(mOrder.getTotalPrice());
        deliveryOrdersDTO.setOrderDiscount(mOrder.getOrderDiscount());
        deliveryOrdersDTO.setOrderType(mOrder.getOrderType());
        deliveryOrdersDTO.setPaymentMode(mOrder.getPaymentMode());
        deliveryOrdersDTO.setPaymentStatus(mOrder.getPaymentStatus());
        deliveryOrdersDTO.setPaymentDate(mOrder.getPaymentDate());
        deliveryOrdersDTO.setDeliveryAddress(mOrder.getDeliveryAddress());
        deliveryOrdersDTO.setLatitude(mOrder.getLatitude());
        deliveryOrdersDTO.setLongitude(mOrder.getLongitude());
        deliveryOrdersDTO.setDeliveryStatus(mOrder.getDeliveryStatus());
        deliveryOrdersDTO.setTotalItems(mOrder.getProductDetailsList().size());
        return deliveryOrdersDTO;
    }

    public Object getAllDeliveryOrders(String adminId, String deliveryBoyId) {
        List<Order> list = repository.findByAdminIdAndDeliveryBoyIdAndStatusOrderByOrderDateDesc(adminId,
                deliveryBoyId, 1);
        List<DeliveryOrdersDTO> ordersDTOList = new ArrayList<>();
        if (list != null) {
            for (Order mOrder : list) {
                DeliveryOrdersDTO deliveryOrdersDTO = convertToDeliveryOrdersDTO(mOrder);
                ordersDTOList.add(deliveryOrdersDTO);
            }
        }
        return ordersDTOList;

    }

    public Object orderResponsive(OrderResponsiveDTO orderDTO) {
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        Order order = convertforOrderResponsiveUpload(orderDTO); //covert Request object into Order
        Order mNewOrder = repository.save(order);
        if (mNewOrder != null) {
            responseMessage.setMessage("Order submitted successfully");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Error in order creation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    private Order convertforOrderResponsiveUpload(OrderResponsiveDTO orderDTO) {
        Order order = new Order();
        Customer customer = null;
        String imagePath = null;
        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        LocalDateTime myDateObj = LocalDateTime.now();
        long millis = myDateObj
                .atZone(ZoneId.systemDefault())
                .toInstant().toEpochMilli();
        order.setAdminId(orderDTO.getAdminId());
        order.setStatus(1);
        order.setCreatedAt(createdAt);
        order.setUpdatedAt(createdAt);
        order.setOrderType("prescribed");
        order.setOrderFrom(0);// set 0 if order is uploaded from web page
        if (orderDTO.getUserId() != null) {
            order.setUserId(orderDTO.getUserId());
        }
        if (orderDTO.getOrderImage() != null) {
            imagePath = uploadFromWebPage(orderDTO);

        }
        if (imagePath != null)
            order.setOrderImagePath(imagePath);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime orderDate = LocalDateTime.parse(createdAt, formatter);
        order.setOrderDate(orderDate);
        order.setOrderNo("OR" + millis);
        //  order.setPrescriptionCode(orderDTO.getCustomerId() + "-" + customer.getMobileNumber());
        order.setOrderStatus("pending");
        order.setDeliveryStatus("not assigned");
        order.setPaymentStatus("pending");
        order.setMobileNumber(orderDTO.getMobileNumber());
        order.setDeliveryAddress(orderDTO.getDeliveryAddress());
        customer = customerRepository.findByMobileNumberAndStatus(orderDTO.getMobileNumber(), 1);
        if (customer == null) {
            Customer newCustomer = new Customer();
            newCustomer.setfName(orderDTO.getFirstName());
            newCustomer.setlName(orderDTO.getLastName());
            newCustomer.setMobileNumber(orderDTO.getMobileNumber());
            newCustomer.setEmailId(orderDTO.getEmail());
            newCustomer.setAppUser(0);//set 0 if customer is registered from web app
            DeliveryAddress deliveryAddress = new DeliveryAddress();
            deliveryAddress.setAddress(orderDTO.getDeliveryAddress());
            deliveryAddress.setCity(orderDTO.getCity());
            deliveryAddress.setPincode(orderDTO.getPincode());
            List<DeliveryAddress> list = new ArrayList<>();
            list.add(deliveryAddress);
            newCustomer.setDeliveryAddress(list);
            newCustomer.setStatus(1);
            newCustomer.setAdminId(orderDTO.getAdminId());
            Customer mCustomer = customerRepository.save(newCustomer);
            if (mCustomer != null) {
                order.setCustomerId(mCustomer.getId());
                order.setPrescriptionCode(mCustomer.getId() + "-" + mCustomer.getMobileNumber());
            }
        } else {
            order.setCustomerId(customer.getId());
            order.setPrescriptionCode(customer.getId() + "-" + customer.getMobileNumber());
        }
        return order;
    }

    public String uploadFromWebPage(OrderResponsiveDTO orderDTO) {
        String dir = null;
        String imagePath = null;
        dir = orderDTO.getAdminId() + "_" + "webpage";
        imagePath = awss3Service.uploadFile(orderDTO.getOrderImage(), dir);
        return imagePath;
    }

    public Object generateInvoice(String orderId) {
        Order order = repository.findByIdAndOrderStatusAndStatus(orderId, "confirmed", 1);
        List<OrderProductDetails> list = order.getProductDetailsList();
        List<InvoiceProducts> orderProducts = new ArrayList<>();
        InvoiceBillDTO invoiceBillDTO = new InvoiceBillDTO();
        Optional<Product> product = null;

        for (OrderProductDetails mList : list) {
            double taxable_value;
            InvoiceProducts invoice = new InvoiceProducts();
            if (mList.getStatus() != 0) {
                product = productRepository.findById(mList.getProductId());
                Product p = product.get();
                invoice.setProductName(mList.getName());
               /* if(p.getiTax()!=0) {
                     taxable_value = (mList.getMrp() * p.getiTax()) / 100;
                }
                else{
                    taxable_value = mList.getMrp();
                }*/
                invoice.setMrp(mList.getMrp());
                invoice.setQuantity(mList.getQuantity());
                invoice.setPacking(p.getPacking());
                invoice.setsTax(p.getsTax());
                invoice.setcTax(p.getcTax());
                invoice.setiTax(p.getiTax());
                invoice.setDiscountAmount(mList.getDiscountAmount());
                invoice.setAmount(mList.getTotalAmount() + mList.getDiscountAmount());
                orderProducts.add(invoice);
            }
        }
        invoiceBillDTO.setOrderNo(order.getOrderNo());
        invoiceBillDTO.setOrderDate(order.getOrderDate());
        invoiceBillDTO.setTotalPrice(order.getTotalPrice());
        invoiceBillDTO.setTotalDiscount(order.getOrderDiscount());
        Customer customer = getCustomer(order.getCustomerId());
        if (customer != null) {
            invoiceBillDTO.setCustomerName(customer.getfName() + " " + customer.getlName());
        }
        invoiceBillDTO.setMobileNumber(order.getMobileNumber());
        invoiceBillDTO.setDeliveryAddress(order.getDeliveryAddress());
        invoiceBillDTO.setProducts(orderProducts);
        return invoiceBillDTO;
    }

    public Object dashboardOrderReports(String adminId) {
        List<Order> list = repository.findByAdminIdAndStatus(adminId, 1);
        int pending_orders = 0;
        int generated_orders = 0;
        int placed_orders = 0;
        int confirmed_orders = 0;
        int completetd_orders = 0;
        int cancelled_Orders = 0;
        int total_orders = 0;
        double total_revenue = 0;
        double revenue_by_cod = 0;
        double revenue_by_online = 0;

        /* Todays List */
        int todays_pending_orders = 0;
        int todays_generated_orders = 0;
        int todays_placed_orders = 0;
        int todays_confirmed_orders = 0;
        int todays_completetd_orders = 0;
        int todays_cancelled_Orders = 0;
        int todays_total_orders = 0;
        double todays_total_revenue = 0;
        double todays_revenue_by_cod = 0;
        double todays_revenue_by_online = 0;

        /* Monthly Lisy **/
        int monthly_pending_orders = 0;
        int monthly_generated_orders = 0;
        int monthly_placed_orders = 0;
        int monthly_confirmed_orders = 0;
        int monthly_completetd_orders = 0;
        int monthly_cancelled_Orders = 0;
        int monthly_total_orders = 0;
        double monthly_total_revenue = 0;
        double monthly_revenue_by_cod = 0;
        double monthly_revenue_by_online = 0;

        for (Order mList : list) {
            total_orders = total_orders + 1;
            if (mList.getOrderStatus().equalsIgnoreCase("pending")) {
                pending_orders = pending_orders + 1;
            }
            if (mList.getOrderStatus().equalsIgnoreCase("confirmed")) {
                confirmed_orders = confirmed_orders + 1;
            }
            if (mList.getOrderStatus().equalsIgnoreCase("delivered")) {
                completetd_orders = completetd_orders + 1;
                total_revenue = total_revenue + mList.getTotalPrice();
                if (mList.getPaymentMode().equalsIgnoreCase("cod")) {
                    revenue_by_cod = revenue_by_cod + mList.getTotalPrice();
                }
                if (mList.getPaymentMode().equalsIgnoreCase("online")) {
                    revenue_by_online = revenue_by_online + mList.getTotalPrice();
                }
            }
            if (mList.getOrderStatus().equalsIgnoreCase("generated")) {
                generated_orders = generated_orders + 1;
            }
            if (mList.getOrderStatus().equalsIgnoreCase("placed")) {
                placed_orders = placed_orders + 1;
            }
            if (mList.getOrderStatus().equalsIgnoreCase("cancelled")) {
                cancelled_Orders = cancelled_Orders + 1;
            }

       /*     Date date = new Date(mList.getOrderDate());
            MonthlyOrders monthlyOrders = new MonthlyOrders();
            //monthlyOrders.setTotalOrders(date_parse(date));*/
        }
        /* Todays Order History */
        String currentDate = DateAndTimeUtil.getDateAndTimeUtil();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime todaysDate = LocalDateTime.parse(currentDate, formatter);
        Date date = Timestamp.valueOf(todaysDate);
        //List<Order> orderList = repository.findByAdminIdAndOrderDateAndStatus(adminId, todaysDate, 1);
        for (Order mList : list) {
            int result = DateTimeComparator.getDateOnlyInstance().compare(date,
                    Timestamp.valueOf(mList.getOrderDate()));
            if (result == 0) {
                todays_total_orders = todays_total_orders + 1;
                if (mList.getOrderStatus().equalsIgnoreCase("pending")) {
                    todays_pending_orders = todays_pending_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("confirmed")) {
                    todays_confirmed_orders = todays_confirmed_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("delivered")) {
                    todays_completetd_orders = todays_completetd_orders + 1;
                    todays_total_revenue = todays_total_revenue + mList.getTotalPrice();
                    if (mList.getPaymentMode().equalsIgnoreCase("cod")) {
                        todays_revenue_by_cod = todays_revenue_by_cod + mList.getTotalPrice();
                    }
                    if (mList.getPaymentMode().equalsIgnoreCase("online")) {
                        todays_revenue_by_online = todays_revenue_by_online + mList.getTotalPrice();
                    }
                }
                if (mList.getOrderStatus().equalsIgnoreCase("generated")) {
                    todays_generated_orders = todays_generated_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("placed")) {
                    todays_placed_orders = todays_placed_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("cancelled")) {
                    todays_cancelled_Orders = todays_cancelled_Orders + 1;
                }
            }
        }
        /* get Monthly Order Reports */
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int current_month = localDate.getMonthValue();
        for (Order mList : list) {
            LocalDateTime localDateTime = mList.getOrderDate().atZone(ZoneId.systemDefault()).toLocalDateTime();
            int order_month = localDateTime.getMonthValue();

            if (current_month == order_month) {
                monthly_total_orders = monthly_total_orders + 1;
                if (mList.getOrderStatus().equalsIgnoreCase("pending")) {
                    monthly_pending_orders = monthly_pending_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("confirmed")) {
                    monthly_confirmed_orders = monthly_confirmed_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("delivered")) {
                    monthly_completetd_orders = monthly_completetd_orders + 1;
                    monthly_total_revenue = monthly_total_revenue + mList.getTotalPrice();
                    if (mList.getPaymentMode().equalsIgnoreCase("cod")) {
                        monthly_revenue_by_cod = monthly_revenue_by_cod + mList.getTotalPrice();
                    }
                    if (mList.getPaymentMode().equalsIgnoreCase("online")) {
                        monthly_revenue_by_online = monthly_revenue_by_online + mList.getTotalPrice();
                    }
                }
                if (mList.getOrderStatus().equalsIgnoreCase("generated")) {
                    monthly_generated_orders = monthly_generated_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("placed")) {
                    monthly_placed_orders = monthly_placed_orders + 1;
                }
                if (mList.getOrderStatus().equalsIgnoreCase("cancelled")) {
                    monthly_cancelled_Orders = monthly_cancelled_Orders + 1;
                }
            }
        }

        /** setting Result into parameters **/
        DashboardOrdersDTO order_reports = new DashboardOrdersDTO();
        order_reports.setTotalOrders(total_orders);
        order_reports.setPendingOrders(pending_orders);
        order_reports.setConfirmedOrders(confirmed_orders);
        order_reports.setCompletedOrders(completetd_orders);
        order_reports.setPlacedOrders(placed_orders);
        order_reports.setGeneratedOrders(generated_orders);
        order_reports.setCancelledOrders(cancelled_Orders);
        order_reports.setTotal_revenue(total_revenue);
        order_reports.setRevenue_by_online(revenue_by_online);
        order_reports.setRevenue_by_cod(revenue_by_cod);
        /** completetd setting Result into parameters **/

        /** Today's Result **/
        order_reports.setTodays_totalOrders(todays_total_orders);
        order_reports.setTodays_pendingOrders(todays_pending_orders);
        order_reports.setTodays_confirmedOrders(todays_confirmed_orders);
        order_reports.setTodays_completedOrders(todays_completetd_orders);
        order_reports.setTodays_placedOrders(todays_placed_orders);
        order_reports.setTodays_generatedOrders(todays_generated_orders);
        order_reports.setTodays_cancelledOrders(todays_cancelled_Orders);
        order_reports.setTodays_total_revenue(todays_total_revenue);
        order_reports.setTodays_revenue_by_online(todays_revenue_by_online);
        order_reports.setTodays_revenue_by_cod(todays_revenue_by_cod);
        /** completetd Today's Result **/

        /** Monthly's Result **/
        MonthlyOrdersDTO monthlyOrdersDTO = new MonthlyOrdersDTO();
        monthlyOrdersDTO.setPendingOrders(monthly_pending_orders);
        monthlyOrdersDTO.setTotalOrders(monthly_total_orders);
        monthlyOrdersDTO.setGeneratedOrders(monthly_generated_orders);
        monthlyOrdersDTO.setPendingOrders(monthly_placed_orders);
        monthlyOrdersDTO.setConfirmedOrders(monthly_confirmed_orders);
        monthlyOrdersDTO.setCompletedOrders(monthly_completetd_orders);
        monthlyOrdersDTO.setCancelledOrders(monthly_cancelled_Orders);
        monthlyOrdersDTO.setTotal_revenue(monthly_total_revenue);
        monthlyOrdersDTO.setRevenue_by_cod(monthly_revenue_by_cod);
        monthlyOrdersDTO.setRevenue_by_online(monthly_revenue_by_online);
        order_reports.setMonthlyOrders(monthlyOrdersDTO);
        return order_reports;
    }

    public Object getAllOrdersBetweenDates(ReportsDTO reportsDTO) {

        /*int pagesize = Integer.parseInt(reportsDTO.getPageSize());
        int pagenumber = Integer.parseInt(reportsDTO.getPageNumber());
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize,
                Sort.by("orderDate").descending());*/
        List<Order> list = new ArrayList<>();
       /* Page<Order> sliceResult = orderPaginationRepository.findByAdminIdAndOrderStatusAndStatusAndOrderDateBetween(
                reportsDTO.getAdminId(), reportsDTO.getStatus(), 1, reportsDTO.getDateFrom(),
                reportsDTO.getDateTo(), pageable);
        list = sliceResult.toList();
       */
       /* GenericData<Order> data = new GenericData(list,
                sliceResult.getTotalElements(), pagenumber, pagesize, sliceResult.getTotalPages());*/
        list = repository.findByAdminIdAndOrderStatusAndStatusAndOrderDateBetween(
                reportsDTO.getAdminId(), reportsDTO.getStatus(), 1, reportsDTO.getDateFrom(),
                reportsDTO.getDateTo());
        return list;
    }

    /**
     * update orders from responsive web page and save into database
     * genereate RazorPay id and status
     * send response to web page
     **/
    public Object orders(ResponsiveOrderUpdateDTO responseDTO) {
        Order modelOrder = repository.findByIdAndStatus(responseDTO.getOrderId(), 1);
        String receipt = modelOrder.getOrderNo();
        String currency = "INR";
        JSONObject orderRequest = new JSONObject();
        ResponsiveOrderResponseDTO paymentResponse = new ResponsiveOrderResponseDTO();
        com.razorpay.Order order = null;
        try {
            orderRequest.put("amount", responseDTO.getPayableAmount() * 100); // amount in the smallest currency unit
            orderRequest.put("currency", currency);
            orderRequest.put("receipt", receipt);
            order = initiateClient().Orders.create(orderRequest);
            Customer customer = customerRepository.findByIdAndStatus(modelOrder.getCustomerId(), 1);
            modelOrder.setTotalPrice(responseDTO.getTotalPrice());
            modelOrder.setOrderDiscount(responseDTO.getOrderDiscount());
            modelOrder.setPaymentMode("online");
            modelOrder.setTransactionNumber(order.get("id"));
            modelOrder.setPayableAmount(responseDTO.getPayableAmount());
            List<OrderProductDetails> mDblist = responseDTO.getProductList();
            List<OrderProductDetails> newProductList = new ArrayList<>();
            for (OrderProductDetails mNewList : mDblist) {
                OrderProductDetails orderDetails = new OrderProductDetails();
                orderDetails.setProductId(mNewList.getProductId());
                orderDetails.setName(mNewList.getName());
                orderDetails.setQuantity(mNewList.getQuantity());
                orderDetails.setMrp(mNewList.getMrp());
                orderDetails.setTotalAmount(mNewList.getTotalAmount());
                orderDetails.setDiscount(mNewList.getDiscount());
                orderDetails.setDiscountAmount(mNewList.getDiscountAmount());
                orderDetails.setPckQuantity(mNewList.getPckQuantity());
                orderDetails.setDiscountType(mNewList.getDiscountType());
                orderDetails.setStatus(mNewList.getStatus());
                orderDetails.setPacking(mNewList.getPacking());
                newProductList.add(orderDetails);
            }
            modelOrder.setProductDetailsList(newProductList);
            Order newUpdatedOrder = repository.save(modelOrder);
            if (newUpdatedOrder != null) {
                paymentResponse.setOrderId(responseDTO.getOrderId());
                paymentResponse.setStatus(order.get("status"));
                paymentResponse.setKeyId("rzp_test_xkSIgLFZ7pW6WH");
                paymentResponse.setPaymentId(order.get("id"));
                paymentResponse.setCustomerName(customer.getfName() + " " + customer.getlName());
                paymentResponse.setMobileNumber(customer.getMobileNumber());
                paymentResponse.setEmail(customer.getEmailId());
                paymentResponse.setDescription("Order paying for medicine");
                paymentResponse.setPayableAmount(responseDTO.getPayableAmount());
            }
        } catch (RazorpayException | JSONException e) {
            // Handle Exception
            System.out.println(e.getMessage());
        }
        return paymentResponse;
    }

    public RazorpayClient initiateClient() {
        RazorpayClient razorpayClient = null;
        try {
            razorpayClient = new RazorpayClient("rzp_test_xkSIgLFZ7pW6WH", "ByZMxQueQNLjaUJfclYqKbzQ");
        } catch (RazorpayException e) {
        }
        return razorpayClient;
    }

    public Object checkPaymentStatus(String order_id, String razorpay_order_id,
                                     String razorpay_payment_id, String razorpay_signature) throws JSONException, RazorpayException {

        JSONObject options = new JSONObject();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        options.put("razorpay_order_id", razorpay_order_id);
        options.put("razorpay_payment_id", razorpay_payment_id);
        options.put("razorpay_signature", razorpay_signature);
        boolean status = Utils.verifyPaymentSignature(options, "ByZMxQueQNLjaUJfclYqKbzQ");
        if (status) {
            Order order = repository.findByIdAndStatus(order_id, 1);
            if (order != null) {
                order.setPaymentStatus("paid");
                String pDate = DateAndTimeUtil.getDateAndTimeUtil();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime paymentDate = LocalDateTime.parse(pDate, formatter);
                order.setPaymentDate(paymentDate);
                order.setOrderStatus("placed");
                repository.save(order);
            }
            responseMessage.setMessage("Order is placed scuccessfully And Link of invoice bill  will be sent to your registered mobile number and email address");
            responseMessage.setResponseStatus("success");
        } else {
            responseMessage.setMessage("Payment failed");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    public Object CustomerReports(ReportsDTO reportsDTO) {

     /*   int pagesize = Integer.parseInt(reportsDTO.getPageSize());
        int pagenumber = Integer.parseInt(reportsDTO.getPageNumber());
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize, Sort.by("orderDate").descending());
        Page<Order> sliceResult = orderPaginationRepository
                .findByAdminIdAndCustomerIdAndOrderStatusAndStatusAndOrderDateBetween(
                        reportsDTO.getAdminId(), reportsDTO.getCustomerId(), reportsDTO.getStatus(), 1,
                        reportsDTO.getDateFrom(), reportsDTO.getDateTo(), pageable);
*/
        //      List<Order> orderList = sliceResult.toList();
        List<Order> orderList = new ArrayList<>();
        orderList = repository.findByAdminIdAndStatusAndOrderDateBetween(
                reportsDTO.getAdminId(), 1,
                reportsDTO.getDateFrom(), reportsDTO.getDateTo());

        List<CustomerReportDTO> customerDTOList = new ArrayList<>();
        Customer mCust = null;
        try {
            for (Order mOrder : orderList) {
                CustomerReportDTO response = new CustomerReportDTO();
                mCust = customerRepository.findByIdAndStatus(mOrder.getCustomerId(), 1);
                response.setCustomerName(mCust.getfName() + " " + mCust.getlName());
                response.setMobileNumber(mCust.getMobileNumber());
                response.setOrderStatus(mOrder.getOrderStatus());
                // response.setTotalOrder();
                customerDTOList.add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

     /*   GenericData<CustomerReportDTO> data = new GenericData(customerDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
     */
        return customerDTOList;
    }

    public Object DeliveryBoyReports(ReportsDTO reportsDTO) {

      /*  int pagesize = Integer.parseInt(reportsDTO.getPageSize());
        int pagenumber = Integer.parseInt(reportsDTO.getPageNumber());
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize, Sort.by("orderDate").descending());
        Page<Order> sliceResult = orderPaginationRepository
                .findByAdminIdAndDeliveryBoyIdAndOrderStatusAndStatusAndOrderDateBetween(
                        reportsDTO.getAdminId(), reportsDTO.getDeliveryBoyId(), reportsDTO.getStatus(), 1,
                        reportsDTO.getDateFrom(), reportsDTO.getDateTo(), pageable);

        List<Order> orderList = sliceResult.toList();
      */
        List<Order> orderList = repository.findByAdminIdAndOrderStatusAndStatusAndOrderDateBetween(
                reportsDTO.getAdminId(), reportsDTO.getStatus(), 1,
                reportsDTO.getDateFrom(), reportsDTO.getDateTo());
        List<DeliveryBoyReportDTO> customerDTOList = new ArrayList<>();
        Customer mCust = null;
        try {
            for (Order mOrder : orderList) {
                mCust = customerRepository.findByIdAndStatus(mOrder.getCustomerId(), 1);
                DeliveryBoyReportDTO response = new DeliveryBoyReportDTO();
                response.setOrderNo(mOrder.getOrderNo());
                response.setOrderDate(mOrder.getOrderDate());
                response.setOrderAmount(mOrder.getTotalPrice());
                response.setDeliveryDate(mOrder.getDeliveryDate());
                response.setPaymentMode(mOrder.getPaymentMode());
                response.setPaymentStatus(mOrder.getPaymentStatus());
                response.setCustomerName(mCust.getfName() + " " + mCust.getlName());
                response.setMobileNumber(mCust.getMobileNumber());
                customerDTOList.add(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*GenericData<DeliveryBoyReportDTO> data = new GenericData(customerDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        */
        return customerDTOList;

    }

    public Object findOrder(String id) {
        Order order = null;
        order = repository.findByIdAndStatus(id, 1);
        if (order != null) {
            order.setOrderImagePath(baseUrl.getBASE_URL() + order.getOrderImagePath());
            return order;
        } else
            return order;
    }

    public Object findCustomerPrevOrder(String id) {
        Order order = null;
        order = repository.findByIdAndStatus(id, 1);
        Customer customer = customerRepository.findByIdAndStatus(order.getCustomerId(), 1);
        List<Order> orderList = repository.findTop3ByCustomerIdAndOrderStatusAndOrderDateBeforeAndStatusOrderByOrderDateDesc(
                customer.getId(), "delivered", order.getOrderDate(), 1);
        return orderList;
    }
}