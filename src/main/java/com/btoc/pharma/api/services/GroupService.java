package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.CompanyRepository;
import com.btoc.pharma.api.dao.GroupPaginationRepository;
import com.btoc.pharma.api.dao.GroupRepository;
import com.btoc.pharma.api.dto.GroupDTO;
import com.btoc.pharma.api.model.GenericData;
import com.btoc.pharma.api.model.Group;
import com.btoc.pharma.api.model.UserName;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupService {
    @Autowired
    private GroupRepository repository;

    @Autowired
    private UserName userName;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private GroupPaginationRepository groupPaginationRepository;

    public Object createGroup(GroupDTO groupDTO) {
        Group mExisting = null;
        mExisting = repository.findByGroupNameIgnoreCaseAndAdminIdAndStatus(groupDTO.getGroupName(),
                groupDTO.getAdminId(), 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (mExisting != null) {
            responseMessage.setMessage("Group name already exist");
            responseMessage.setResponseStatus("200");
        } else {
            Group group = convertTo(groupDTO);
            group.setStatus(1);
            try {
                Group dto = repository.save(group);
                if (dto != null) {
                    responseMessage.setMessage("Group created successfully");
                    responseMessage.setResponseStatus("success");


                } else {
                    responseMessage.setMessage("Error in group creation");
                    responseMessage.setResponseStatus("error");
                }

            } catch (Exception e) {
                responseMessage.setMessage("Error in group creation");
                responseMessage.setResponseStatus("error");
            }
        }
        return responseMessage;
    }

  /*  public GroupDTO updateGroup(String id) {
    }

    public GroupDTO deleteGroup(String id) {
    }

    public List<GroupDTO> getAllGroup(String outletId) {
    }

    public GroupDTO findGroup(String id) {
    }*/
  private Group convertTo(GroupDTO groupDTO) {
      Group group = new Group();
      String mDate = DateAndTimeUtil.getDateAndTimeUtil();
      group.setUserId(groupDTO.getUserId());
      group.setAdminId(groupDTO.getAdminId());
      group.setGroupName(groupDTO.getGroupName());
      //   group.setCompanyId(groupDTO.getCompanyId());
      group.setCreatedAt(mDate);
      group.setUpdatedAt(mDate);
      if (group.getUserId() != null) {
          group.setCreatedBy(userName.getUserName(groupDTO.getUserId()));
          group.setUpdatedBy(userName.getUserName(groupDTO.getUserId()));
      } else {
          group.setCreatedBy(userName.getUserName(groupDTO.getAdminId()));
          group.setUpdatedBy(userName.getUserName(groupDTO.getAdminId()));
      }
      return group;
  }

   /* public List<GroupDTO> findGroup(String userId, String companyId) {
        // List<Group> groupList = repository.findByUserIdAndCompanyIdAndStatus(userId, companyId, 1);
        List<Group> groupList = repository.findByAdminIdAndCompanyIdAndStatus(userId, companyId, 1);

        List<GroupDTO> groupDTOList = new ArrayList<>();

        if (groupList.size() > 0) {
            for (Group mGroup : groupList) {
                groupDTOList.add(convertToDTO(mGroup));
            }
        }
        return groupDTOList;
    }*/

    private GroupDTO convertToDTO(Group group) {

       /* Company company = getCompany(group.getCompanyId(), 1);
        String companyName = null;
        if (company != null) {
            companyName = company.getCompanyName();
        }*/
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setId(group.getId());
        groupDTO.setUserId(group.getUserId());
        groupDTO.setAdminId(group.getAdminId());
        // groupDTO.setCompanyId(group.getCompanyId());
        groupDTO.setGroupName(group.getGroupName());
        //groupDTO.setCompanyName(companyName);
        groupDTO.setStatus(group.getStatus());
        groupDTO.setCreatedAt(group.getCreatedAt());
        groupDTO.setUpdatedAt(group.getUpdatedAt());
        return groupDTO;
    }

    public List<GroupDTO> getAllGroup(String userId) {

        List<Group> groupList = repository.findByAdminIdAndStatus(userId, 1);
        List<GroupDTO> groupDTOList = new ArrayList<>();

        if (groupList.size() > 0) {
            for (Group mGroup : groupList) {
                groupDTOList.add(convertToDTO(mGroup));
            }
        }
        return groupDTOList;
    }

    public Object updateGroup(GroupDTO groupDTO) {

        Group group = repository.findByIdAndStatus(groupDTO.getId(), 1);
        String mDate = DateAndTimeUtil.getDateAndTimeUtil();
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        if (group != null) {
            group.setGroupName(groupDTO.getGroupName());
            group.setUpdatedAt(mDate);
            Group mgroup = repository.save(group);
            if (mgroup != null) {
                responseMessage.setMessage("Group updation successfully");
                responseMessage.setResponseStatus("success");
               /* groupDTO.setStatus(group.getStatus());
                groupDTO.setUserId(group.getUserId());
                groupDTO.setAdminId(group.getAdminId());
                groupDTO.setCompanyId(groupDTO.getCompanyId());
                groupDTO.setCompanyName(getCompany(groupDTO.getCompanyId(), 1).getCompanyName());*/
            } else {
                responseMessage.setMessage("Errro in group updation");
                responseMessage.setResponseStatus("error");
            }
        } else {
            responseMessage.setMessage("Errro in group updation");
            responseMessage.setResponseStatus("error");
        }
        return responseMessage;
    }

    /* public Company getCompany(String companyId, int i) {
         Company company = companyRepository.findByIdAndStatus(companyId, i);
         return company;
     }
 */
    public Object deleteGroup(String id) {
        Group group = repository.findByIdAndStatus(id, 1);
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        // GroupDTO groupDTO = new GroupDTO();
        if (group != null) {
            group.setStatus(0);
            if (repository.save(group) != null) {
                responseMessage.setMessage("Group deleted successfully");
                responseMessage.setResponseStatus("success");
            } else {
                responseMessage.setMessage("Group deleted successfully");
                responseMessage.setResponseStatus("success");
            }
        } else {
            responseMessage.setMessage("Error in group deletion ");
            responseMessage.setResponseStatus("error");

        }
        return responseMessage;
    }

    public Object fetchAllGroup(String adminId, String pageNumber, String pageSize) {
        int pagesize = Integer.parseInt(pageSize);
        int pagenumber = Integer.parseInt(pageNumber);
        Pageable pageable = PageRequest.of(pagenumber - 1, pagesize);
        Page<Group> sliceResult = groupPaginationRepository.findByAdminIdAndStatus(adminId,
                1, pageable);
        List<Group> list = sliceResult.toList();
        List<GroupDTO> groupDTOList = new ArrayList<>();
        if (list.size() > 0) {
            for (Group mProduct : list) {
                groupDTOList.add(convertToDTO(mProduct));
            }
        }
        GenericData<GroupDTO> data = new GenericData(groupDTOList, sliceResult.getTotalElements(),
                pagenumber, pagesize, sliceResult.getTotalPages());
        return data;
    }
}

