package com.btoc.pharma.api.services;

import com.btoc.pharma.api.appresponse.ResponseMessage;
import com.btoc.pharma.api.dao.SubCategoryRepository;
import com.btoc.pharma.api.dto.SubCategoryDTO;
import com.btoc.pharma.api.model.SubCategory;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageProperties;
import com.btoc.pharma.api.uploadingfiles.storage.FileStorageService;
import com.btoc.pharma.api.utils.DateAndTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubCategoryService {
    @Autowired
    SubCategoryRepository repository;
    @Autowired
    private FileStorageService fileStorageService;

    private FileStorageProperties fileStorageProperties;


    public ResponseEntity<ResponseMessage> createSubCategory(SubCategoryDTO subCategoryDTO) {

        String createdAt = DateAndTimeUtil.getDateAndTimeUtil();
        subCategoryDTO.setCreatedAt(createdAt);
        subCategoryDTO.setUpdatedAt(createdAt);
        fileStorageProperties = new FileStorageProperties();
        //String branchName = "_dir"+String.valueOf(subCategoryDTO.getBranchId()); //get from session
        // String industryCode ;//get from session
        // get industry code branch from session, now temporary we are assigning industry code manually,replace it
        // with session value of industry code and create sub directory under branch under category,here we are
        // not created subdirectory of category
        fileStorageProperties.setUploadDir("./uploads" + "/ph" + "/subcategory");
        String filename = fileStorageService.storeFile(subCategoryDTO.getImage(), fileStorageProperties,
                "_subcategory");
        ResponseMessage responseMessage = ResponseMessage.getInstance();

        SubCategory subCategory = new SubCategory();
        subCategory.setCategoryId(subCategoryDTO.getCategoryId());
        subCategory.setSubCategoryTitle(subCategoryDTO.getSubCategoryTitle());
        subCategory.setSubCategoryImage(filename);
        subCategory.setStatus(1);
        subCategory.setCreatedAt(subCategoryDTO.getCreatedAt());
        subCategory.setCreatedBy(subCategoryDTO.getCreatedBy());
        subCategory.setUpdatedAt(subCategoryDTO.getUpdatedAt());
        subCategory.setUpdatedBy(subCategoryDTO.getUpdatedBy());
        SubCategory subCat = repository.save(subCategory);
        if (subCat != null) {
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("1");
        } else {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("0");
        }
        return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
    }

    public ResponseEntity<ResponseMessage> updateSubCategory(SubCategoryDTO subCategoryDTO) {

        String updatedAt = DateAndTimeUtil.getDateAndTimeUtil();
        subCategoryDTO.setCreatedAt(updatedAt);
        subCategoryDTO.setUpdatedAt(updatedAt);
        fileStorageProperties = new FileStorageProperties();
        //String branchName = "_dir"+String.valueOf(subCategoryDTO.getBranchId()); //get from session
        // String industryCode ;//get from session
        // get industry code branch from session, now temporary we are assigning industry code manually,replace it
        // with session value of industry code and create sub directory under branch under category,here we are
        // not created subdirectory of category
        fileStorageProperties.setUploadDir("./uploads" + "/ph" + "/subcategory");
        String filename = fileStorageService.storeFile(subCategoryDTO.getImage(), fileStorageProperties,
                "_subcategory");
        ResponseMessage responseMessage = ResponseMessage.getInstance();
        SubCategory subCategory = new SubCategory();
        subCategory.setCategoryId(subCategoryDTO.getCategoryId());
        subCategory.setSubCategoryTitle(subCategoryDTO.getSubCategoryTitle());
        subCategory.setSubCategoryImage(filename);
        subCategory.setStatus(1);
        subCategory.setCreatedAt(subCategoryDTO.getCreatedAt());
        subCategory.setCreatedBy(subCategoryDTO.getCreatedBy());
        subCategory.setUpdatedAt(subCategoryDTO.getUpdatedAt());
        subCategory.setUpdatedBy(subCategoryDTO.getUpdatedBy());
        SubCategory subCat = repository.save(subCategory);
        if (subCat != null) {
            responseMessage.setMessage("success");
            responseMessage.setResponseStatus("1");
        } else {
            responseMessage.setMessage("error");
            responseMessage.setResponseStatus("0");
        }
        return new ResponseEntity<ResponseMessage>(responseMessage, HttpStatus.OK);
    }

    public SubCategoryDTO getSubCategory(String id, String categoryId) {
        SubCategory subCategory = repository.findByIdAndCategoryId(id, categoryId);
        return convertToDTO(subCategory);
    }

    public List<SubCategoryDTO> getAllSubCategories(String categoryId) {
        List<SubCategory> subCategories = repository.findByCategoryId(categoryId);
        List<SubCategoryDTO> subCategoryDTOList = new ArrayList<>();
        for (SubCategory mSubCat : subCategories) {
            subCategoryDTOList.add(convertToDTO(mSubCat));
        }
        return subCategoryDTOList;
    }

    public SubCategoryDTO convertToDTO(SubCategory subCat) {
        SubCategoryDTO subCategoryDTO = new SubCategoryDTO();
        if (subCat != null) {
            subCategoryDTO.setCategoryId(subCat.getCategoryId());
            subCategoryDTO.setSubCategoryTitle(subCat.getSubCategoryTitle());
            subCategoryDTO.setSubCategoryImage(subCat.getSubCategoryImage());
            subCategoryDTO.setStatus(subCat.getStatus());
            subCategoryDTO.setCreatedAt(subCat.getCreatedAt());
            subCategoryDTO.setUpdatedAt(subCat.getUpdatedAt());
            subCategoryDTO.setCreatedBy(subCat.getCreatedBy());
            subCategoryDTO.setUpdatedBy(subCat.getUpdatedBy());
        }
        return subCategoryDTO;
    }
}
