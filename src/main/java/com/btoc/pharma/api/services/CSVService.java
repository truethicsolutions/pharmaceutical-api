package com.btoc.pharma.api.services;

import com.btoc.pharma.api.dao.ProductRepository;
import com.btoc.pharma.api.helper.CSVHelper;
import com.btoc.pharma.api.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class CSVService {

    @Autowired
    ProductRepository repository;

    @Autowired(required = true)
    CSVHelper csvHelper;

    public void save(MultipartFile file, String userId, String adminId, String fileDir) {
        try {
            List<Product> products = csvHelper.csvProducts(file.getInputStream(),
                    userId, adminId, fileDir);
            repository.saveAll(products);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    public List<Product> getAllProducts() {
        return repository.findAll();
    }
}
